# Panorama

The panorama files are organised into the following subfolders (after layout/scripts/styles):

* `dota_hud` contains elements that remove, extend or replace parts of the dota hud. This includes the minimap, inventory, bottom panel, shops, ...
* `transitive` contains elements that appear temporarily during the game. This includes menus, transition between game components etc.
* `daemon` contains elements that are not related to the "core" dota hud, but are intended to blend seamlessly with it.
* `util` contains utility files which are used for various components.

The following .xml files appear in the root folders

* `custom_ui_manifest.xml` disables unwanted parts of the core dota hud and loads ours
* `hud_manager.xml` is our main workhorse for loading other panorama files. We keep it separate from `custom_ui_manifest` so that we can make use of some shared javascript functions between different HUD components
* `custom_loading_screen.xml` replaces the dota loading screen with one of our own
* `team_select.xml` and related files replace the team select (right hand side of the loading screen) with one of our own
* `debug_menu.xml` appears only in tools mode and gives a set of buttons to trigger certain panorama events, for testing
* `___template` files give a copy-paste format for each of the major file formats


## DotA Hud Elements

These elements are intended to appear like core elements of the DotA hud, but have been rewritten to extend functionality.

* `action_bar` replaces the DotA ability tray
* `bottom_panel` replaces the DotA bottom panel - portrait, health/mana bars and buff tray


## Transitive Hud Elements

These elements are ones that appear for short periods during gameplay, and are obviously menus or injected elements of some type. They are:

* `pre_game_screen` runs before a minigame starts, giving the players information about what they will need to do
* `post_game_screen` runs at the end of a minigame. It summarises the results of the minigame and gives some information about what is next
* `zone_transition_screen` runs after every collection of 3-4 minigames, summarising the results of the last segment

## Daemon Hud Elements

Daemon hud elements extend the dota hud in ways that are not "core" to dota. However, they are intended to blend seamlessly in with the hud during gameplay.

* `building_helper` is a library by Noya that enables units which can create buildings.
* `notifications` is a library by BMD that adds several locations to print custom notifications to players

## Util

These contain several sets of utility functions. Unlike other components, they may not contain a full set of xml/css/js files.

* `clicks` provides event handlers for mouse position and processing clicks for buildinghelper (and perhaps later other things)
* `images` precaches images. This is needed for images that might be "hot-loaded" during gameplay - dota won't compile them unless they're needed, so we forcefully compile them
* `messagehandler` is a small utility that is paired with `hud_manager` to give freshly created panels access to the function calls needed to send and receive messages or subscribe to events. 
* `playertables` is a library that emulates nettables, but sends information only to specific players. It is a requirement of `containers`
* `test_data` contains JSON objects for sample data sent to various panorama elements
* `utils` contains a set of functions common functions
* `util_sprintf` adds a c-style print function, with term replacement etc.
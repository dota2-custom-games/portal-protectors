"use strict";
/*
    Name - Version

    Description

*/

// ------------------------------------------------------------- //
// -------------------------- Variables ------------------------ //
// ------------------------------------------------------------- //


// ------------------------------------------------------------- //
// ----------------------- Message Passing --------------------- //
// ------------------------------------------------------------- //
// Handles receiving messages from other panels
// Send messages with SendMessage(target:string, title:string, data:obj)


function ReceiveMessage(sender, title, data){
    $.Msg("js_template.js ReceiveMessage | Received a message titled: "+title+", from: "+sender)
}

function EveryFiveSeconds(eventData){
    $.Msg("Received event fiveSeconds with data: "+eventData);
}

// ------------------------------------------------------------- //
// ---------------------------- IIFE --------------------------- //
// ------------------------------------------------------------- //

// Small schedule to allow messagehandler to initialize
($.Schedule(0.01, function() {
    SubscribeToEvent("fiveSeconds", function(eventData){ EveryFiveSeconds(eventData)});
}))

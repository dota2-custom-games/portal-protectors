"use strict";
/*
    Debug Menu - 0.01

    Side bar with buttons to trigger various debug functions
*/

// ------------------------------------------------------------- //
// -------------------------- Utility -------------------------- //
// ------------------------------------------------------------- //

// Creates a button that will trigger a message send event on click
function CreateDebugButton(target, title, data, tooltip, label)
{
    $.Msg("debug_menu.js CreateDebugButton | Creating a button with title: "+title);

    var newPanel = $.CreatePanel("Panel", $("#DebugMenu"), "");
    newPanel.BLoadLayoutSnippet( "DebugButton" );

    var debugButton = newPanel.FindChildTraverse("DebugButton");
    newPanel.FindChildTraverse("DebugLabel").text = label;

    debugButton.SetPanelEvent("onactivate", function() {
        SendMessage(target, title, data);
    });

    newPanel.SetPanelEvent("onmouseover", function(){ $.DispatchEvent( "DOTAShowTextTooltip",
        newPanel, tooltip );
    });

    newPanel.SetPanelEvent("onmouseout", function(){ $.DispatchEvent( "DOTAHideTextTooltip",
        newPanel);
    });



}

// ------------------------------------------------------------- //
// -------------------------- Utility -------------------------- //
// ------------------------------------------------------------- //

function AddShowPregameButton() {
    CreateDebugButton(
        "pre_game_screen",
        "LoadPregameScreen",
        PREGAME_TEST_DATA_1,
        "Show the pregame load screen with test data 1",
        "Pre1"
    );

    CreateDebugButton(
        "pre_game_screen",
        "LoadPregameScreen",
        PREGAME_TEST_DATA_2,
        "Show the pregame load screen with test data 2",
        "Pre2"
    );

    CreateDebugButton(
        "pre_game_screen",
        "HidePregameScreen",
        {},
        "Hide the pregame screen",
        "PreHide"
    );    
}

function AddShowPostgameButton() {
    CreateDebugButton(
        "post_game_screen",
        "LoadPostgameScreen",
        POSTGAME_TEST_DATA_1,
        "Show the postgame load screen with test data 1",
        "Post1"
    );

    CreateDebugButton(
        "post_game_screen",
        "HidePostgameScreen",
        {},
        "Hide the postgame screen",
        "PostHide"
    );    
}

function AddZoneTransitionButton() {
    CreateDebugButton(
        "zone_transition_screen",
        "ShowZoneTransition",
        ZONE_TRANSITION_TEST_DATA_1,
        "Show test zone transition 1",
        "ZT1"
    )

    CreateDebugButton(
        "zone_transition_screen",
        "ShowZoneTransition",
        ZONE_TRANSITION_TEST_DATA_2,
        "Show test zone transition 2",
        "ZT2"
    )

};


function ReceiveMessage() {};

// ------------------------------------------------------------- //
// ---------------------------- IIFE --------------------------- //
// ------------------------------------------------------------- //

// Small time delay for messagehandler
($.Schedule(0.01, function() {

    // Helps in hot reload
    $("#DebugMenu").RemoveAndDeleteChildren();

    // Add actual debug buttons
    AddShowPregameButton();
    AddShowPostgameButton();
    AddZoneTransitionButton();

}))
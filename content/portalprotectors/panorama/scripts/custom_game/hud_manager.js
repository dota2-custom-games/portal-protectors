"use strict";
/*
    HUD Manager 0.0.1

    The intent of the HUD manager is to move the task of loading
    and managing .xml files into javascript. There are three main
    reasons for this:

        1) Panels can be dynamically loaded or deleted as needed.
        "Heavy" components can be completely disabled if desired.

        2) Messages can be more conveniently passed between HUD
        components without having to bounce them off the Lua server.
        For example, this allows for much easier "querying" of units
        (checking their stats) when interacting with other hud elements.

        3) The demand of complex event listeners can be reduced. For
        example, we may have multiple panels that want to know which
        unit is being highlighted by the player, or if the player
        clicks on a unit ingame. We define the event handler here,
        and pass the useful information to any panels that are interested.

*/



// ------------------------------------------------------------- //
// ----------------------- Variables --------------------------- //
// ------------------------------------------------------------- //

var hud;
var customHud;

var loadedPanels = {}; // Object of {panelName : {"panel": Panel}}
var subscriptions = {}; // Object of {eventName: {panelName: Function()} }


// ------------------------------------------------------------- //
// ---------------------- Core UI Loading ---------------------- //
// ------------------------------------------------------------- //

// Util. Adds a panel to the UI
// @panelName: String. Name that will be used for the panel by the UI
        // This will be used when messages are sent to or from this panel
// @parent: Panel. Where should the panel be created? Should always be either hud or customHud.
// @layoutFile: String. Path to the layoutfile this panel should use
function AddNewPanel(panelName, parent, layoutFile){
    $.Msg("hud_manager.js AddNewPanel | Creating panel with name: "+panelName);

    if (loadedPanels[panelName] != null) {
        $.Msg("hud_manager.js AddNewPanel | Tried to create panel with name: "+panelName+", but such a name is already in use!");
        return false
    }
    if (parent.FindChildTraverse(panelName))
    {
        $.Msg("hud_manager.js AddNewPanel | Tried to create panel with name: "+panelName+", but such a panel already exists!");
        if (Game.IsInToolsMode()) {// Reassign the inherited functions for the panel
            $.Msg("We're in tools mode, replacing it!");
            var oldPanel = parent.FindChildTraverse(panelName);
            oldPanel.DeleteAsync(0);
        } else {
            return false
        }
    }

    var newPanel = $.CreatePanel( "Panel", parent, panelName );
    loadedPanels[panelName] = {"panel": newPanel};

    // This lets us call SendMessage() inside the associated js file of the new panel
    // In order to send a communique to another panel
    // The other panel must have set up ReceiveMessage (i.e. it must have messagehandler.js included)
    newPanel.SendMessage = function(target, title, data) {
        SendMessage(panelName, target, title, data);
    }
    newPanel.SubscribeToEvent = function(eventName, callback) {
        SubscribePanelToEvent(panelName, eventName, callback);
    }
    newPanel.UnsubscribeFromEvent = function(eventName) {
        UnsubscribePanelFromEvent(panelName, eventName);
    }

    newPanel.BLoadLayout(layoutFile, false, false );

    return newPanel
}

// Workhorse function.
// This function loads xml files for all of the main UI components
function LoadUI(){

    $.Msg("hud_manager.js LoadUI | Loading UI!");
    
    AddNewPanel("pre_game_screen", customHud, "file://{resources}/layout/custom_game/transitive/pre_game_screen.xml");
    AddNewPanel("post_game_screen", customHud, "file://{resources}/layout/custom_game/transitive/post_game_screen.xml");
    AddNewPanel("zone_transition_screen", customHud, "file://{resources}/layout/custom_game/transitive/zone_transition_screen.xml");
    AddNewPanel("notifications", customHud, "file://{resources}/layout/custom_game/daemon/notifications.xml");
    AddNewPanel("buildinghelper", customHud, "file://{resources}/layout/custom_game/daemon/building_helper.xml");
    AddNewPanel("progressbars", customHud, "file://{resources}/layout/custom_game/daemon/progress_bars.xml");

    AddNewPanel("bottompanel", customHud, "file://{resources}/layout/custom_game/dota_hud/bottom_panel/bottom_panel.xml");
    AddNewPanel("container_base", customHud,"file://{resources}/layout/custom_game/dota_hud/containers/container_base.xml");
    AddNewPanel("minimap_manager", customHud,"file://{resources}/layout/custom_game/dota_hud/objective_minimap.xml");

    // Black panel is special, it needs z-index to make sure it's always on top!
    var black_screen = AddNewPanel("black_screen", customHud, "file://{resources}/layout/custom_game/transitive/black_screen.xml");
    black_screen.style.zIndex = "999";

    if (Game.IsInToolsMode()) { 
        AddNewPanel("debugmenu", customHud, "file://{resources}/layout/custom_game/debug_menu.xml");
    }

}


// ------------------------------------------------------------- //
// ----------------------- Event Handlers ---------------------- //
// ------------------------------------------------------------- //

// Called from any child panel. Sends a message to another panel
// @sender: String. Should be a key in loadedPanels
// @target: String. Must be a key in loadedPanels
// @title: String. Indicator of what the message should be for
// @data: Object. Indicates what the recipient should actually do
function SendMessage(sender, target, title, data)
{
    if (!loadedPanels[sender]) {
        $.Msg("hud_manager.js SendMessage | Sender: "+sender+" is not a known panel!");
        return false
    }
    if (!loadedPanels[target]) {
        $.Msg("hud_manager.js SendMessage | Target: "+target+" is not a known panel!");
        return false
    }
    if (!loadedPanels[target].panel.ReceiveMessage) {
        $.Msg("hud_manager.js SendMessage | Target: "+target+" is not accepting messages!");
        return false
    }

    $.Msg("hud_manager.js SendMessage | Sender: "+sender+" sends message: "+title+" to target: "+target);
    loadedPanels[target].panel.ReceiveMessage(sender, title, data);

    return true

}

// Called from any child panel.
// Registers the child as being interested in a specific event (clicks, mouse position etc.)
// @panelName: String. Must be a key in loadedPanels
// @eventName: String. 
// @callback: Function
function SubscribePanelToEvent(panelName, eventName, callback){
    if (!subscriptions.eventName) { subscriptions.eventName = {} }
    $.Msg("hud_manager.js SubscribePanelToEvent | Subscribing Panel: "+panelName+" to event: "+eventName+", with callback: "+callback);
    subscriptions.eventName.panelName = callback;
}
function UnsubscribePanelFromEvent(panelName, eventName){
    if (!subscriptions.eventName) {
        $.Msg("hud_manager.js UnsubscribePanelFromEvent | Tried to remove from event: "+eventName+", but noone is registered!");
        return false
    }
    if (!subscriptions.eventName.panelName) {
        $.Msg("hud_manager.js UnsubscribePanelFromEvent | Tried to remove an event from panel: "+panelName+", but it isn't registered!");
        return false
    }

    delete subscriptions.eventName.panelName;
}

// Takes an event and forwards it to any interested panels
function ForwardEvent(eventName, eventData){
    if (!subscriptions.eventName){
        return false
    }

    for (var panelName in subscriptions.eventName) {
        //$.Msg("hud_manager.js ForwardEvent | Sending event: "+eventName+" to panel: "+panelName+", with data: "+eventData);
        subscriptions.eventName.panelName(eventData); // (calls the callback function)
    }

}

// ------------------------------------------------------------- //
// ---------------------------- Util --------------------------- //
// ------------------------------------------------------------- //

// Debug. Sends an event every five seconds
/*
var counter = 0;
function FiveSecondsEvent(){
    counter += 1;
    //$.Msg("hud_manager.js FiveSecondsEvent | Sending five seconds event")
    ForwardEvent("fiveSeconds", counter)
    $.Schedule(5, FiveSecondsEvent);
}
*/

// Hides a bunch of ugly dota hud elements that we don't want
function ConfigureDotaHud() {

    $.Msg("hud_manager.js ConfigureDotaHud | Runs!");

    var panelsToHide = [
    "GlyphScanContainer", // The glyph and radar buttons next to minimap
    "quickstats", // The K/D thing in the top left
    "HUDSkinTopBarBG",
    "TimeOfDayBG", // Time of day background
    "TimeUntil", // "Time until night..." label when holding alt
    "ToggleScoreboardButton",
    "OrdersContainer", // Thing above the action bar for when you have queued orders
    "combat_events" // Deaths thing on the left hand side
    ];

    for (var panel of panelsToHide) {
        var testPanel = hud.FindChildTraverse(panel);
        if (testPanel) { 
            testPanel.visible = false;
        }
    }
}

// ------------------------------------------------------------- //
// ---------------------------- IIFE --------------------------- //
// ------------------------------------------------------------- //


(
function() {
    $.Msg("hud_manager.js | Initializing!");

    hud = GetHud();
    customHud = hud.FindChildTraverse("CustomUIRoot");

    ConfigureDotaHud();

    LoadUI();

    //FiveSecondsEvent();

})()
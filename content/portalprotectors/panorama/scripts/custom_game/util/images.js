"use strict";

// Kills all image panels.
// We needed to load them at the start in order to precache them
// But once that is done we don't want them any more
function DestroyEverything(){
    $("#ImagesRoot").RemoveAndDeleteChildren();
}

(function() {
    $.Schedule(5, DestroyEverything);
})()
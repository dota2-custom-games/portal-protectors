"use strict";
/*
    Test Data - 0.01

    Contains test data, mainly used by the debug menu

*/

// ------------------------------------------------------------- //
// -------------------------- Variables ------------------------ //
// ------------------------------------------------------------- //

var PREGAME_TEST_DATA_1 = {
    name: "testgame1"
}

var PREGAME_TEST_DATA_2 = {
    name: "testgame2",
    choiceCount: 3
}

var POSTGAME_TEST_DATA_1 = {
    name: "testgame2",
    upNext: "testgame1",
    success: true,
    scoreMetric: "time_seconds",
    leaderboards: [
        // Entry 1
        {
            rank: 1,
            score: 11.7,
            players: [
                { steamId: "76561197999387723" },
                { steamId: "76561198030737141" },
                { steamId: "76561198010088486" }
            ]
        },
        // Entry 2
        {
            rank: 27,
            score: 21.6,
            players: [
                { steamId: "76561198002366151" },
                { steamId: "76561198102731341" }
            ]
        },
        // Entry 3
        {
            rank: 28,
            score: 21.8,
            highlight: true,
            players: [
                { steamId: "76561198029043021" },
                { steamId: "76561198002366151" },
                { steamId: "76561198010088486" },
                { steamId: "76561198102731341" },
                { steamId: "76561197989160801" },
                { steamId: "76561198030737141" },
                { steamId: "76561197999387723" }
            ]
        },
        // Entry 4
        {
            rank: 29,
            score: 22.3,
            players: [
                { steamId: "76561198030737141" },
                { steamId: "76561197989160801" },
                { steamId: "76561198102731341" },
                { steamId: "76561198002366151" }
            ]
        },
    ]
}

var ZONE_TRANSITION_TEST_DATA_1 = {
    fromZone: {
        zone: "Arcade",
        games: [
            {
                name: "testgame1",
                won: true
            },
            {
                name: "testgame2",
                won: false
            },
            {
                name: "testgame2",
                won: false
            }
        ],
        averageRank: 521,
        totalRank: 1029,
        zonePassed: false
    }
}

var ZONE_TRANSITION_TEST_DATA_2 = {
    fromZone: {
        zone: "Arcade",
        games: [
            {
                name: "testgame1",
                won: true
            },
            {
                name: "testgame2",
                won: false
            },
            {
                name: "testgame2",
                won: true
            },
            {
                name: "testgame2",
                won: true
            }
        ],
        averageRank: 521,
        totalRank: 1029,
        zonePassed: true
    },
    toZone: {
        winsNeeded: "3",
        zone: "Scifi",
        games: [
            {
                name: "testgame1"
            },
            {
                name: "testgame2"
            },
            {
                name: "testgame2"
            },
            {
                name: "testgame1"
            },
            {
                name: "testgame1"
            }
        ]
    }

}
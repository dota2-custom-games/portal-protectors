/*
    Utility Functions

    This file contains a collection of useful utility functions.

*/

// Returns a reference to the dota hud panel
function GetHud(){
    var panel = $.GetContextPanel().GetParent();
    for(var i = 0; i < 100; i++) {
        if(panel.id != "Hud") {
            panel = panel.GetParent();
        } else {
            break;
        }
    };
    return panel
}

// For some reason indexOf doesn't consistently work with arrays
// Returns true if arr contains an element with value val, else false
function contains(arr, val){
    for (var i = 0; i < arr.length; i++ )
    {
        if (arr[i] == val) {
            return true;
        };
    };
    return false;
};

// Returns true iff a nettable value is empty
// for some reason obj = nil, obj = {}... don't work consistently
function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}

// "Safely" remove an element from an array based on value
// Iterate backwards over array so that we don't crop the list
// Returns true iff something was deleted
// We will only delete at most one element!
function RemoveFromArrayByValue(arr, value){
    var i = arr.length
    while (i--) {
        if (arr[i] == value)
        {
            arr.splice(i, 1);
            return true
        }
    }
    return false
}


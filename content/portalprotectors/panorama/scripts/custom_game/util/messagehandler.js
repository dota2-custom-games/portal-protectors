"use strict";
/*
    MessageHandler 0.0.1

    Works with hud_manager.js
    Include this on any xml files that need to make use of the message system
    This assigns the function names for each of the important message functions

*/

var SendMessage = $.GetContextPanel().SendMessage;
var SubscribeToEvent = $.GetContextPanel().SubscribeToEvent;
var UnsubscribeFromEvent = $.GetContextPanel().UnsubscribeFromEvent;

// Receivemessage needs to be defined on the main javascript file
// We're going to wait a frame just in case the js files were loaded in a different order
$.Schedule(0.03, function() {
    if (ReceiveMessage){1
        $.GetContextPanel().ReceiveMessage = ReceiveMessage;
    }
});

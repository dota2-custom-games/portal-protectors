"use strict";
/*
    Zone Transition Screen - 0.02

    Handles transitions between "Zones" - sets of 3-5 games.

*/

// ------------------------------------------------------------- //
// ------------------------ Main Function ---------------------- //
// ------------------------------------------------------------- //

// Called from lua. Lua sends tables, so we have to perform a little scrubbing to convert into arrays
function OnZoneTransition(data)
{
    var tidiedData = {}

    if (data["fromZone"]) {
        tidiedData["fromZone"] = {}
        tidiedData["fromZone"]["zone"] = data["fromZone"]["zone"]
        tidiedData["fromZone"]["zonePassed"] = data["fromZone"]["zonePassed"]
        tidiedData["fromZone"]["averageRank"] = data["fromZone"]["averageRank"]
        tidiedData["fromZone"]["totalRank"] = data["fromZone"]["totalRank"]
        tidiedData["fromZone"]["games"] = jsonToArray(data["fromZone"]["games"])
    }

    if (data["toZone"]) {
        tidiedData["toZone"] = {}
        tidiedData["toZone"]["zone"] = data["toZone"]["zone"]
        tidiedData["toZone"]["winsNeeded"] = data["toZone"]["winsNeeded"]
        tidiedData["toZone"]["games"] = jsonToArray(data["toZone"]["games"])
    }

    PerformZoneTransition(tidiedData)
}
// Cast a dumb json object from lua into a beautiful array
function jsonToArray(json){
    var output = [];
    $.Each(json, function(key, val){
        output.push(key); // Yeah I don't understand why this isn't val either
    })
    return output
}



// Workhorse - transitions from a zone to another zone
function PerformZoneTransition(data)
{
    var timeToWait = 0;
    var nextTimeToWait = 0;

    //SendMessage("black_screen", "SetFadeToBlack", {show: true, inFront: false});
    $.GetContextPanel().visible = true;

    if (data["fromZone"]) {
        timeToWait = SetupFromZoneTransition(data["fromZone"]);
    }

    if (data["toZone"]) {
        $.Schedule(timeToWait, function(){
            nextTimeToWait = SetupToZoneTransition(data["toZone"]);
            $.Schedule(nextTimeToWait, function() {
                //SendMessage("black_screen", "SetFadeToBlack", {show: false, inFront: false});
                $.GetContextPanel().visible = false;
            })
        })
    } else {
        $.Schedule(timeToWait, function(){
            //SendMessage("black_screen", "SetFadeToBlack", {show: false, inFront: false});
            $.GetContextPanel().visible = false;
        })
    }

}

// Transition "from" a zone
// Show games won, etc.
// Returns the amount of time to be spent in this menu!
function SetupFromZoneTransition(data)
{

    $.Msg("zone_transition_screen.js SetupFromZoneTransition | Runs!");

    var waitTime = 0;
    var zone = data["zone"];
    var games = data["games"];
    var zonePassed = data["zonePassed"];
    var locale = $.Language();

    // Title area
    $("#Title").style.visibility = "visible";
    $("#Subtitle").style.visibility = "visible";

    $("#Title").text = zonePassed ? $.Localize("#PP_Zone_Complete") : $.Localize("#PP_Zone_Failed");
    $("#Subtitle").text = $.Localize("#PP_Zone_Name_"+zone);
    $("#Title").style.transform = "translatex(0px)";
    $("#Subtitle").style.transform = "translatex(0px)";

    // Slide out
    $.Schedule(5.5, function() {
        $("#Title").style.transform = "translatex(-3000px)";
        $("#Subtitle").style.transform = "translatex(-3000px)";
    })

    // Hide and reposition
    $.Schedule(7, function() {
        $("#Title").style.visibility = "collapse";
        $("#Subtitle").style.visibility = "collapse";
        $("#Title").style.transform = "translatex(3000px)";
        $("#Subtitle").style.transform = "translatex(3000px)";
    })

    //-- Games
    // Okay, we need to manually position these because they will slide on 1 by 1
    // So prepare for some maffs
    var won;
    var gameName;
    var game;
    var parent;
    var newPanel;
    var isRowZero;
    var xOffset;
    var titleImage;
    var titleLabel;
    $("#GamesRow0").RemoveAndDeleteChildren();
    $("#GamesRow1").RemoveAndDeleteChildren();
    var rowZeroGames = (games.length < 4) ? games.length : Math.ceil(games.length/2);
    var rowOneGames = games.length - rowZeroGames;
    for (var i in games) {
        game = games[i];
        gameName = game["name"];
        won = game["won"];
        isRowZero = (games.length < 4 || (i%2===0))
        parent = $("#GamesArea").FindChildTraverse(
                isRowZero ? "GamesRow0" : "GamesRow1"
        );

        $.Msg("FromZoneTransition | Game name: "+gameName+", victory: "+won)

        newPanel = $.CreatePanel("Panel", parent, "");
        newPanel.BLoadLayoutSnippet( "GamePreview" );

        // Position the panel (after a delay)
        xOffset = (
            10 + // Side margin
            30*(parent.Children().length - 1) + // Existing children
            (15 * (3 - (isRowZero ? rowZeroGames : rowOneGames))) // Extra margin for un-full row
        );
        newPanel.style.transition = "transform 0.33s linear "+(1+(0.25*i))+"s";
        newPanel.style.transform = "translatex("+xOffset+"%)";

        SetScheduledHide(newPanel, 6);

        // Set game title        
        titleImage = newPanel.FindChildTraverse("GameNameImage");
        titleLabel = newPanel.FindChildTraverse("GameNameLabel");
        if ($.Localize("#PP_Minigame_Title_Image_Available_"+gameName+"_"+locale) == "TRUE") {
            titleImage.style.backgroundImage = "url('file://{images}/games/"+ 
            gameName +"_title_"+locale+".png');";
            titleImage.style.visibility = "visible";
            titleLabel.style.visibility = "collapse";
        } else {
            titleImage.style.visibility = "collapse";
            titleLabel.style.visibility = "visible";
            titleLabel.text = $.Localize("#PP_Minigame_Title_"+gameName);
        }

        // Set Hover effects
        AssignHoverEffects(newPanel, gameName);

        // Set game success
        newPanel.SetHasClass("Won", (won));
        newPanel.SetHasClass("Lost", (!won));

    }

    // "Average Ranking" section

    // Appear
    $.Schedule(1+0.25*games.length, function() {
        $("#FlavourText").style.visibility = "visible";
        $("#FlavourText").style.transform = "translatex(0px)";
    });

    // Set text
    if (zonePassed) {
        $("#FlavourText").text = $.Localize("#PP_Zone_Complete_Flavour")
        waitTime = 10;
    } else {
        $("#FlavourText").text = $.Localize("#PP_Zone_Failed_Flavour")
        waitTime = 12; // We'll freeze on this screen because players lost
    }
    if (data["averageRank"] && data["totalRank"]) {
        $("#FlavourText").text = $("#FlavourText").text +
            $.Localize("#PP_Average_Rank")+
            data["averageRank"]+"/"+
            data["totalRank"];
    }

    // Disappear
    $.Schedule(9, function() {
        $("#FlavourText").style.transform = "translatex(-3000px)";
    });

    // Hide and reposition
    $.Schedule(9.5, function() {
        $("#FlavourText").style.transform = "translatex(3000px)";
        $("#FlavourText").style.visibility = "collapse";
    })



    return waitTime;

}

// Transition "to" a zone
// Show games that will be played
function SetupToZoneTransition(data)
{
    $.Msg("zone_transition_screen.js SetupToZoneTransition | Runs!");

    var waitTime = 0;
    var zone = data["zone"];
    var games = data["games"];
    var winsNeeded = data["winsNeeded"];
    var locale = $.Language();

    $.Msg("Wins needed: "+winsNeeded);

    // Title area
    $("#Title").style.visibility = "visible";
    $("#Subtitle").style.visibility = "visible";

    $("#Title").text = $.Localize("#PP_Entering_Zone");
    $("#Subtitle").text = $.Localize("#PP_Zone_Name_"+zone);
    $("#Title").style.transform = "translatex(0px)";
    $("#Subtitle").style.transform = "translatex(0px)";

    // Slide out
    $.Schedule(5.5, function() {
        $("#Title").style.transform = "translatex(-3000px)";
        $("#Subtitle").style.transform = "translatex(-3000px)";
    })

    // Hide and reposition
    $.Schedule(7, function() {
        $("#Title").style.visibility = "collapse";
        $("#Subtitle").style.visibility = "collapse";
        $("#Title").style.transform = "translatex(3000px)";
        $("#Subtitle").style.transform = "translatex(3000px)";
    })

    //-- Games
    // Okay, we need to manually position these because they will slide on 1 by 1
    // So prepare for some maffs
    var gameName;
    var game;
    var parent;
    var newPanel;
    var isRowZero;
    var xOffset;
    var titleImage;
    var titleLabel;
    $("#GamesRow0").RemoveAndDeleteChildren();
    $("#GamesRow1").RemoveAndDeleteChildren();
    var rowZeroGames = (games.length < 4) ? games.length : Math.ceil(games.length/2);
    var rowOneGames = games.length - rowZeroGames;
    for (var i in games) {
        game = games[i];
        gameName = game["name"];
        isRowZero = (games.length < 4 || (i%2===0))
        parent = $("#GamesArea").FindChildTraverse(
                isRowZero ? "GamesRow0" : "GamesRow1"
        );


        newPanel = $.CreatePanel("Panel", parent, "");
        newPanel.BLoadLayoutSnippet( "GamePreview" );

        // Position the panel (after a delay)
        xOffset = (
            10 + // Side margin
            30*(parent.Children().length - 1) + // Existing children
            (15 * (3 - (isRowZero ? rowZeroGames : rowOneGames))) // Extra margin for un-full row
        );
        newPanel.style.transition = "transform 0.33s linear "+(1+(0.25*i))+"s";
        newPanel.style.transform = "translatex("+xOffset+"%)";

        SetScheduledHide(newPanel, 6);

        // Set game title        
        titleImage = newPanel.FindChildTraverse("GameNameImage");
        titleLabel = newPanel.FindChildTraverse("GameNameLabel");
        if ($.Localize("#PP_Minigame_Title_Image_Available_"+gameName+"_"+locale) == "TRUE") {
            titleImage.style.backgroundImage = "url('file://{images}/games/"+ 
            gameName +"_title_"+locale+".png');";
            titleImage.style.visibility = "visible";
            titleLabel.style.visibility = "collapse";
        } else {
            titleImage.style.visibility = "collapse";
            titleLabel.style.visibility = "visible";
            titleLabel.text = $.Localize("#PP_Minigame_Title_"+gameName);
        }

        // Set Hover effects
        AssignHoverEffects(newPanel, gameName);

    }

    //-- Pass n games section

    // Appear
    $.Schedule(1+0.25*games.length, function() {
        $("#FlavourText").style.visibility = "visible";
        $("#FlavourText").style.transform = "translatex(0px)";
    });

    $("#FlavourText").text = $.Localize("#PP_Pass_N_Games").replace("%s", winsNeeded);

    // Disappear
    $.Schedule(9, function() {
        $("#FlavourText").style.transform = "translatex(-3000px)";
    });

    // Hide and reposition
    $.Schedule(9.5, function() {
        $("#FlavourText").style.transform = "translatex(3000px)";
        $("#FlavourText").style.visibility = "collapse";
    });

    waitTime = 10;
    return waitTime;



}

function SetScheduledHide(newPanel, delay) {
    $.Schedule(delay, function() {
        newPanel.style.transform = "translatex(-3000px)";
    })
}

function AssignHoverEffects(newPanel, gameName) {
    newPanel.SetPanelEvent("onmouseover", function(){ $.DispatchEvent( "DOTAShowTitleTextTooltip",
        newPanel,
        $.Localize("#PP_Minigame_Title_"+gameName),
        $.Localize("#PP_Minigame_Description_"+gameName) );
    });

    newPanel.SetPanelEvent("onmouseout", function(){ $.DispatchEvent( "DOTAHideTitleTextTooltip",
        newPanel);
    });

}

// ------------------------------------------------------------- //
// ----------------------- Message Passing --------------------- //
// ------------------------------------------------------------- //
// Handles receiving messages from other panels
// Send messages with SendMessage(target:string, title:string, data:obj)


function ReceiveMessage(sender, title, data){
    $.Msg("js_template.js ReceiveMessage | Received a message titled: "+title+", from: "+sender)

    if (title === "ShowZoneTransition") {
        PerformZoneTransition(data);
    }
}

// ------------------------------------------------------------- //
// ---------------------------- IIFE --------------------------- //
// ------------------------------------------------------------- //

// Small schedule to allow messagehandler to initialize
($.Schedule(0.01, function() {
    GameEvents.Subscribe( "perform_zone_transition", OnZoneTransition );
}))

"use strict";

// ------------------------------------------------------------- //
// -------------------------- Variables ------------------------ //
// ------------------------------------------------------------- //

// ------------------------------------------------------------- //
// ----------------------------- Main -------------------------- //
// ------------------------------------------------------------- //

function LoadPregameScreen(data) {

    var locale = $.Language();
    var name = data["name"]; // Most data is loaded from this
    var choiceCount = data["choiceCount"]; // How many "choices" should we make available?

    //------- Title -------// 
    // Attempt to set the background image of the title based on the locale
    // Check with hardcoded strings in localization, because I don't know another way
    if ($.Localize("#PP_Minigame_Title_Image_Available_"+name+"_"+locale) == "TRUE") {
        $("#TitleImage").style.backgroundImage = "url('file://{images}/games/"+ 
        name +"_title_"+locale+".png');";
        $("#TitleImage").style.visibility = "visible";
        $("#TitleLabel").style.visibility = "collapse";
    } else {
        $("#TitleImage").style.visibility = "collapse";
        $("#TitleLabel").style.visibility = "visible";
        $("#TitleLabel").text = $.Localize("#PP_Minigame_Title_"+name);
    }

    //------- Description Text ------//
    $("#DescriptionText").text = $.Localize("#PP_Minigame_Description_"+name);
    var i=0;
    // Tooltip counting hack that will inevitably cause irreversible crashes in the future
    while ($.Localize("#PP_Minigame_Description_Image_"+name+"_"+i) !==
        ("PP_Minigame_Description_Image_"+name+"_"+i)
    ) {
        $("#DescriptionText").text += "\n\t • "+ $.Localize("#PP_Minigame_Description_Image_"+name+"_"+i);
        i += 1;
        if (i>5) { break; } // Just in case
    }

    //------- Guide Images --------//
    /* Disabled unless I can improve the CSS
    for (var i=0; i<3; i++) {
        $("#DescriptionImage"+i).style.backgroundImage = "url('file://{images}/games/"+ 
            name +"_description_image_"+i+".png');"
        $("#DescriptionImageText"+i).text = $.Localize("#PP_Minigame_Description_Image_"+name+"_"+i);
    }
    */

    //------- Game Options/Choices --------//
    $("#ChoiceContainerRow").RemoveAndDeleteChildren();
    $("#ChoiceRow").style.visibility = "collapse";
    $.GetContextPanel().SetHasClass("LargeBorder", false)
    if (choiceCount && choiceCount > 0) {
        $("#ChoiceRow").style.visibility = "visible";
        $.GetContextPanel().SetHasClass("LargeBorder", true)
        
        for (var i=0; i<choiceCount; i++) {
            var newPanel = $.CreatePanel("Panel", $("#ChoiceContainerRow"), "");
            newPanel.BLoadLayoutSnippet( "ChoicePanel" );

            newPanel.FindChildTraverse("ChoiceImage").style.backgroundImage = "url('file://{images}/games/"+ 
                name +"_choice_image_"+i+".png');"
            newPanel.FindChildTraverse("ChoiceImage").style.backgroundSize = "100% 100%";

            newPanel.FindChildTraverse("ChoiceText").text = $.Localize("#PP_Minigame_Choice_Text_"+name+"_"+i)


            AssignChoiceEvents(newPanel, name, i);

        }
    }

    $.GetContextPanel().style.visibility = "visible";
    $.GetContextPanel.hittest = true;
    $.Schedule(0.5, function() {
        $("#PregameRoot").style.opacity = "1";
    })

}

function HidePregameScreen() {
    $("#PregameRoot").style.opacity = "0";
    $.GetContextPanel.hittest = false;
    $.Schedule(0.5, function() {
        $.GetContextPanel().style.visibility = "collapse";
    })
}

// Scoping
function AssignChoiceEvents(newPanel, name, index)
{

    newPanel.SetPanelEvent("onactivate", function() {
        $.Msg("pre_game_screen.js AssignChoiceEvents | Clicked choice: "+index);
        
        for (var targetPanel of $("#ChoiceContainerRow").Children()) {
                    targetPanel.FindChildTraverse("ChoiceImage").style.backgroundSize = "100% 100%";
                    targetPanel.SetHasClass("Active", targetPanel===newPanel);
                }


        // TODO: Send lua event
    });

    var title = $.Localize("#PP_Minigame_Choice_Text_Title_"+name+"_"+index);
    var tooltip = $.Localize("#PP_Minigame_Choice_Text_Description_"+name+"_"+index);

    newPanel.SetPanelEvent("onmouseover", function(){ $.DispatchEvent( "DOTAShowTitleTextTooltip",
        newPanel, title, tooltip );
    });

    newPanel.SetPanelEvent("onmouseout", function(){ $.DispatchEvent( "DOTAHideTitleTextTooltip",
        newPanel);
    });

}

function UpdateCountdown(time) {
    $("#CountdownTimer").text = time
}

// ------------------------------------------------------------- //
// ----------------------- Message Passing --------------------- //
// ------------------------------------------------------------- //
// Handles receiving messages from other panels

function ReceiveMessage(sender, title, data){

    $.Msg("pre_game_screen.js ReceiveMessage | Received a message titled: "+title+", from: "+sender)

    if (title=="LoadPregameScreen") {
        LoadPregameScreen(data);
    }
    else if (title=="HidePregameScreen") {
        HidePregameScreen();
    }
}

// Redirect from lua call
function OnSetupPregameScreen(keys) {
    LoadPregameScreen(keys);
}

function OnUpdateCountdown(keys) {
    if (!keys.time || keys.time == "0") {
        HidePregameScreen();
    } else {
        UpdateCountdown(keys.time);
    }
}


// ------------------------------------------------------------- //
// ---------------------------- IIFE --------------------------- //
// ------------------------------------------------------------- //

// Small schedule to allow messagehandler to initialize
($.Schedule(0.01, function() {

    GameEvents.Subscribe( "setup_pregame_screen", OnSetupPregameScreen );
    GameEvents.Subscribe( "update_pregame_countdown", OnUpdateCountdown );

}))
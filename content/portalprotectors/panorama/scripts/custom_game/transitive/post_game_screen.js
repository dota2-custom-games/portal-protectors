"use strict";


// ------------------------------------------------------------- //
// -------------------------- Variables ------------------------ //
// ------------------------------------------------------------- //

// ------------------------------------------------------------- //
// ----------------------------- Main -------------------------- //
// ------------------------------------------------------------- //

function LoadPostgameScreen(data) {
    
    $.GetContextPanel().style.visibility = "visible";
    $.GetContextPanel.hittest = true;
    $.Schedule(0.5, function() {
        $("#PostgameRoot").style.opacity = "1";
    })

    // You know, Clean Code dictates that this should be 5 different functions
    // Yeah, I read that book.
    // But on the other hand...

    var locale = $.Language();

    // ------- Title ------- //
    var name = data["name"];
    if ($.Localize("#PP_Minigame_Title_Image_Available_"+name+"_"+locale) == "TRUE") {
        $("#TitleImage").style.backgroundImage = "url('file://{images}/games/"+ 
        name +"_title_"+locale+".png');";
        $("#TitleImage").style.visibility = "visible";
        $("#TitleLabel").style.visibility = "collapse";
    } else {
        $("#TitleImage").style.visibility = "collapse";
        $("#TitleLabel").style.visibility = "visible";
        $("#TitleLabel").text = $.Localize("#PP_Minigame_Title_"+name);
    }

    // ------- Pass/Fail ------- //
    var success = data["success"];
    // TODO: Change this if/when success/fail images available
    $("#ResultText").text = $.Localize("#PP_Minigame_Success_"+success);

    // ------- Leaderboards ------- //
    $("#RankingContainer").RemoveAndDeleteChildren();
    var leaderboardRows = data["leaderboards"];
    var scoreMetric = data["scoreMetric"];
    var newPanel;
    var lastRank = 0;
    var rank;
    var players;
    var score;
    var playerPanel;
    var steamId;
    var highlight;
    if (leaderboardRows) {
        // Headers
        $("#RankingRow").style.visibility = "visible";

        newPanel = $.CreatePanel("Panel", $("#RankingContainer"), "");
        newPanel.BLoadLayoutSnippet( "LeaderboardHeader" );
        newPanel.FindChildTraverse("ScoreHeader").text = $.Localize("#PP_Score_Metric_"+scoreMetric);

        // Rows
        for (var row of leaderboardRows) {
            rank = row["rank"];
            players = row["players"];
            score = row["score"];
            highlight = row["highlight"];

            // Add jubjub if there's a big difference
            if (rank-lastRank !== 1) {
                newPanel = $.CreatePanel("Panel", $("#RankingContainer"), "");
                newPanel.BLoadLayoutSnippet( "LeaderboardJubJub" );
            }

            // Proceed to load the data   
            newPanel = $.CreatePanel("Panel", $("#RankingContainer"), "");
            newPanel.BLoadLayoutSnippet( "LeaderboardRow" );
            newPanel.FindChildTraverse("Rank").text = rank;
            newPanel.FindChildTraverse("Score").text = score;
            if (highlight) {
                newPanel.SetHasClass("Highlight", highlight);
            }

            for (var player of players) {
                playerPanel = $.CreatePanel("Panel", newPanel.FindChildTraverse("PlayersContainer"), "");
                playerPanel.BLoadLayoutSnippet( "LeaderboardPlayer" );
                steamId = player["steamId"];
                playerPanel.FindChildTraverse("AvatarImage").steamid = steamId;
            }

            lastRank = rank;

        }
    } else {
        $("#RankingRow").style.visibility = "collapse";
    }

    // ------- Up Next ------- //
    name = data["upNext"];
    if ($.Localize("#PP_Minigame_Title_Image_Available_"+name+"_"+locale) == "TRUE") {
        $("#UpNextImage").style.backgroundImage = "url('file://{images}/games/"+ 
        name +"_title_"+locale+".png');";
        $("#UpNextImage").style.visibility = "visible";
        $("#UpNextLabel").style.visibility = "collapse";
    } else {
        $("#UpNextImage").style.visibility = "collapse";
        $("#UpNextLabel").style.visibility = "visible";
        $("#UpNextLabel").text = $.Localize("#PP_Minigame_Title_"+name);
    }


}

function HidePostgameScreen() {
    $("#PostgameRoot").style.opacity = "0";
    $.GetContextPanel.hittest = false;
    $.Schedule(0.5, function() {
        $.GetContextPanel().style.visibility = "collapse";
    })
}

function UpdateCountdown(time) {
    // TODO: Should we add a countdown timer?
    //$("#CountdownTimer").text = time
}

// ------------------------------------------------------------- //
// ----------------------- Message Passing --------------------- //
// ------------------------------------------------------------- //

function ReceiveMessage(sender, title, data){

    $.Msg("pre_game_screen.js ReceiveMessage | Received a message titled: "+title+", from: "+sender)
    
    if (title=="LoadPostgameScreen") {
        LoadPostgameScreen(data);
    }
    else if (title=="HidePostgameScreen") {
        HidePostgameScreen();
    }

}

// Redirect from lua call
function OnSetupPostgameScreen(keys) {
    LoadPostgameScreen(keys);
}

function OnUpdateCountdown(keys) {
    if (!keys.time || keys.time == "0") {
        HidePostgameScreen();
    } else {
        UpdateCountdown(keys.time);
    }
}


// ------------------------------------------------------------- //
// ---------------------------- IIFE --------------------------- //
// ------------------------------------------------------------- //

($.Schedule(0.01, function() {
    GameEvents.Subscribe( "setup_postgame_screen", OnSetupPostgameScreen );
    GameEvents.Subscribe( "update_postgame_countdown", OnUpdateCountdown );
}))

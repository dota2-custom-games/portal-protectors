"use strict";
/*
    Zone Transition Screen - 0.02

    Handles transitions between "Zones" - sets of 3-5 games.

*/

// ------------------------------------------------------------- //
// ------------------------ Main Function ---------------------- //
// ------------------------------------------------------------- //

// Shows/Hides the black background
// @param show: bool. Should we set it to black?
// @param inFont: bool. Should the black have the highest priority z-index (i.e in front of other panorama elements?)
function SetFadeToBlack(show, inFront)
{

    $.Msg("Fade to black called | Show: "+show)

    $.GetContextPanel().style.visibility = "visible";

    // "game" panels will have a parent zindex < 0,
    // "transitive" panels will have a parent zindex > 0
    // inFront puts us in front of all panels,
    // false puts us only in front of "game" panels
    $.GetContextPanel().style.zIndex = inFront ? "100" : "0";

    $("#Background").style.opacity = (show) ? "1" : "0"
    $.Schedule(0.5, function(){
        $("#BackgroundTopRight").hittest = show;
        $("#BackgroundMain").hittest = show;
        $.GetContextPanel().style.visibility = show ? "visible" : "collapse";
    })

}

// ------------------------------------------------------------- //
// ----------------------- Message Passing --------------------- //
// ------------------------------------------------------------- //
// Handles receiving messages from other panels
// Send messages with SendMessage(target:string, title:string, data:obj)


function ReceiveMessage(sender, title, data){
    $.Msg("black_screen.js ReceiveMessage | Received a message titled: "+title+", from: "+sender)

    if (title === "SetFadeToBlack") {
        SetFadeToBlack(data.show, data.inFront);
    }
}

function OnSetFadeToBlack(keys) {
    SetFadeToBlack(keys.show, keys.inFront)
}

// ------------------------------------------------------------- //
// ---------------------------- IIFE --------------------------- //
// ------------------------------------------------------------- //

// Small schedule to allow messagehandler to initialize
($.Schedule(0.01, function() {
    GameEvents.Subscribe( "set_black_screen", OnSetFadeToBlack );
}))

from PIL import Image
import os
import math

SIZE = 256

def pixel_opacity(pt):

    origin = (0, -1)

    angle = 45

    dist = euclid_dist(origin, pt)
    closeEnough = (dist < 2) and (dist > 0.1)

    # The topleft edge of the segment is the left-most point possible
    # same for rightmost
    # We just have to check that |x| < abs(leftmost)

    leftmost = 2*math.sin(math.radians(angle/2)) * (dist/2)
    withinAngle = abs(pt[0]) < leftmost

    if closeEnough and withinAngle:
        return 1
    return 0



def euclid_dist(p1, p2):
    return math.sqrt( (p1[0]-p2[0])**2 + (p1[1]-p2[1])**2 )

def main():
    img = Image.new('RGBA', (SIZE, SIZE))
    pixels = img.load()

    for x in range(SIZE):
        for y in range(SIZE):
            norm_pt = (2*x/SIZE-1, -2*y/SIZE+1)
            opacity = pixel_opacity(norm_pt)
            if opacity < 0:
                opacity = 0
            if opacity > 1:
                opacity = 1
            pixels[x,y] = (255, 255, 255, opacity*255)

    img.save("output/"+os.path.basename(__file__)[:-3]+".png")

if __name__ == '__main__':
    main()
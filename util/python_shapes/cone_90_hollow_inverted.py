from PIL import Image
import os
import math

SIZE = 256
OPACITY_DIST = 0.2

def pixel_opacity(pt):

    origin = (0, 0)
    angle = 90
    radius = 1

    dist = euclid_dist(origin, pt)
    closeEnough = (dist < radius) and (dist > 0.1)

    # The topleft edge of the segment is the left-most point possible
    # same for rightmost
    # We just have to check that |x| < abs(leftmost)

    leftmost = radius*math.sin(math.radians(angle)) * (dist/2)
    withinAngle = (abs(pt[0]) < leftmost) and (pt[1] >= 0)

    if closeEnough and withinAngle:
        innerOpacity = linear_scale_function(0.1, OPACITY_DIST)(dist)
        outerOpacity = linear_scale_function(radius, OPACITY_DIST)(dist)
        edgeOpacity = linear_scale_function(leftmost, OPACITY_DIST)(abs(pt[0]))

        opacitySum = (innerOpacity + outerOpacity + edgeOpacity)/1.5
        return (1-opacitySum)
    return 0

def linear_scale_function(origin, max_dist):
    def output(x):
        dist = abs(origin-x)
        scaled = dist/max_dist
        result = 1-scaled
        if result < 0:
            return 0
        if result > 1:
            return 1
        return result
    return output

def euclid_dist(p1, p2):
    return math.sqrt( (p1[0]-p2[0])**2 + (p1[1]-p2[1])**2 )

def main():
    img = Image.new('RGBA', (SIZE, SIZE))
    pixels = img.load()

    for x in range(SIZE):
        for y in range(SIZE):
            norm_pt = (2*x/SIZE-1, -2*y/SIZE+1)
            opacity = pixel_opacity(norm_pt)
            if opacity < 0:
                opacity = 0
            if opacity > 1:
                opacity = 1
            pixels[x,y] = (255, 255, 255, math.floor(opacity*255))

    img.save("output/"+os.path.basename(__file__)[:-3]+".png")

if __name__ == '__main__':
    main()
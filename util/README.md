# Util

Contains some utility scripts used in creation of the mod

* `python_shapes` contains a family of python scripts for drawing simple images (for example, to be used as opacity masks or particle sprites)
* `resize_icons` contains a simple python script for resizing icons (for example, to reduce from source quality when it is too large for speed reasons)
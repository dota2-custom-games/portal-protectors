import os
from PIL import Image

# Util used for icons
# For all images in the folder, makes sure that their smallest height/width is
# At most 128px, preserving aspect ratio

def isImageFile(file):
    if not os.path.isfile(file):
        return False
    if (file.endswith(".png") or
        file.endswith(".jpg")
    ):
        return True

    return False


def main():
    LOCAL_PATH = "./"
    localEntries = os.listdir(LOCAL_PATH)
    for entry in localEntries:
        if isImageFile(entry):
            img = Image.open(entry)
            madeChange = False
            while (img.size[1] > 128 or img.size[0] > 128):
                madeChange = True
                img = img.resize( (int(img.size[0]/2), int(img.size[1]/2)), Image.ANTIALIAS)
            if madeChange:
                img.save(entry)

if __name__ == '__main__':
    main()
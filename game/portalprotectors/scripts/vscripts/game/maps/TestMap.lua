if not TestMap then
    TestMap = {}
    TestMap.__index = TestMap
    Log:addLogFunctions(TestMap, "TestMap")
    Map:init(TestMap)
end

-- Loads all minigames built in the TestMap
-- @return list of minigames
function TestMap:loadMinigames()
    local map = Map:new()

    local minigames = {
        TestMinigame
    }

    return minigames
end

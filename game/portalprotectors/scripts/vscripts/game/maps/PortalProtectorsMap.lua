if not PortalProtectorsMap then
    PortalProtectorsMap = {}
    PortalProtectorsMap.__index = PortalProtectorsMap
    Log:addLogFunctions(PortalProtectorsMap, "PortalProtectorsMap")
    Map:init(PortalProtectorsMap)
end

-- Loads all minigames built in the Portal Protectors map
-- @return list of minigames
function PortalProtectorsMap:loadMinigames()
    local map = Map:new()

    local minigames = {
        Prologue,
        TarPits,
        SpiderHunt,
        SpaceShips,
        ScifiTD,
        AxeFarm,
        DotaDodging,
        Tanks,
        PixelMaze,
        Epilogue
    }

    return minigames
end

function PortalProtectorsMap:loadMinigameManagerConfig()
    local gameInfo = {
        prologue = Prologue,
        epilogue = Epilogue,
        zones = {
            {
                name = "Fantasy",
                minigames = {
                    TarPits,
                    SpiderHunt,
                    AxeFarm,
                    DotaDodging
                }
            },
            {
                name = "Scifi",
                minigames = {
                    SpaceShips,
                    ScifiTD,
                    Tanks,
                    PixelMaze
                }
            }
            -- {
            --     name = "Fantasy",
            --     minigames = {
            --         TarPits,
            --         SpiderHunt,
            --     }
            -- },
            -- {
            --     name = "Dota",
            --     minigames = {
            --         DotaDodging,
            --         AxeFarm
            --     }
            -- },
            -- {
            --     name = "Scifi",
            --     minigames = {
            --         SpaceShips,
            --         ScifiTD
            --     }
            -- },
            -- {
            --     name = "Arcade",
            --     minigames = {
            --         PixelMaze,
            --         Tanks
            --     }
            -- },
        }
    }

    local gamesRemaining = 5 -- Arbitrary
    local zonesRemaining = (#gameInfo.zones)
    for _,zone in pairs(gameInfo.zones) do
        RandomizeList(zone.minigames)
        local gamesThisZone = RandomInt(math.floor(gamesRemaining/zonesRemaining),
                                        math.ceil(gamesRemaining/zonesRemaining))
        for k,_ in pairs(zone.minigames) do
            if k > gamesThisZone then
                zone.minigames[k] = nil
            end
        end
    end

    return gameInfo
end

-- Represents the map file being loaded and contains some helper methods to extract information
-- from the map file, like regions

if not Map then
    Map = {}
    Map.__index = Map
    Log:addLogFunctions(Map, "Map")
end

function Map:new()
    o = {}
    setmetatable(o, self)
    self.__index = self

    return o
end

-- Adds the helper functions to the pass in map file
function Map:init(map)
    map.findRegion = Map.findRegion
    map.findRegionsOfParent = Map.findRegionsOfParent
end

-- MARK: Static Methods

-- Finds the region with the provided name or throws an exception if it's not there
function Map:findRegion(regionName)
    local region = Entities:FindByName(nil, regionName)
    if not region then
        self:loge("Could not find a region named: " .. tostring(regionName) .. " in map " .. tostring(GetMapName()))
    end
    return region
end

-- Finds a region with the provided name and returns its children. An exception is thrown if either the parent
-- is null or there are no child regions
function Map:findRegionsOfParent(regionName)
    local entity = Entities:FindByName(nil, regionName)
    if not entity then
        self:loge("Could not find a region parent named: " .. tostring(regionName) .. " in map " .. tostring(GetMapName()))
        return nil
    end
    local childRegions = entity:GetChildren()
    if not childRegions or table.isEmpty(childRegions) then
        self:loge("Could not find child regions for a region parent named: " .. tostring(regionName) .. " in map " .. tostring(GetMapName()))
    end
    return childRegions
end

-- Class loads in a Minigame and manages playing that one minigame
-- This controls states and setup/cleanup for running the minigame

if not MinigameRound then
    MinigameRound = {}
    MinigameRound.__index = MinigameRound
end

MinigameRound.State = {
    INITIAL = 10001, -- Initial state before minigame has begun.
    PRE_ROUND = 10002, -- Heroes are moved to their spots but not controllable. Countdown
    ACTIVE = 10003, -- Game is fully active
    END_OF_ROUND = 10004, -- Round is over. Heroes teleported out.
    DISPOSED = 10005, -- Round disposed. No more actions are allowed. Clean up performed
}

MinigameRound.Result = {
    SUCCESS = 20001,
    FAILURE = 20002
}

MinigameRound.PRE_ROUND_BEFORE_COUNTDOWN = 3 -- Seconds to wait before the preround count
MinigameRound.POST_ROUND_BEFORE_COUNTDOWN = 3 -- Seconds to wait before the postround count

-- Create a new MinigameRound that will go through the lifecycle of a single [Minigame]
-- Config Arguments:
-- minigame, -- A [Minigame] that will be run for this round
-- onRoundComplete -- Function called when the round finishes in success/failure
function MinigameRound:new(config)
    Check:tableHasKeys(config, "Minigame - config", {
        "minigame", "onRoundComplete"
    })

    local minigame = config.minigame
    local onRoundComplete = config.onRoundComplete

    Check:tableHasKeys(minigame, "Minigame - minigame", {
        "name", "duration", "onPreGame", "onStart", "onEnd"
    })

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(minigame, onRoundComplete)

    return o
end

function MinigameRound:init(minigame, onRoundComplete)
    MinigameRound:addLogFunctions()

    self.onRoundComplete = onRoundComplete
    self.minigame = minigame
    self.id = minigame.name
    self.state = MinigameRound.State.INITIAL

    self:logv("Initializing")

    self.roundTime = 0
    self.result = nil

    -- Set up Damage Listener
    self.damageListener = function(unit, source, damageDealt, wasKilled, damageReason)
        self:onHeroDamaged(unit, source, damageDealt, wasKilled, damageReason)
    end

    -- Add in the callbacks to the minigame
    local callbacks = {}
    minigame.callbacks = callbacks
    callbacks.onSuccess = function(minigame)
        self:onSuccess()
    end
    callbacks.onFailure = function(minigame)
        self:onFailure()
    end
    callbacks.addDuration = function(duration)
        self:logv("Adding " .. tostring(duration) .. " seconds to the minigame duration")
        self.duration = self.duration + duration
    end
    callbacks.setDuration = function(duration)
        self:logv("Setting minigame duration to " .. tostring(duration) .. " seconds")
        self.duration = duration
    end
end

-- MARK: "Public" methods

function MinigameRound:onStart(gameState)
    self.gameState = gameState
    self:logv("OnStart | Gamestate: "..tostring(self.gameState))
    self:moveToPreRound()
end

function MinigameRound:dispose()
    self:moveToDisposed()
end

function MinigameRound:endRound()
    if self.state == MinigameRound.State.PRE_ROUND
        or self.state == MinigameRound.State.ACTIVE then
        self:moveToRoundEnd()
    else
        self:logw("Called to endRound() ignored as round in invalid state = " .. self:stateName())
    end
end

function MinigameRound:onPlayerRemoved(playerInfo)
    if (
        self.state == MinigameRound.State.INITIAL or
        self.state == MinigameRound.State.PRE_ROUND or
        self.state == MinigameRound.State.ACTIVE
   ) then
       self:logv("Removing hero because of disconnect")
       self.minigame:onHeroRemoved(playerInfo.hero)
   end
end

function MinigameRound:toString()
    return "MinigameRound | " .. self:extraInformation()
end

-- "Private" methods

-- MARK: State Transitions

function MinigameRound:moveToPreRound()
    if self.state ~= MinigameRound.State.INITIAL then
        self:logw("Attempted to go from Initial to Pre-Round state but was in: " .. self:stateName())
        return
    end

    self.state = MinigameRound.State.PRE_ROUND
    self:logv("Starting Pre-Round")

    GameRules:SetTimeOfDay(0.5)
    self:updateGameTimeHud(0)

    -- Set up UI elements for the minigame
    MinimapManager:setMinimapBoundaries(self.minigame.mapBounds)

    -- "Reset" the fog of war
    -- Abusing a dota glitch, don't tell Valve!
    local mode = GameRules:GetGameModeEntity()
    mode:SetUnseenFogOfWarEnabled(false)
    Timers:CreateTimer(0.01, function()
        mode:SetUnseenFogOfWarEnabled(true)
    end)

    -- Set up heroes for the minigame
    local heroes = PlayerManager:getConnectedPlayerHeroes()
    for _,hero in pairs(heroes) do
        self:setUpHero(hero)
    end
    self.minigame.heroes = heroes

    -- Move minigame into its PreGame state
    self.minigame:onPreGame()

    -- Pregame panorama initialization
    self:setUpPregamePanorama()

    -- Lock all heroes
    -- TODO: Minigames might want to have heroes usable during this time
    -- This should be a bit more fine-grained than this
    Minigame:setAllHeroesToState(self.minigame, HeroStateManager.HeroState.INACTIVE)

    Timers:CreateTimer(MinigameRound.PRE_ROUND_BEFORE_COUNTDOWN, function()

        -- Start the countdown
        local countdown = self.minigame.pregameTime
        Timers:CreateTimer(0, function()
            if self.state == MinigameRound.State.PRE_ROUND then
                if countdown > 0 then
                    self:updatePreGameCountdown(tostring(countdown))
                    -- TODO: Play sound?
                    -- EmitGlobalSound("Snowball.RoundCountDown")
                    countdown = countdown - 1
                    return 1
                else
                    self:moveToRoundStart()
                    -- TODO: Play sound?
                    -- EmitGlobalSound("Snowball.RoundHorn")
                    -- EmitGlobalSound("Snowball.Tusk.RoundStart")
                end
            end
        end)
    end)

    -- Reset stats for the new round
    -- self:logv("Reseting player stats")
    -- g_StatsManager:resetAll()
end

function MinigameRound:moveToRoundStart()
    if self.state ~= MinigameRound.State.PRE_ROUND then
        self:logw("Attempted to move from Pre-Round to Active state but was in: " .. self:stateName())
        return
    end

    self.state = MinigameRound.State.ACTIVE
    self:logi("Starting Round")

    Minigame:setAllHeroesToState(self.minigame, HeroStateManager.HeroState.ACTIVE)

    self:updatePreGameCountdown("")

    self:startRoundTime()

    self.minigame:onStart()
end

function MinigameRound:moveToRoundEnd()
    if self.state ~= MinigameRound.State.PRE_ROUND and self.state ~= MinigameRound.State.ACTIVE then
        self:logw("Attempted to move from Round End state but was in: " .. self:stateName())
        return
    end

    local previousState = self.state
    self.state = MinigameRound.State.END_OF_ROUND
    self:logi("Ending Round")

    self.minigame:onEnd()

    Minigame:setAllHeroesToState(self.minigame, HeroStateManager.HeroState.INACTIVE)
    -- TODO: "Teleport" heroes out
    -- TODO: Play sound. "Teleport" heroes
    -- EmitGlobalSound("crowd.lv_03")
    -- Timers:CreateTimer(0.5, function()
    --     EmitGlobalSound("Snowball.Tusk.RoundEnd")
    -- end)

    if
        previousState == MinigameRound.State.PRE_ROUND or
        previousState == MinigameRound.State.ACTIVE
    then
        Timers:CreateTimer(MinigameRound.POST_ROUND_BEFORE_COUNTDOWN, function()

            self:setUpPostgamePanorama()
            -- Start the countdown
            local countdown = self.minigame.postgameTime
            Timers:CreateTimer(0, function()
                if self.state == MinigameRound.State.END_OF_ROUND then
                    if countdown > 0 then
                        self:updatePostGameCountdown(tostring(countdown))
                        -- TODO: Play sound?
                        -- EmitGlobalSound("Snowball.RoundCountDown")
                        countdown = countdown - 1
                        return 1
                    else
                        self:tearDownPostgamePanorama()

                        -- Alert the manager that the round is over
                        if self.onRoundComplete then
                            self.onRoundComplete(self, self.result)
                        end
                    end
                end
            end)
        end)
    end
end

function MinigameRound:moveToDisposed()
    if self.state == MinigameRound.State.DISPOSED then
        self:logw("Attempted to dispose round but it is already disposed")
        return
    end

    self:logv("Disposing")

    -- Ensure that the minigame is cleaned up properly. The last state wasn't end of round,
    -- things may not be cleaned up
    local shouldCallEndRound = (self.state ~= MinigameRound.State.END_OF_ROUND)

    self.state = MinigameRound.State.DISPOSED

    if shouldCallEndRound then
        self:moveToRoundEnd()
    end

    for _,hero in pairs(self.minigame.heroes) do
        self:removeHero(hero)
    end
    self.minigame:dispose()

    self.minigame = nil
    self.damageListener = nil
    self.onRoundComplete = nil

    self = nil
end

-- MARK: Minigame Callbacks

function MinigameRound:onSuccess()
    if self.state ~= MinigameRound.State.PRE_ROUND and self.state ~= MinigameRound.State.ACTIVE then
        self:logw("onSuccess() was called by the minigame, but the round is no longer ACTIVE. State=" .. self:stateName())
        return
    end

    self.result = MinigameRound.Result.SUCCESS

    self:logv("Minigame was a SUCCESS")
    self:moveToRoundEnd()
end

function MinigameRound:onFailure()
    if self.state ~= MinigameRound.State.PRE_ROUND and self.state ~= MinigameRound.State.ACTIVE then
        self:logw("onFailure() was called by the minigame, but the round is no longer ACTIVE. State=" .. self:stateName())
        return
    end

    self.result = MinigameRound.Result.FAILURE

    self:logv("Minigame was FAILED")
    self:moveToRoundEnd()
end

-- MARK: Actions

-- Called when pre round begins. Adds listeners to the hero
function MinigameRound:setUpHero(hero)
    if not hero:IsAlive() then
        hero:respawn(false, 0)
    end
    hero:addDamageListener(self.damageListener)
end

-- Called when round ends or a player leaves. Removes any listeners we attached to him
function MinigameRound:removeHero(hero)
    hero:removeDamageListener(self.damageListener)
end

-- MARK: Event Methods
--
-- Called any time any hero is dealt damage
function MinigameRound:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if
        self.state ~= MinigameRound.State.PRE_ROUND and
        self.state ~= MinigameRound.State.ACTIVE
    then
        self:logw("Player (id=" .. tostring(unit.playerInfo.playerId) .. ") was damaged but round is not active | state=" .. self:stateName())
        return
    end

    if wasKilled then
        self:logv("Player Killed! victim=" .. tostring(unit.playerInfo.playerId) .. " reason=" .. tostring(reasonCode))

        self.minigame:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)

        -- TODO: Create a kill alert?
    end
end

-- MARK: Round Time

-- Starts incrementing the round time (assumes ACTIVE mode)
-- Will broadcast out the current round time roughly every second
function MinigameRound:startRoundTime()
    if self.state ~= MinigameRound.State.ACTIVE then
        self:logw("Attempted to start round time before in ACTIVE state. State=" .. self:stateName())
        return
    end

    self.roundTime = self.minigame.duration

    local lastTime = math.floor(GetGameDuration())
    Timers:CreateTimer(0, function()
        if self.state == MinigameRound.State.ACTIVE then
            local currentTime = math.floor(GetGameDuration())
            if currentTime ~= lastTime then
                self.roundTime = self.roundTime - (currentTime - lastTime)
                lastTime = currentTime

                self:onRoundTimeChanged(self.roundTime)
            end
            return 0.5
        else
            self:logv("Stopping round time as state is no longer active. State = " .. self:stateName())
        end
    end)
end

-- Called roughly every second
function MinigameRound:onRoundTimeChanged(time)
    self:updateGameTimeHud(time)
end

function MinigameRound:updateGameTimeHud(time)
    -- TODO: Update HUD
    -- GameEventCacheBroadcaster:Send_ServerToAllClients(
    --     "UpdateRoundInfo",
    --     {
    --         hidden=false,
    --         num=self.id,
    --         time=time
    --     }
    -- )
end

function MinigameRound:updatePreGameCountdown(text)
    CustomGameEventManager:Send_ServerToAllClients("update_pregame_countdown",
        {time = text}
    )
end

function MinigameRound:updatePostGameCountdown(text)
    -- TODO: Maybe. Right now the up next screen has no countdown
    -- This is used solely to hide the screen when the timer hits 0
    CustomGameEventManager:Send_ServerToAllClients("update_postgame_countdown",
        {time = text}
    )
end

function MinigameRound:setUpPregamePanorama()
    -- TODO: Send general configuration to entire UI

    -- Set up pregame screen with necessary data
    self:updatePreGameCountdown("-")
    CustomGameEventManager:Send_ServerToAllClients("setup_pregame_screen",
        {name = self.minigame.name
        ,choiceCount = self.minigame.choiceCount
        }
    )

    -- Remove black mask element
    CustomGameEventManager:Send_ServerToAllClients("set_black_screen",
        {show = false
        ,inFront = false
        }
    )

end

function MinigameRound:setUpPostgamePanorama()
    -- TODO: Get data for HTTP request for leaderboards?

    -- TODO: Get data for "next up" from when this round was instantiated?
    local nextAction = self.gameState.actions[1]
    local upNext = nil
    if nextAction and nextAction.id then
        upNext = nextAction.id
    end

    -- Show the postgame panorama screen
    CustomGameEventManager:Send_ServerToAllClients("setup_postgame_screen",
        {name = self.minigame.name
        ,success = ((self.result == MinigameRound.Result.SUCCESS) and "true") or "false"
        --TODO: ,leaderboards = {}
        ,scoreMetric = "time"
        ,upNext = upNext
        }
    )
end

function MinigameRound:tearDownPostgamePanorama()
    -- Force timer to 0 in order to hide postgame window
    self:updatePostGameCountdown("0")

    -- Add black mask element
    CustomGameEventManager:Send_ServerToAllClients("set_black_screen",
        {show = true
        ,inFront = false
        }
    )

end

-- MARK: Utils

function MinigameRound:extraInformation()
    local minigameName = nil
    if self.minigame then
        minigameName = tostring(self.minigame.name)
    else
        minigameName = "nil"
    end
    return "[" .. minigameName .. " - " .. tostring(self.id) .. " - " .. self:stateName() .. "] "
end

-- Similar to Log:addLogFunctions(class, tag), but adds in extra information before the msg
function MinigameRound:addLogFunctions()

    local tag = "MinigameRound"
    self.logv = function(self, msg)
        Log:v(tag, self:extraInformation() .. msg)
    end
    self.logd = function(self, msg)
        Log:d(tag, self:extraInformation() .. msg)
    end
    self.logi = function(self, msg)
        Log:i(tag, self:extraInformation() .. msg)
    end
    self.logw = function(self, msg)
        Log:w(tag, self:extraInformation() .. msg)
    end
    self.loge = function(self, msg)
        Log:e(tag, self:extraInformation() .. msg)
    end

end

function MinigameRound:stateName()
    if self.state == MinigameRound.State.INITIAL then
        return "INITIAL "
    elseif self.state == MinigameRound.State.PRE_ROUND then
        return "PRE_ROUND "
    elseif self.state == MinigameRound.State.ACTIVE then
        return "ACTIVE "
    elseif self.state == MinigameRound.State.END_OF_ROUND then
        return "END_OF_ROUND "
    elseif self.state == MinigameRound.State.DISPOSED then
        return "DISPOSED "
    else
        return "UNKNOWN(" .. tostring(self.state) .. ")"
    end
end

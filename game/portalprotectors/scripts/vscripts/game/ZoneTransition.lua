--[[
    Class loads in a "Zone Transition" between several minigames
    Optionally gives results about the previous zone
    Optionally gives information about the next zone

    For now, this basically just calls panorama to trigger a pretty animation.

    TODO: Under some circumstances, a zone transition might call a pre-emptive end to the game
        (e.g. players didn't win enough games in the zone)
    TODO: Under some circumstances, a zone transition might change the upcoming games list
        (e.g. players can "veto" one of the games in the upcoming set)
        (e.g. the "final" ending is dependent on the total number of games won over the entire match)

--]]

if not ZoneTransition then
    ZoneTransition = {}
    ZoneTransition.__index = ZoneTransition
    Log:addLogFunctions(ZoneTransition, "ZoneTransition")
end

ZoneTransition.Result = {
    COMPLETE = 30001,
}


-------------------------------------------------------------
--         Required Functions from MinigameManager         --
-------------------------------------------------------------

-- Create a new ZoneTransition that will go through the lifecycle of a single zone transition
-- Config Arguments:
-- id: Name to be used for this transition (relevant for e.g. "up next" screen)
-- fromZoneData? : Information about the zone we're coming from
-- toZoneData? : Information about the zone we're going to
-- Note these fields are expected values of the upcoming games:
    -- We will check their actual completion in the onStart step by checking the gameState
function ZoneTransition:new(config)
    Check:tableHasKeys(config, "ZoneTransition", {"id", "onTransitionComplete"})

    o = {}
    setmetatable(o, self)
    self.__index = self

    local id = config.id
    local onTransitionComplete = config.onTransitionComplete
    local fromZoneData = config.fromZone
    local toZoneData = config.toZone

    -- Debug printing stuff
    local fromZoneName = "none"
    local toZoneName = "none"
    if fromZoneData then
        fromZoneName = fromZoneData.name
    end
    if toZoneData then
        toZoneName = toZoneData.name
    end
    self:logv("Initializing with id: "..config.id..", fromZone: "..fromZoneName..", toZone: "..toZoneName)

    o:init(id, fromZoneData, toZoneData, onTransitionComplete)

    return o
end

function ZoneTransition:init(id, fromZoneData, toZoneData, onTransitionComplete)
    self.id = id
    self.fromZoneData = fromZoneData
    self.toZoneData = toZoneData
    self.onTransitionComplete = onTransitionComplete

    self.duration = 0

end

function ZoneTransition:onStart(gameState)
    local payload = {}

    if self.fromZoneData then
        local fromZoneData = self:GetFromZoneDataPayload(gameState)
        payload.fromZone = fromZoneData
        self.duration = self.duration + 12
    end

    if self.toZoneData then
        local toZoneData = self:GetToZoneDataPayload(gameState)
        payload.toZone = toZoneData
        self.duration = self.duration + 10
    end

    if payload == {} then
        self:logv("onStart | Attempted to send a payload, but it was empty?")
    end

    -- Boop the UI to actually update it
    CustomGameEventManager:Send_ServerToAllClients("perform_zone_transition", payload)

    Timers:CreateTimer(self.duration, function()
        if self.onTransitionComplete then
            self.onTransitionComplete(self, ZoneTransition.Result.COMPLETE)
        end
    end)

end

-- Not really sure what to do here? But it's a required field.
function ZoneTransition:dispose()
    self = nil
end

function ZoneTransition:onPlayerRemoved(playerInfo)
end

function ZoneTransition:toString()
    return "ZoneTransition | " .. self.id
end

-------------------------------------------------------------
--                       Helpers/Util                      --
-------------------------------------------------------------

function ZoneTransition:GetFromZoneDataPayload(gameState)
    local fromZoneData = {}
    fromZoneData.zone = self.fromZoneData.name

    -- Check for completedActions with the same name as games in fromZoneData
    local completedActions = gameState.completedActions
    fromZoneData.games = {}
    local gamesPlayed = 0
    local gamesWon = 0
    for _,gameToCheck in pairs(self.fromZoneData.minigames) do
        for _,completedAction in pairs(completedActions) do
            if completedAction.id == gameToCheck.tag then
                -- Found a game to submit in the screen
                local wasVictory = (completedAction.result == MinigameRound.Result.SUCCESS)
                
                gamesPlayed = gamesPlayed + 1
                if wasVictory then
                    gamesWon = gamesWon + 1
                end
                
                local game = {}
                game.name = completedAction.id
                game.won = wasVictory
                table.insert(fromZoneData.games, game)
            end
        end
    end

    local zonePassed = (gamesWon >= (gamesPlayed-1))
    -- TODO: Do something if zone wasn't passed?
    fromZoneData.zonePassed = zonePassed

    -- Integration with leaderboards etc.
    --fromZoneData["averageRank"] = nil 
    --fromZoneData["totalRank"] = nil

    return fromZoneData
end

function ZoneTransition:GetToZoneDataPayload(gameState)
    local toZoneData = {}
    toZoneData.zone = self.toZoneData.name

    self:logv("Building ToZoneDataPayload for transition: "..self.id..", toZoneName: "..toZoneData.zone)

    -- Check for actions with the same name as games in toZoneData
    local upcomingActions = gameState.actions
    toZoneData.games = {}
    for _,gameToCheck in pairs(self.toZoneData.minigames) do
        --self:logv("GetToZoneDataPayload | Looking for a game with name: "..gameToCheck.tag)
        for _,upcomingAction in pairs(upcomingActions) do
            --self:logv("Checking against action with id: "..upcomingAction.id)
            if upcomingAction.id == gameToCheck.tag then
                
                local game = {}
                game.name = upcomingAction.id
                
                --self:logv("Adding a game to the next zone payload, id: "..upcomingAction.id)
                table.insert(toZoneData.games, game)
            end
        end
    end

    toZoneData.winsNeeded = (#(toZoneData.games))-1 -- TODO: Choose a value for this?

    self:logv("Wins needed: "..toZoneData["winsNeeded"])

    return toZoneData

end
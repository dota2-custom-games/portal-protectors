--[[
Minigame: TarPits
State: Unimplemented
Description:
    Players have to complete a maze
    Various obstacles on the maze cause increasing stacks
    of a "tarred" debuff, which slows the player down.

    Players win if they reach the end of the maze,
    lose if they run out of time or become fully tarred
--]]


if not TarPits then
    TarPits = {}
    TarPits.__index = TarPits
    TarPits.tag = "TarPits"
    Log:addLogFunctions(TarPits, TarPits.tag)
end

function TarPits:new(map)
    Check.notNil(map, "map")

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(map)

    return o
end

function TarPits:init(map)
    Log:addLogFunctions(self, "TarPits")

    self.spawnRegion = map:findRegion("minigame_tarpits_spawn")
    self.avoidRegion = map:findRegion("minigame_tarpits_danger_zone")
    self.exitRegion = map:findRegion("minigame_tarpits_exit")

    self.name = "TarPits"
    self.duration = 60
    self.minPlayers = -1
    self.maxPlayers = -1

    self.mapBounds = {
        minX = map:findRegion("tar_pits_map_bot_left"):GetAbsOrigin().x,
        maxX = map:findRegion("tar_pits_map_top_right"):GetAbsOrigin().x,
        minY = map:findRegion("tar_pits_map_bot_left"):GetAbsOrigin().y,
        maxY = map:findRegion("tar_pits_map_top_right"):GetAbsOrigin().y
    }

    Minigame:init(self)
    self:logv("init complete")
end

function TarPits:onPreGame()
    self:logv("onPreGame")
    Minigame:moveHeroesToSpawnRegion(self, self.spawnRegion)
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.FROZEN)

    -- Register damage regions
    self:registerRegionListener(
        self.avoidRegion,
        function(unit, region, isEntering)
            if unit.playerInfo and unit:IsAlive() then
                self:logv("Hero entered the Danger Zone! " .. unit.playerInfo.playerId)
                unit:killHero(HeroStateManager.DamageReasons.EXPLODED)
            end
        end
    )

    -- Register victory region
    self:registerRegionListener(
        self.exitRegion,
        function(unit, region, isEntering)
            self:logv("Unit made it to exit region. Declaring success!")
            self:endInSuccess()
        end
    )
end

function TarPits:onStart()
    self:logv("onStart")
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.ACTIVE)
end

function TarPits:onEnd()
    self:logv("onEnd")
end

function TarPits:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if wasKilled then
        self:logv("onHeroKilled")
        if self:numberOfAliveHeroes() == 0 then
            self:logv("all heroes killed. endInFailure()")
            self:endInFailure()
        end
    end
end

function TarPits:onHeroRemoved(hero)
    self:logv("onHeroRemoved")
    hero:killHero(HeroStateManager.DamageReasons.DISCONNECTED)
end

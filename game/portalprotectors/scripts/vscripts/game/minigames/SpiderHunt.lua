--[[
Minigame: SpiderHunt
State: Unimplemented
Description: Clone of the scorpion minigame in Pyramid Escape
    - Players have generic melee heroes
    - Attribute tomes periodically spawn on the ground
    - Baby spiders periodically spawn and attack the players
    - After some amount of time, a boss spider spawns
    - Players must defeat the boss spider before the time runs out
--]]


if not SpiderHunt then
    SpiderHunt = {}
    SpiderHunt.__index = SpiderHunt
    SpiderHunt.tag = "SpiderHunt"
    Log:addLogFunctions(SpiderHunt, SpiderHunt.tag)
end

function SpiderHunt:new(map)
    Check.notNil(map, "map")

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(map)

    return o
end

function SpiderHunt:init(map)
    Log:addLogFunctions(self, "SpiderHunt")

    self.spawnRegion = map:findRegion("minigame_spiderhunt_spawn")
    self.exitRegion = map:findRegion("minigame_spiderhunt_exit")

    self.name = "SpiderHunt"
    self.duration = 60
    self.minPlayers = -1
    self.maxPlayers = -1

    self.mapBounds = {
        minX = map:findRegion("spider_hunt_map_bot_left"):GetAbsOrigin().x,
        maxX = map:findRegion("spider_hunt_map_top_right"):GetAbsOrigin().x,
        minY = map:findRegion("spider_hunt_map_bot_left"):GetAbsOrigin().y,
        maxY = map:findRegion("spider_hunt_map_top_right"):GetAbsOrigin().y
    }

    Minigame:init(self)
    self:logv("init complete")
end

function SpiderHunt:onPreGame()
    self:logv("onPreGame")
    Minigame:moveHeroesToSpawnRegion(self, self.spawnRegion)
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.FROZEN)

    -- Register victory region
    self:registerRegionListener(
        self.exitRegion,
        function(unit, region, isEntering)
            self:logv("Unit made it to exit region. Declaring success!")
            self:endInSuccess()
        end
    )
end

function SpiderHunt:onStart()
    self:logv("onStart")
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.ACTIVE)
end

function SpiderHunt:onEnd()
    self:logv("onEnd")
end

function SpiderHunt:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if wasKilled then
        self:logv("onHeroKilled")
        if self:numberOfAliveHeroes() == 0 then
            self:logv("all heroes killed. endInFailure()")
            self:endInFailure()
        end
    end
end

function SpiderHunt:onHeroRemoved(hero)
    self:logv("onHeroRemoved")
    hero:killHero(HeroStateManager.DamageReasons.DISCONNECTED)
end

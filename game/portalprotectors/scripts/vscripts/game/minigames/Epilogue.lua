--[[
Minigame: Epilogue
State: Unimplemented
Description: Final game, only plays if players won enough rounds
    - Players stand on buttons which open portals (as per prologue)
    - Portals gain "positive" energy while not open
    - While open they release things that give points and lose energy
    - If they run out of energy, they start releasing things that deal damage
    - Game ends after time expires
--]]


if not Epilogue then
    Epilogue = {}
    Epilogue.__index = Epilogue
    Epilogue.tag = "Epilogue"
    Log:addLogFunctions(Epilogue, Epilogue.tag)
end

function Epilogue:new(map)
    Check.notNil(map, "map")

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(map)

    return o
end

function Epilogue:init(map)
    Log:addLogFunctions(self, "Epilogue")

    self.spawnRegion = map:findRegion("minigame_epilogue_spawn")
    self.exitRegion = map:findRegion("minigame_epilogue_exit")

    self.name = "Epilogue"
    self.duration = 60
    self.minPlayers = -1
    self.maxPlayers = -1

    self.mapBounds = {
        minX = map:findRegion("protectors_hq_map_bot_left"):GetAbsOrigin().x,
        maxX = map:findRegion("protectors_hq_map_top_right"):GetAbsOrigin().x,
        minY = map:findRegion("protectors_hq_map_bot_left"):GetAbsOrigin().y,
        maxY = map:findRegion("protectors_hq_map_top_right"):GetAbsOrigin().y
    }

    Minigame:init(self)
    self:logv("init complete")
end

function Epilogue:onPreGame()
    self:logv("onPreGame")
    Minigame:moveHeroesToSpawnRegion(self, self.spawnRegion)
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.FROZEN)

    -- Register victory region
    self:registerRegionListener(
        self.exitRegion,
        function(unit, region, isEntering)
            self:logv("Unit made it to exit region. Declaring success!")
            self:endInSuccess()
        end
    )
end

function Epilogue:onStart()
    self:logv("onStart")
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.ACTIVE)
end

function Epilogue:onEnd()
    self:logv("onEnd")
end

function Epilogue:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if wasKilled then
        self:logv("onHeroKilled")
        if self:numberOfAliveHeroes() == 0 then
            self:logv("all heroes killed. endInFailure()")
            self:endInFailure()
        end
    end
end

function Epilogue:onHeroRemoved(hero)
    self:logv("onHeroRemoved")
    hero:killHero(HeroStateManager.DamageReasons.DISCONNECTED)
end

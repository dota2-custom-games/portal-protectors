-- An example minigame used on the test map used as a template
--
-- Objective: Players spawn in on one side and must reach the other side, avoiding damage regions

if not TestMinigame then
    TestMinigame = {}
    TestMinigame.__index = TestMinigame
    TestMinigame.tag = "TestMinigame"
    Log:addLogFunctions(TestMinigame, TestMinigame.tag)
end

function TestMinigame:new(map)
    Check.notNil(map, "map")

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(map)

    return o
end

function TestMinigame:init(map)
    Log:addLogFunctions(self, "TestMinigame")

    self.spawnRegion = map:findRegion("minigame_test1_spawn")
    self.avoidRegions = map:findRegionsOfParent("minigame_test1_danger_zones")
    self.exitRegion = map:findRegion("minigame_test1_exit")

    self.name = "TestMinigame"
    self.duration = 60
    self.minPlayers = -1
    self.maxPlayers = -1

    Minigame:init(self)
    self:logv("init complete")
end

function TestMinigame:onPreGame()
    self:logv("onPreGame")
    Minigame:moveHeroesToSpawnRegion(self, self.spawnRegion)
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.FROZEN)

    -- Register damage regions
    self:registerRegionSetListener(
        Set(self.avoidRegions),
        function(unit, region, isEntering)
            if unit.playerInfo and unit:IsAlive() then
                self:logv("Hero entered the Danger Zone! " .. unit.playerInfo.playerId)
                unit:killHero(HeroStateManager.DamageReasons.EXPLODED)
            end
        end
    )

    -- Register victory region
    self:registerRegionListener(
        self.exitRegion,
        function(unit, region, isEntering)
            self:logv("Unit made it to exit region. Declaring success!")
            self:endInSuccess()
        end
    )
end

function TestMinigame:onStart()
    self:logv("onStart")
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.ACTIVE)
end

function TestMinigame:onEnd()
    self:logv("onEnd")
end

function TestMinigame:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if wasKilled then
        self:logv("onHeroKilled")
        if self:numberOfAliveHeroes() == 0 then
            self:logv("all heroes killed. endInFailure()")
            self:endInFailure()
        end
    end
end

function TestMinigame:onHeroRemoved(hero)
    self:logv("onHeroRemoved")
    hero:killHero(HeroStateManager.DamageReasons.DISCONNECTED)
end

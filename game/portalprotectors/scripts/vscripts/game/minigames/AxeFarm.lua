--[[
Minigame: AxeFarm
State: Unimplemented
Description: Game highlighting axe's various interesting abilities
    - Each player controls an Axe (the hero)
    - Treants of various shapes and sizes periodically spawn
    - Players have to use their abilities to either kite or kill the treants
--]]


if not AxeFarm then
    AxeFarm = {}
    AxeFarm.__index = AxeFarm
    AxeFarm.tag = "AxeFarm"
    Log:addLogFunctions(AxeFarm, AxeFarm.tag)
end

function AxeFarm:new(map)
    Check.notNil(map, "map")

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(map)

    return o
end

function AxeFarm:init(map)
    Log:addLogFunctions(self, "AxeFarm")

    self.spawnRegion = map:findRegion("minigame_axefarm_spawn")
    self.exitRegion = map:findRegion("minigame_axefarm_exit")

    self.name = "AxeFarm"
    self.duration = 60
    self.minPlayers = -1
    self.maxPlayers = -1

    self.mapBounds = {
        minX = map:findRegion("axe_farm_map_bot_left"):GetAbsOrigin().x,
        maxX = map:findRegion("axe_farm_map_top_right"):GetAbsOrigin().x,
        minY = map:findRegion("axe_farm_map_bot_left"):GetAbsOrigin().y,
        maxY = map:findRegion("axe_farm_map_top_right"):GetAbsOrigin().y
    }

    Minigame:init(self)
    self:logv("init complete")
end

function AxeFarm:onPreGame()
    self:logv("onPreGame")
    Minigame:moveHeroesToSpawnRegion(self, self.spawnRegion)
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.FROZEN)

    -- Register victory region
    self:registerRegionListener(
        self.exitRegion,
        function(unit, region, isEntering)
            self:logv("Unit made it to exit region. Declaring success!")
            self:endInSuccess()
        end
    )
end

function AxeFarm:onStart()
    self:logv("onStart")
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.ACTIVE)
end

function AxeFarm:onEnd()
    self:logv("onEnd")
end

function AxeFarm:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if wasKilled then
        self:logv("onHeroKilled")
        if self:numberOfAliveHeroes() == 0 then
            self:logv("all heroes killed. endInFailure()")
            self:endInFailure()
        end
    end
end

function AxeFarm:onHeroRemoved(hero)
    self:logv("onHeroRemoved")
    hero:killHero(HeroStateManager.DamageReasons.DISCONNECTED)
end

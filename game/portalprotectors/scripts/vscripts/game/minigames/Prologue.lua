--[[
Minigame: Prologue
State: Unimplemented
Description: Brief minigame to introduce players to the game
    - A random assortment of portals and buttons are arranged
    - Each button turns on some random subset of portals
    - Players have to step on the correct subset of buttons
    - Victory when all portals are lit up
    - No lose condition
    - Add some "barrel" mechanic to also stand on buttons
--]]


if not Prologue then
    Prologue = {}
    Prologue.__index = Prologue
    Prologue.tag = "Prologue"
    Log:addLogFunctions(Prologue, Prologue.tag)
end

function Prologue:new(map)
    Check.notNil(map, "map")

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(map)

    return o
end

function Prologue:init(map)
    Log:addLogFunctions(self, "Prologue")

    self.spawnRegion = map:findRegion("minigame_prologue_spawn")
    self.exitRegion = map:findRegion("minigame_prologue_exit")

    self.name = "Prologue"
    self.duration = 60
    self.minPlayers = -1
    self.maxPlayers = -1

    self.mapBounds = {
        minX = map:findRegion("protectors_hq_map_bot_left"):GetAbsOrigin().x,
        maxX = map:findRegion("protectors_hq_map_top_right"):GetAbsOrigin().x,
        minY = map:findRegion("protectors_hq_map_bot_left"):GetAbsOrigin().y,
        maxY = map:findRegion("protectors_hq_map_top_right"):GetAbsOrigin().y
    }

    Minigame:init(self)
    self:logv("init complete")
end

function Prologue:onPreGame()
    self:logv("onPreGame")
    Minigame:moveHeroesToSpawnRegion(self, self.spawnRegion)
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.FROZEN)

    -- Register victory region
    self:registerRegionListener(
        self.exitRegion,
        function(unit, region, isEntering)
            self:logv("Unit made it to exit region. Declaring success!")
            self:endInSuccess()
        end
    )
end

function Prologue:onStart()
    self:logv("onStart")
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.ACTIVE)
end

function Prologue:onEnd()
    self:logv("onEnd")
end

function Prologue:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if wasKilled then
        self:logv("onHeroKilled")
        if self:numberOfAliveHeroes() == 0 then
            self:logv("all heroes killed. endInFailure()")
            self:endInFailure()
        end
    end
end

function Prologue:onHeroRemoved(hero)
    self:logv("onHeroRemoved")
    hero:killHero(HeroStateManager.DamageReasons.DISCONNECTED)
end

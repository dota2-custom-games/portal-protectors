--[[
Minigame: PixelMaze
State: Unimplemented
Description: Classic maze, players have to pass through avoiding damage
--]]


if not PixelMaze then
    PixelMaze = {}
    PixelMaze.__index = PixelMaze
    PixelMaze.tag = "PixelMaze"
    Log:addLogFunctions(PixelMaze, PixelMaze.tag)
end

function PixelMaze:new(map)
    Check.notNil(map, "map")

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(map)

    return o
end

function PixelMaze:init(map)
    Log:addLogFunctions(self, "PixelMaze")

    self.spawnRegion = map:findRegion("minigame_pixelmaze_spawn")
    self.exitRegion = map:findRegion("minigame_pixelmaze_exit")

    self.name = "PixelMaze"
    self.duration = 60
    self.minPlayers = -1
    self.maxPlayers = -1

    self.mapBounds = {
        minX = map:findRegion("pixel_maze_map_bot_left"):GetAbsOrigin().x,
        maxX = map:findRegion("pixel_maze_map_top_right"):GetAbsOrigin().x,
        minY = map:findRegion("pixel_maze_map_bot_left"):GetAbsOrigin().y,
        maxY = map:findRegion("pixel_maze_map_top_right"):GetAbsOrigin().y
    }

    Minigame:init(self)
    self:logv("init complete")
end

function PixelMaze:onPreGame()
    self:logv("onPreGame")
    Minigame:moveHeroesToSpawnRegion(self, self.spawnRegion)
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.FROZEN)

    -- Register victory region
    self:registerRegionListener(
        self.exitRegion,
        function(unit, region, isEntering)
            self:logv("Unit made it to exit region. Declaring success!")
            self:endInSuccess()
        end
    )
end

function PixelMaze:onStart()
    self:logv("onStart")
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.ACTIVE)
end

function PixelMaze:onEnd()
    self:logv("onEnd")
end

function PixelMaze:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if wasKilled then
        self:logv("onHeroKilled")
        if self:numberOfAliveHeroes() == 0 then
            self:logv("all heroes killed. endInFailure()")
            self:endInFailure()
        end
    end
end

function PixelMaze:onHeroRemoved(hero)
    self:logv("onHeroRemoved")
    hero:killHero(HeroStateManager.DamageReasons.DISCONNECTED)
end

--[[
Minigame: Tanks
State: Unimplemented
Description: Players use tanks to blow up enemies
    - Tank chassis and turret are controlled separately (rotation etc)
    - Players have to defend a central base
    - Increasingly complex enemies spawn over time (e.g. artillery)
--]]


if not Tanks then
    Tanks = {}
    Tanks.__index = Tanks
    Tanks.tag = "Tanks"
    Log:addLogFunctions(Tanks, Tanks.tag)
end

function Tanks:new(map)
    Check.notNil(map, "map")

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(map)

    return o
end

function Tanks:init(map)
    Log:addLogFunctions(self, "Tanks")

    self.spawnRegion = map:findRegion("minigame_tanks_spawn")
    self.exitRegion = map:findRegion("minigame_tanks_exit")

    self.name = "Tanks"
    self.duration = 60
    self.minPlayers = -1
    self.maxPlayers = -1

    self.mapBounds = {
        minX = map:findRegion("tanks_map_bot_left"):GetAbsOrigin().x,
        maxX = map:findRegion("tanks_map_top_right"):GetAbsOrigin().x,
        minY = map:findRegion("tanks_map_bot_left"):GetAbsOrigin().y,
        maxY = map:findRegion("tanks_map_top_right"):GetAbsOrigin().y
    }

    Minigame:init(self)
    self:logv("init complete")
end

function Tanks:onPreGame()
    self:logv("onPreGame")
    Minigame:moveHeroesToSpawnRegion(self, self.spawnRegion)
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.FROZEN)

    -- Register victory region
    self:registerRegionListener(
        self.exitRegion,
        function(unit, region, isEntering)
            self:logv("Unit made it to exit region. Declaring success!")
            self:endInSuccess()
        end
    )
end

function Tanks:onStart()
    self:logv("onStart")
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.ACTIVE)
end

function Tanks:onEnd()
    self:logv("onEnd")
end

function Tanks:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if wasKilled then
        self:logv("onHeroKilled")
        if self:numberOfAliveHeroes() == 0 then
            self:logv("all heroes killed. endInFailure()")
            self:endInFailure()
        end
    end
end

function Tanks:onHeroRemoved(hero)
    self:logv("onHeroRemoved")
    hero:killHero(HeroStateManager.DamageReasons.DISCONNECTED)
end

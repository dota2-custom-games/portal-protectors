--[[
Minigame: DotaDodging
State: Unimplemented
Description: Players dodge traps using abilities from Dota heroes
    - Arena with areas for traps and "shrines"
    - Shrines periodically glow with the face of a Dota hero
    - If a player moves to the shrine, they gain a single-use ability
    - E.g. timbersaw gives grapple, puck gives phase shift, ...
    - Meanwhile a bunch of random traps go off
--]]


if not DotaDodging then
    DotaDodging = {}
    DotaDodging.__index = DotaDodging
    DotaDodging.tag = "DotaDodging"
    Log:addLogFunctions(DotaDodging, DotaDodging.tag)
end

function DotaDodging:new(map)
    Check.notNil(map, "map")

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(map)

    return o
end

function DotaDodging:init(map)
    Log:addLogFunctions(self, "DotaDodging")

    self.spawnRegion = map:findRegion("minigame_dotadodging_spawn")
    self.exitRegion = map:findRegion("minigame_dotadodging_exit")

    self.name = "DotaDodging"
    self.duration = 60
    self.minPlayers = -1
    self.maxPlayers = -1

    self.mapBounds = {
        minX = map:findRegion("dota_dodging_map_bot_left"):GetAbsOrigin().x,
        maxX = map:findRegion("dota_dodging_map_top_right"):GetAbsOrigin().x,
        minY = map:findRegion("dota_dodging_map_bot_left"):GetAbsOrigin().y,
        maxY = map:findRegion("dota_dodging_map_top_right"):GetAbsOrigin().y
    }

    Minigame:init(self)
    self:logv("init complete")
end

function DotaDodging:onPreGame()
    self:logv("onPreGame")
    Minigame:moveHeroesToSpawnRegion(self, self.spawnRegion)
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.FROZEN)

    -- Register victory region
    self:registerRegionListener(
        self.exitRegion,
        function(unit, region, isEntering)
            self:logv("Unit made it to exit region. Declaring success!")
            self:endInSuccess()
        end
    )
end

function DotaDodging:onStart()
    self:logv("onStart")
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.ACTIVE)
end

function DotaDodging:onEnd()
    self:logv("onEnd")
end

function DotaDodging:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if wasKilled then
        self:logv("onHeroKilled")
        if self:numberOfAliveHeroes() == 0 then
            self:logv("all heroes killed. endInFailure()")
            self:endInFailure()
        end
    end
end

function DotaDodging:onHeroRemoved(hero)
    self:logv("onHeroRemoved")
    hero:killHero(HeroStateManager.DamageReasons.DISCONNECTED)
end

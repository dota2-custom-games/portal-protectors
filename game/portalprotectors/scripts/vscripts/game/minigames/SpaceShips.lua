--[[
Minigame: SpaceShips
State: Unimplemented
Description: Classic arcade-style spaceship thing (e.g. Asteroids)
    - Players control a spaceship (from a choice of multiple?)
    - Ice-style physics
    - Enemy ships periodically spawn, getting stronger over time
    - Players pick up powerups from fallen enemies
    - Player death makes them lose their powerups and respawn after a delay
    - Players have to score some point quota before the game ends
--]]

if not SpaceShips then
    SpaceShips = {}
    SpaceShips.__index = SpaceShips
    SpaceShips.tag = "SpaceShips"
    Log:addLogFunctions(SpaceShips, SpaceShips.tag)
end

function SpaceShips:new(map)
    Check.notNil(map, "map")

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(map)

    return o
end

function SpaceShips:init(map)
    Log:addLogFunctions(self, "SpaceShips")

    self.spawnRegion = map:findRegion("minigame_spaceships_spawn")
    self.exitRegion = map:findRegion("minigame_spaceships_exit")

    self.name = "SpaceShips"
    self.duration = 60
    self.minPlayers = -1
    self.maxPlayers = -1

    self.mapBounds = {
        minX = map:findRegion("spaceships_map_bot_left"):GetAbsOrigin().x,
        maxX = map:findRegion("spaceships_map_top_right"):GetAbsOrigin().x,
        minY = map:findRegion("spaceships_map_bot_left"):GetAbsOrigin().y,
        maxY = map:findRegion("spaceships_map_top_right"):GetAbsOrigin().y
    }

    Minigame:init(self)
    self:logv("init complete")
end

function SpaceShips:onPreGame()
    self:logv("onPreGame")
    Minigame:moveHeroesToSpawnRegion(self, self.spawnRegion)
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.FROZEN)

    -- Register victory region
    self:registerRegionListener(
        self.exitRegion,
        function(unit, region, isEntering)
            self:logv("Unit made it to exit region. Declaring success!")
            self:endInSuccess()
        end
    )
end

function SpaceShips:onStart()
    self:logv("onStart")
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.ACTIVE)
end

function SpaceShips:onEnd()
    self:logv("onEnd")
end

function SpaceShips:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if wasKilled then
        self:logv("onHeroKilled")
        if self:numberOfAliveHeroes() == 0 then
            self:logv("all heroes killed. endInFailure()")
            self:endInFailure()
        end
    end
end

function SpaceShips:onHeroRemoved(hero)
    self:logv("onHeroRemoved")
    hero:killHero(HeroStateManager.DamageReasons.DISCONNECTED)
end

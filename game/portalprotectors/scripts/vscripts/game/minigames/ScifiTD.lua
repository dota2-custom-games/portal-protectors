--[[
Minigame: ScifiTD
State: Unimplemented
Description: Generic tower defense
--]]


if not ScifiTD then
    ScifiTD = {}
    ScifiTD.__index = ScifiTD
    ScifiTD.tag = "ScifiTD"
    Log:addLogFunctions(ScifiTD, ScifiTD.tag)
end

function ScifiTD:new(map)
    Check.notNil(map, "map")

    o = {}
    setmetatable(o, self)
    self.__index = self

    o:init(map)

    return o
end

function ScifiTD:init(map)
    Log:addLogFunctions(self, "ScifiTD")

    self.spawnRegion = map:findRegion("minigame_scifitd_spawn")
    self.exitRegion = map:findRegion("minigame_scifitd_exit")

    self.name = "ScifiTD"
    self.duration = 60
    self.minPlayers = -1
    self.maxPlayers = -1

    self.mapBounds = {
        minX = map:findRegion("scifi_td_map_bot_left"):GetAbsOrigin().x,
        maxX = map:findRegion("scifi_td_map_top_right"):GetAbsOrigin().x,
        minY = map:findRegion("scifi_td_map_bot_left"):GetAbsOrigin().y,
        maxY = map:findRegion("scifi_td_map_top_right"):GetAbsOrigin().y
    }

    Minigame:init(self)
    self:logv("init complete")
end

function ScifiTD:onPreGame()
    self:logv("onPreGame")
    Minigame:moveHeroesToSpawnRegion(self, self.spawnRegion)
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.FROZEN)

    -- Register victory region
    self:registerRegionListener(
        self.exitRegion,
        function(unit, region, isEntering)
            self:logv("Unit made it to exit region. Declaring success!")
            self:endInSuccess()
        end
    )
end

function ScifiTD:onStart()
    self:logv("onStart")
    Minigame:setAllHeroesToState(self, HeroStateManager.HeroState.ACTIVE)
end

function ScifiTD:onEnd()
    self:logv("onEnd")
end

function ScifiTD:onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
    if wasKilled then
        self:logv("onHeroKilled")
        if self:numberOfAliveHeroes() == 0 then
            self:logv("all heroes killed. endInFailure()")
            self:endInFailure()
        end
    end
end

function ScifiTD:onHeroRemoved(hero)
    self:logv("onHeroRemoved")
    hero:killHero(HeroStateManager.DamageReasons.DISCONNECTED)
end

-- This file acts similar to TestMinigameManager but does not cycle through anything
-- All minigames must be loaded with Debug commands

if not TestMinigameManager then
    TestMinigameManager = {}
    TestMinigameManager.__index = TestMinigameManager
    Log:addLogFunctions(TestMinigameManager, "TestMinigameManager")
end

function TestMinigameManager:new(o)
    setmetatable(o, self)
    self.__index = self

    Check:tableHasKeys(o, "TestMinigameManager", {"minigames"})

    self.minigames = o.minigames

    self.activeRound = nil

    return o
end

-- MARK: GameMode lifecycle methods

function TestMinigameManager:onStart()
    self:logv("onStart")

    -- Reveal the entire map
    GameRules:GetGameModeEntity():SetFogOfWarDisabled(true)

    -- Disable day/night time progression
    cvar_setf("dota_time_of_day_rate", 0.0000001)
end

-- Called when a player was added to the game (created/reconnected)
function TestMinigameManager:onPlayerAdded(playerInfo)
end

-- Called when a player was removed from the game (disconnected/kicked)
function TestMinigameManager:onPlayerRemoved(playerInfo)
    if self.activeRound then
        self.activeRound:removePlayer(playerInfo.hero)
    end
end

-- MARK: "Public" methods

-- MARK: "Private" methods

-- MARK: MinigameRounds

-- Constructs and returns a new MinigameRound object
-- Tries to pick a new arena that is not the same as the previous one
function TestMinigameManager:createRound(minigameClass)
    local minigame = minigameClass:new(TestMap)
    local round = MinigameRound:new({
        minigame = minigame,
        onRoundComplete = function(round) self:onRoundComplete(round) end
    })
    return round
end

function TestMinigameManager:startActiveRound()
    self.activeRound:onStart()
end

-- Event listener called when the active round ends
function TestMinigameManager:onRoundComplete(round)
    if self.activeRound ~= round then
        print("WARNING | Received onRoundComplete from round=" .. tostring(round.id) .. " but active round is: " .. tostring(self.activeRound.id))
        return
    end
    self:logv("onRoundComplete")
    round:dispose()

    for _,hero in pairs(PlayerManager:getPlayerHeroes()) do
        if not hero:IsAlive() then
            hero:respawn(true)
        end
        FindClearSpaceForUnit(hero, Vector(0,0,0) + RandomVector(100), true)
        hero:SetForwardVector(RandomVector(1.0))
        hero:focusCameraOnUnit()
    end
end

-- This file controls the loading of MinigameRounds, RoundIntermissions and other overall game states

-- Setup:
--      MinigameManager requires a Map file that has configured the minigames into specific groups:
--      {
--          prologue = Prologue,
--          epilogue = Epilogue,
--          zones = {
--              {
--                  name = "Fantasy",
--                  minigames = {
--                      TarPits,
--                      SpiderHunt,
--                      AxeFarm,
--                      DotaDodging
--                  }
--              },
--              ... etc
--          }
--      }
--
--      prologue: The first game that will always be run
--      zone: A list of as many zone that should be loaded
--               A zone will launch an intermission screen when starting, listing all of the zones
--               Then each game will be played in some random order
--               Then a final intermission showing results
--      epilogue: The final game to be played

-- Process:
-- The MinigameManager will use the setup to construct a (mutable) list of "Action"s that effectively
-- play through the entire game. An action may be playing a minigame, displaying a vote, or the end
-- of the game.

if not MinigameManager then
    MinigameManager = {}
    MinigameManager.__index = MinigameManager
    Log:addLogFunctions(MinigameManager, "MinigameManager")
end

-- Slight delay between actions to make fades prettier
MinigameManager.TIME_BETWEEN_ACTIONS = 1.5

function MinigameManager:new(o)
    setmetatable(o, self)
    self.__index = self
    Check:tableHasKeys(o, "MinigameManager", {"map"})

    local map = o.map
    if not map.loadMinigameManagerConfig then
        error("Map passed in must have a loadMinigameManagerConfig()!!")
    end

    local minigameManagerConfig = map:loadMinigameManagerConfig()
    Check:tableHasKeys(minigameManagerConfig, "MinigameManager", {
        "prologue",
        "epilogue",
        "zones"
    })

    o:init(minigameManagerConfig)
    return o
end

function MinigameManager:init(config)
    self.prologue = config.prologue
    self.epilogue = config.epilogue
    self.zones = config.zones

    local zoneInfoLog = ""
    for _,zone in pairs(self.zones) do
        zoneInfoLog = zoneInfoLog .. "\n    " .. zone.name .. "\n        "
        for _,minigame in pairs(zone.minigames) do
            zoneInfoLog = zoneInfoLog .. " " .. minigame.tag
        end
    end
    self:logv(
        "Initializing\n" ..
        "Prologue: " .. self.prologue.tag .. "\n" ..
        "Zones: " .. zoneInfoLog .. "\n" ..
        "Epilogue: " .. self.epilogue.tag
    )

    self.currentAction = nil
    self.state = {}
end

-- MARK: GameMode lifecycle methods

function MinigameManager:onStart()
    self:logv("onStart")

    -- Disable day/night time progression
    cvar_setf("dota_time_of_day_rate", 0.0000001)

    -- Set the initial state
    self.state = {
        actions = {},
        completedActions = {}
    }

    -- Create and start the actions
    self:constructAllActions()
end

function MinigameManager:onStop()
    self:logv("onStop | Canceling current action and removing all actions.")
    self.state.actions = nil
    if self.currentAction then
        self:endCurrentAction()
    end
end

-- Called when a player was added to the game (created/reconnected)
function MinigameManager:onPlayerAdded(playerInfo)
    -- TODO: Maybe update HUD. Should probably not add to the active minigame
    if self.currentAction and self.currentAction.onPlayerAdded then
        self.currentAction:onPlayerAdded(playerInfo)
    end
end

-- Called when a player was removed from the game (disconnected/kicked)
function MinigameManager:onPlayerRemoved(playerInfo)
    if self.currentAction and self.currentAction.onPlayerRemoved then
        self.currentAction:onPlayerRemoved(playerInfo)
    end
end

-- MARK: "Public" methods

-- MARK: "Private" methods

-- MARK: Actions

function MinigameManager:startNextAction()
    if self.currentAction then  
        self:endCurrentAction()
    end

    local nextAction = self.state.actions[1]
    if not nextAction then
        self:logi("No more actions to process.")
    else
        table.remove(self.state.actions, 1)
        self.currentAction = nextAction
        self:logi("Starting next action: " .. nextAction:toString()..", sending state: "..tostring(self.state))
        nextAction:onStart(self.state)
    end

end

function MinigameManager:endCurrentAction()
    if not self.currentAction then
        self:logw("No current action to end. Ignoring endCurrentAction()")
        return
    end
    self.currentAction:dispose()

    self.currentAction = nil
end

function MinigameManager:onActionComplete(action, result)
    if not self.currentAction then
        self:logw("onActionComplete called with no currentAction?")
        return
    end

    self:logv("Action Completed: " .. self.currentAction:toString()..", Id: "..tostring(action.id)..", Result: "..tostring(result))
    table.insert(self.state.completedActions,
        {id = action.id,
         result = result
        }
    )

    self.currentAction = nil

    Timers:CreateTimer(MinigameManager.TIME_BETWEEN_ACTIONS, function() 
        self:startNextAction()
    end)
end

function MinigameManager:addAction(action)
    Check:notNil(action, "action")
    Check:tableHasKeys(o, "Action", {"onStart", "dispose"})

    if not self.currentAction then
        self:logi("Starting newly added action as it is the only action in the queue.")
        self.currentAction = action
        action:onStart(self.state)
    else
        self:logv("Adding action to queue: " .. action:toString())
        table.insert(self.state.actions, action)
    end
end

-- MARK: Action Sequence Construction

function MinigameManager:constructAllActions()
    self:logv("Construcing, adding, and starting actions for MinigameManager")

    local zoneInfoLog = ""
    for _,zone in pairs(self.zones) do
        zoneInfoLog = zoneInfoLog .. "\n    " .. zone.name .. "\n        "
        for _,minigame in pairs(zone.minigames) do
            zoneInfoLog = zoneInfoLog .. " " .. minigame.tag
        end
    end

    -- TODO: Add in more actions like votes

    self:addAction(
        self:createMinigameAction(self.prologue)
    )
    RandomizeList(self.zones)
    local lastZone = nil
    for _,zone in pairs(self.zones) do

        self:addAction(
            self:createZoneTransitionAction(zone.name, lastZone, zone)
        )

        RandomizeList(zone.minigames)
        for _,minigame in pairs(zone.minigames) do
            self:addAction(
                self:createMinigameAction(minigame)
            )
        end

        lastZone = zone

    end

    self:addAction(
        self:createZoneTransitionAction("epilogue", lastZone, nil)
    )

    self:addAction(
        self:createMinigameAction(self.epilogue)
    )
end

-- MARK: Actions
-- An Action is an object that runs some process. They are queue'd up in the MinigameManager
-- and executed in order. An action will be handed the current state of the game, which
-- includes the list of actions, meaning actions can mutate the remaining actions. They are also
-- passed a function onComplete, that must be called once the action is done so that minigame
-- manager can execute the next action.

-- Constructs and returns a new MinigameRound object
function MinigameManager:createMinigameAction(minigameClass)
    local minigame = minigameClass:new(PortalProtectorsMap)
    local round = MinigameRound:new({
        minigame = minigame,
        onRoundComplete = function(round, result)
            if self.currentAction ~= round then
                print("WARNING | Received onRoundComplete from round=" .. tostring(round.id) .. " but current action is: " .. tostring(self.currentAction.id))
            else
                self:onActionComplete(round, result)
            end
        end
    })
    return round
end

-- Constructs and returns a new ZoneTransition object
-- @param id: ID that this transition will take
-- @param fromZone? : information about the zone we will be leaving
-- @param toZone? : information about the zone we will be entering
function MinigameManager:createZoneTransitionAction(id, fromZone, toZone)
    local config = {}
    config.id = id
    config.fromZone = fromZone
    config.toZone = toZone
    local zone = ZoneTransition:new({
        id = id,
        fromZone = fromZone,
        toZone = toZone,
        onTransitionComplete = function(zone, result)
            if self.currentAction ~= zone then
                print("WARNING | Received onTransitionComplete from id=" .. tostring(zone.id) .. " but current action is: " .. tostring(self.currentAction.id))
            else
                self:onActionComplete(zone, result)
            end
        end
    })
    return zone
end

if not PlaygroundManager then
    PlaygroundManager = {}
end


function PlaygroundManager:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    self.playerID = nil
    self.heroUnit = nil

    return o

end

------------------------------------------------------------------
--                   Calls from Gamemanager                     --
------------------------------------------------------------------

function PlaygroundManager:onPlayerHeroInGame()
end

function PlaygroundManager:onStart()
    -- Assign some local variables
    self:FetchHeroUnit()

    -- Run the tests
    self:StartTests()
end

function PlaygroundManager:onPlayerRemoved()

end

function PlaygroundManager:onPlayerReconnect()

end

------------------------------------------------------------------
--                       Main Functionality                     --
------------------------------------------------------------------


--[[
    Main "Work" function, runs a bunch of interesting tests
--]]
function PlaygroundManager:StartTests()
    print("PlaygroundManager | Initialize!")


    -- Spawns a test builder unit for figuring out BuildingHelper
    self:SpawnBuilderUnit()

    -- Spawn some items for testing the containers library
    self:SpawnItems()

    -- Make some pretty objective indicator particles
    self:TestObjectiveMarkers()

    -- Spawn Doom and make him play some animations with attack telegraphs
    self:TestAttackTelegraphs()

end

function PlaygroundManager:FetchHeroUnit()

    self.playerID = 1

    -- Create for each player 
    for _, hero in ipairs(PlayerManager:getPlayerHeroes()) do

        print("PlaygroundManager | Found hero unit")
        self.hero = hero
        CustomStats:SetUpHeroForCustomStats(hero)
        hero:SetLevelUpGains(1.6, 2.7, 3.6)
        for i=1,5 do
            hero:HeroLevelUp(true)
            end

        self.heroUnit = hero
        CustomStats:SetUpHeroForCustomStats(self.heroUnit)
        self.heroUnit:SetLevelUpGains(1.6, 2.7, 3.6)
        for i=1,5 do
            self.heroUnit:HeroLevelUp(true)
        end

        for i = 1,24 do
            local ability = self.heroUnit:GetAbilityByIndex(i)
            if ability then
                local abilityName = ability:GetName()
                if string.find(abilityName, "special_bonus") then
                    self.heroUnit:RemoveAbility(abilityName)
                end
            end
        end

        -- Mapwide vision
        self.heroUnit:SetDayTimeVisionRange(500000)
        self.heroUnit:SetNightTimeVisionRange(500000)

    end
end

-- Spawns a builder unit to test BuildingHelper
function PlaygroundManager:SpawnBuilderUnit()
    print("PlaygroundManager SpawnBuilderUnit | Spawning a test builder unit")

    local location = self.heroUnit:GetAbsOrigin() + Vector(100, 0, 0)
    local team = self.heroUnit:GetTeam()
    local builder = CreateUnitByName("test_builder", location, true, nil, nil, team)
    builder:SetOwner(self.heroUnit)
    builder:SetControllableByPlayer(self.playerID, true)

end

-- Spawns a few blinksticks just to test the containers library
function PlaygroundManager:SpawnItems()

    local blinky = CreateItem("item_blink", self.hero, self.hero) 
    CreateItemOnPositionSync(Vector(0,50,0), blinky)
    blinky = CreateItem("item_blink", self.hero, self.hero) 
    CreateItemOnPositionSync(Vector(0,100,0), blinky)
    blinky = CreateItem("item_blink", self.hero, self.hero) 
    CreateItemOnPositionSync(Vector(0,0,0), blinky)
end

-- tests the ObjectiveMarkers particle interface library
function PlaygroundManager:TestObjectiveMarkers()

    print("PlaygroundManager CreateObjectiveParticle | Creating test objective particle!")

    print("Herounit is: "..tostring(self.heroUnit))

    -- Looping colour changing particle   
    --[[ 
    local blueMarker = self:CreateObjectiveMarker(Vector(-450,-450,0), Vector(-100, -100, 0), Vector(0,0,128), 0)

    local oldColor = Vector(128, 0, 0)
    local newColor = Vector(0, 128, 0)
    local temp = nil

    Timers:CreateTimer(5, function()
        self:SetObjectiveMarkerColor(blueMarker, newColor)
        temp = newColor
        newColor = oldColor 
        oldColor = temp
        return 5
    end)
    ]]

    -- Looping creating and destroying particle
    local loopingParticle = nil

    Timers:CreateTimer(6, function() 
        if loopingParticle then
            ParticleManager:DestroyParticle(loopingParticle, false)
            ParticleManager:ReleaseParticleIndex(loopingParticle)
            loopingParticle = nil
        else
            loopingParticle = ObjectiveMarkers:CreateObjectiveMarker(Vector(-450,-450,0), Vector(0, 0, 0), Vector(128,0,128), 0)
        end
        return 6
    end)

    -- Checking if we're in the area particle
    local botleft = Vector(250,250,0)
    local topright = Vector(250,750, 0)
    local rotation =  0.78
    local unit = self.heroUnit
    local checkIfInAreaParticle = ObjectiveMarkers:CreateObjectiveMarker(botleft, topright , Vector(128,0,0),rotation)
    Timers:CreateTimer(0.1, function()
        if ObjectiveMarkers:UnitIsWithinRotatedSquare(unit, botleft, topright, rotation) then
            ObjectiveMarkers:SetObjectiveMarkerColor(checkIfInAreaParticle, Vector(0,128,0))
            ObjectiveMarkers:ImmobiliseObjectiveMarker(checkIfInAreaParticle, rotation, true)
        else
            ObjectiveMarkers:SetObjectiveMarkerColor(checkIfInAreaParticle, Vector(128,0,0))
            ObjectiveMarkers:ImmobiliseObjectiveMarker(checkIfInAreaParticle, rotation, false)
        end
        return 0.1
    end)

end

-- Tests the Attack Telegraphs particle interface library
function PlaygroundManager:TestAttackTelegraphs()

    -- Spawn Doom
    local location = Vector(1000, -1000, 300)
    local team = DOTA_TEAM_BADGUYS
    local doom = CreateUnitByName("test_doom", location, true, nil, nil, team)

    -- 4 separate loops that will never overlap to run animations

    -- "circle" attack
    Timers:CreateTimer(0, function()

        -- "Casting" animation
        StartAnimation(doom,
            {duration=1.9,
            activity=ACT_DOTA_TELEPORT,
            rate=0.75
        })
        -- "Finished" Animation
        Timers:CreateTimer(2, function() 
        StartAnimation(doom,
            {duration=2.5,
            activity=ACT_DOTA_SPAWN,
            rate=0.75,
            translate="loadout"
        })
        end)

        -- Show telegraph particle
        doom.circleParticle = AttackTelegraphs:CreateCircleIndicator(
                doom:GetAbsOrigin(), 600, false, false)
        Timers:CreateTimer(4, function()
            ParticleManager:DestroyParticle(doom.circleParticle, false)
            ParticleManager:ReleaseParticleIndex(doom.circleParticle)
            doom.circleParticle = nil
        end)

        -- Show real particle
        Timers:CreateTimer(3, function()
            AttackTelegraphs:CreateCircleIndicator(
                doom:GetAbsOrigin(), 600, true, false)
        end)        

        return 20
    end)

    -- "line" attack
    Timers:CreateTimer(5, function()
        -- "Casting" animation
        StartAnimation(doom,
            {duration=0.9,
            activity=ACT_DOTA_CAST_ABILITY_2,
            rate=1
        })
        -- "Finished" Animation
        Timers:CreateTimer(1, function() 
        StartAnimation(doom,
            {duration=2.5,
            activity=ACT_DOTA_ATTACK_STATUE,
            rate=0.6,
            translate="eyeoffetizu"
        })
        end)

        -- Show telegraph particle
        local forward = doom:GetForwardVector()
        doom.lineParticle = AttackTelegraphs:CreateLineIndicator(
                doom:GetAbsOrigin()+Vector(1.5*forward.x*doom:GetHullRadius(),1.5*forward.y*doom:GetHullRadius(),0),
                forward,
                1500, 150, false, false
        )
        Timers:CreateTimer(3, function()
            ParticleManager:DestroyParticle(doom.lineParticle, false)
            ParticleManager:ReleaseParticleIndex(doom.lineParticle)
            doom.lineParticle = nil
        end)

        -- Show real particle
        Timers:CreateTimer(1.8, function() 
        local forward = doom:GetForwardVector()
        AttackTelegraphs:CreateLineIndicator(
                doom:GetAbsOrigin()+Vector(1.5*forward.x*doom:GetHullRadius(),1.5*forward.y*doom:GetHullRadius(),0),
                forward,
                1500, 150, true, false
        )
        end)

        return 20
    end)

    -- "cone" attack
    Timers:CreateTimer(10, function()
        -- Cast animation
        StartAnimation(doom,
            {duration=2.75,
            activity=ACT_DOTA_GENERIC_CHANNEL_1,
            rate=0.75
        })
        -- Freeze with sword at top for a moment
        Timers:CreateTimer(0.2, function() 
        FreezeAnimation(doom, 0.5)
        end)

        -- Show telegraph particle
        doom.coneParticle = AttackTelegraphs:CreateConeIndicator(
            doom:GetAbsOrigin(),
            doom:GetForwardVector().x,
            90,
            1200,
            false,
            false)
        Timers:CreateTimer(3, function()
            ParticleManager:DestroyParticle(doom.coneParticle, false)
            ParticleManager:ReleaseParticleIndex(doom.coneParticle)
            doom.coneParticle = nil
        end)

        -- Show real particle
        Timers:CreateTimer(1.7, function() 
        AttackTelegraphs:CreateConeIndicator(
            doom:GetAbsOrigin(),
            doom:GetForwardVector().x,
            90,
            1200,
            true,
            false)
        end)



        return 20
    end)

    -- Move to a nearby place
    Timers:CreateTimer(15, function()
        local nearbyPoint = doom:GetAbsOrigin() + Vector(RandomInt(-100, 100),RandomInt(-100, 100),0 )
        doom:MoveToPosition(nearbyPoint)
        return 20
    end)

end

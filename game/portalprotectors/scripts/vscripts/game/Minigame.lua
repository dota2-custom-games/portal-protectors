-- This file defines the interface for a Minigame and utility methods for minigames
-- All minigames must implement the methods here
--
-- Minigame Interface:
--
--  Properties:
--    (Config - Minigames must define these)
--      name: String -- The name of the minigame, used for string localization, logging, and debug commands
--          Note: There are expected string localizations based on this name:
--              Ex: if the name property was "NAME":
--              "#minigame_title_NAME" -- Localization name for minigame.
--              "#minigame_instructions_NAME" -- Localization for minigame instructions.
--      duration: Int -- How long the minigame lasts, in seconds. Can be changed with updateDuration(int)
--      minPlayers: Int (Optional, default: 1)-- The minimum number of players that must be in the game to play this.
--      maxPlayers: Int (Optional, default: -1) -- The maximum number of players, or negative to indicate no limit
--    (AutoSet - DO NOT OVERRIDE OR MUTATE)
--      active: Boolean -- Set to true before onPreGame() called and false before onEnd()
--      heroes: List<Entity> -- List of the active heroes playing. Set before onPreGame() and updated
--              when players are removed or the game ends. This is mostly for utility methods.
--
--  Methods: (Must Implement)
--      -- Called when this minigame is selected to be the next one.
--      -- The minigame should focus the camera and move heroes to the start location.
--      -- But, the minigame should not "begin". Ex: move players but keep them in the FROZEN state or in a pen they can't escape.
--      -- New heroes will not be added during the duration of the minigame, although they may leave
--      onPreGame()
--
--      -- Called when the minigame should start. The timer for the game will begin at this point
--      onStart()
--
--      -- Called when the minigame completes, either in success/failure/no more players
--      -- Hero cleanup is handled elsewhere, so the minigame should reset its state and cancel any actions in force
--      onEnd()
--
--      -- Called when a hero in the minigame is damaged. Minigames may want to fail out if all players are dead
--      -- @param unit | Unit | The unit that was attacked
--      -- @param attacker | Unit? | The attacker unit that damaged the unit, if any
--      -- @param damage | Number | The amount of damage the unit took.
--      -- @param wasKilled | boolean | true if the unit was killed
--      -- @param reasonCode | boolean | A HeroStateManager.DamageReasons explaining what damaged him
--      onHeroDamaged(unit, attacker, damage, wasKilled, reasonCode)
--
--      -- @param hero | The hero that was removed, usually because player was disconnected/kicked
--                     | Minigames should remove the hero and perform any other necessary actions
--                     | This will NOT be the last hero in the game. In that case, the minigame always fails.
--      onHeroRemoved(hero)
--
--  Utility Methods
--  These methods are added in Minigame:init()
--      numberOfAliveHeroes(): Int -- returns the number of alive heroes still in the minigame
--      addDuration(int) -- Call to add time to the duration of the minigame (can be negative to reduce time)
--      setDuration(int) -- Call to change the duration of the minigame.
--      moveHeroesToSpawnRegion(region) -- Call to move all hero to random positions within a region
--      endInSuccess() -- Call when the minigame completes in success. onEnd() will be called after for cleanup
--      endInFailure() -- Call when the minigame completes in failure. onEnd() will be called after for cleanup
--

if not Minigame then
    Minigame = {}
    Minigame.__index = Minigame
    Log:addLogFunctions(Minigame, "Minigame")
    Minigame.StaticIdNumber = 1 -- Don't let this reset on script_reload

    Minigame.DEFAULT_MAP_BOUNDS = {
        minX = GetWorldMinX(),
        maxX = GetWorldMaxX(),
        minY = GetWorldMinY(),
        maxY = GetWorldMaxY()
    }

    Minigame.DEFAULT_CHOICE_COUNT = nil
    Minigame.DEFAULT_PREGAME_TIME = 5
    Minigame.DEFAULT_POSTGAME_TIME = 5
end

-- Must be called in the initializer of the minigame to add parameters and extra methods
function Minigame:init(minigame)
    Check:notNil(minigame.name, "Minigame name")
    Check:isNumber(minigame.minPlayers, "Minigame minPlayers")
    Check:isNumber(minigame.maxPlayers, "Minigame maxPlayers")
    Check:notNil(minigame.onPreGame, "Minigame onPreGame()")
    Check:notNil(minigame.onStart, "Minigame onStart()")
    Check:notNil(minigame.onEnd, "Minigame onEnd()")
    Check:notNil(minigame.onHeroRemoved, "Minigame onHeroRemoved()")

    if minigame.mapBounds == nil then
        self:logw("No mapBounds set. Using world map")
        minigame.mapBounds = Minigame.DEFAULT_MAP_BOUNDS
    end

    minigame.choiceCount = minigame.choiceCount or Minigame.DEFAULT_CHOICE_COUNT
    minigame.pregameTime = minigame.pregameTime or Minigame.DEFAULT_PREGAME_TIME
    minigame.postgameTime = minigame.postgameTime or Minigame.DEFAULT_POSTGAME_TIME

    minigame.id = Minigame.StaticIdNumber
    Minigame.StaticIdNumber = Minigame.StaticIdNumber + 1

    Log:addLogFunctions(minigame, "Minigame [" .. tostring(minigame.name) .. "-" .. tostring(minigame.id) .. "]")
    minigame.active = false
    minigame.heroes = {}
    minigame.callbacks = nil

    minigame.endInSuccess = function(self)
        if self.callbacks then
            self.callbacks.onSuccess(minigame)
        else
            self:logw("No callbacks for onSuccess()")
        end
    end
    minigame.endInFailure = function(self)
        if self.callbacks then
            self.callbacks.onFailure(minigame)
        else
            self:logw("No callbacks for onFailure()")
        end
    end
    minigame.addDuration = function(self, duration)
        if self.callbacks then
            self.callbacks.addDuration(duration)
        else
            self:logw("No callbacks for addDuration()")
        end
    end
    minigame.setDuration = function(self, duration)
        if self.callbacks then
            self.callbacks.setDuration(duration)
        else
            self:logw("No callbacks for setDuration()")
        end
    end

    -- Helper Methods

    -- Returns the number of currently alive heroes in the minigame
    minigame.numberOfAliveHeroes = function(self)
        local count = 0
        for _,hero in pairs(self.heroes) do
            if hero:IsAlive() then
                count = count + 1
            end
        end
        return count
    end

    -- Improved Region Listener Support
    -- Listeners registered in this manner will be unregistered when the minigame is disposed

    minigame.regionListeners = {}
    minigame.registerRegionListener = function(self, region, listener)
        RegionListeners:registerRegionListener(
            region,
            listener
        )
        self.regionListeners[listener] = true
    end
    minigame.registerRegionSetListener = function(self, regionSet, listener)
        RegionListeners:registerRegionSetListener(
            regionSet,
            listener
        )
        self.regionListeners[listener] = true
    end

    -- "Private" methods

    -- dispose is generally called when the round ends and the minigame should
    -- clean up things like region listeners
    minigame.dispose = function(self)
        self:logv("Disposing")
        self:logd("regionListeners: " .. tostring(table.size(self.regionListeners)))
        for listener,_ in pairs(self.regionListeners) do
            RegionListeners:removeListener(listener)
        end
    end
end

-- Sets all heroes in the minigame to the provided state
function Minigame:setAllHeroesToState(minigame, state)
    for _,hero in pairs(minigame.heroes) do
        hero:setHeroState(HeroStateManager.HeroState.ACTIVE)
    end
end

-- Helper method that teleports all of the heroes in the minigame to a random position in the region.
-- Intended for use in onPreGame()
function Minigame:moveHeroesToSpawnRegion(minigame, region)
    for _,hero in pairs(minigame.heroes) do
        local position = GetRandomPointInRegion(region)
        FindClearSpaceForUnit(hero, position, true)
        hero:SetForwardVector(RandomVector(1.0))
        hero:focusCameraOnUnit()
    end
end


--[[
    Attack Telegraphing Library 
    Version 0.0.1
    by Almouse

    The intent of this library is to create an interface
    with "Attack telegraphing" particles for abilities.

    This family of particles are designed to be lightweight,
    generic indicators that a unit is attacking. It behaves
    as an extension of the ParticleManager API.

    Intended 0.1 functionality:
        -- Create markers based on shape (circle, rect, cone)
        -- Assign marker types e.g.
            -- Incoming
            -- Ongoing
            -- Instantaneous
            -- Aborted

    Later intended functionality:
        -- Store particle information inside ability information
        -- Markers which follow entities
        -- Support for multiple particles for a single ability

]]


if AttackTelegraphs == nil then
    print ( '[AttackTelegraphs] creating AttackTelegraphs' )
    AttackTelegraphs = {}
end

function AttackTelegraphs:initialize()

    self.initialized = true

end

function AttackTelegraphs:CreateLineIndicator(position, angle, range, width, isBurst, isGlow)
    local particleName = nil
    local color = nil
    if isBurst then
        particleName = "particles/boss_fight/line_indicator_burst.vpcf"
        color = Vector(200, 50, 50)
    elseif isGlow then
        particleName = "particles/boss_fight/line_indicator_glow.vpcf"
    else
        particleName = "particles/boss_fight/line_indicator.vpcf"
        color = Vector(200, 0, 0)
    end

    local particle = ParticleManager:CreateParticle( 
        particleName,
        PATTACH_WORLDORIGIN,
        nil
    )
    -- Shift position up slightly to avoid trash on ground
    local modPosition = position+Vector(0,0,40)

    ParticleManager:SetParticleControl(particle, 0, modPosition)
    ParticleManager:SetParticleControl(particle, 1, modPosition+Vector(angle.x*range, angle.y*range, angle.z*range))
    ParticleManager:SetParticleControl(particle, 2, Vector(width,0,0))
    ParticleManager:SetParticleControl(particle, 3, color)

    if isBurst then
        ParticleManager:ReleaseParticleIndex(particle)
    end

    return particle

end


function AttackTelegraphs:CreateCircleIndicator(position, radius, isBurst, isGlow)
    local particleName = nil
    local color = nil
    if isBurst then
        particleName = "particles/boss_fight/circle_indicator_burst.vpcf"
        color = Vector(200, 50, 50)
    elseif isGlow then
        particleName = "particles/boss_fight/circle_indicator_glow.vpcf"
    else
        particleName = "particles/boss_fight/circle_indicator.vpcf"
        color = Vector(200, 0, 0)
    end

    local particle = ParticleManager:CreateParticle( 
        particleName,
        PATTACH_WORLDORIGIN,
        nil
    )
    -- Shift position up slightly to avoid trash on ground
    local modPosition = position+Vector(0,0,40)

    ParticleManager:SetParticleControl(particle, 0, modPosition)
    ParticleManager:SetParticleControl(particle, 1, Vector(radius,0,0))
    ParticleManager:SetParticleControl(particle, 2, color)

    if isBurst then
        ParticleManager:ReleaseParticleIndex(particle)
    end

    return particle

end


function AttackTelegraphs:CreateConeIndicator(position, angle, width, radius, isBurst, isGlow)
    local particleName = nil
    local color = nil
    if isBurst then
        particleName = "particles/boss_fight/cone_indicator_"..tostring(width).."_burst.vpcf"
        color = Vector(200, 50, 50)
    elseif isGlow then
        particleName = "particles/boss_fight/cone_indicator_"..tostring(width).."_glow.vpcf"
    else
        particleName = "particles/boss_fight/cone_indicator_"..tostring(width)..".vpcf"
        color = Vector(200, 0, 0)
    end

    local particle = ParticleManager:CreateParticle( 
        particleName,
        PATTACH_WORLDORIGIN,
        nil
    )
    -- Shift position up slightly to avoid trash on ground
    local modPosition = position+Vector(0,0,40)

    ParticleManager:SetParticleControl(particle, 0, modPosition)
    ParticleManager:SetParticleControl(particle, 1, Vector(radius,angle,0))
    ParticleManager:SetParticleControl(particle, 2, color)

    if isBurst then
        ParticleManager:ReleaseParticleIndex(particle)
    end

    return particle

end

if not AttackTelegraphs.initialized then
    AttackTelegraphs:initialize()
end

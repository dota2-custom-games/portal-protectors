--[[
    Minimap Manager Library 
    Version 1.0.0
    by Almouse

    This library provides an interface to the minimap panoramanager
    component, which manipulates the core Dota HUD minimap element.
    It provides custom support for minimap icons that can be manipulated
    using panorama/js code (rather than manipulating keyvalues).

    It can be combined with ObjectiveManager, which allows for dynamic highlighting
    of minimap icons (e.g. tracking which objectives correspond to which icons).

    Remember to enable the minimap_settings, minimap_objects, minimap_regions and minimap_heroes nettables!
    
    -- Minimap Icons and Regions will have tooltips based on their "minimapIcon" or "category" respectively
    -- Configure the precise strings by reading the javascript side

    -- Future functionality:
        - Right now, it is assumed all players are on the Radiant team. Multi-team support is needed
        - Right now, all updates are sent to all players. Individual player support is needed

    -- Public functions:
        - setMinimapBoundaries: Set the boundaries of the minimap
        - setWorldBoundaries: Set the boundaries of the world (usually use once at map initialization)
        - stopUpdates: Stop the minimap from updating. Can be helpful to "freeze" near the end of the game

        - registerEntity: Add a regular entity to be tracked on the minimap
        - updateEntity: Change the minimap icon associated with an entity
        - deregisterEntity: Remove an entity from the minimap

        - registerRegion: Add a region to the minimap (using coordinates)
        - registerCenteredRegion: Add a region to the minimap (using a center and radius)
        - registerHammerRegion: Add a region to the minimap (using a reference to a Hammer region)
        - deregisterRegion: Remove a region from the minimap

        - registerHeroEntity: Track a "hero" icon on the minimap (always visible, updates on panorama rather than Lua)
        - deregisterHeroEntity: Remove a "hero" icon from the minimap

    -- "Private" functions:
        - initialize()
        - startMinimapUpdates()

]]

if MinimapManager == nil then
    print ( '[MinimapManager] creating MinimapManager' )
    
    MinimapManager = {}
    -- Time between refreshes (in seconds)
    MinimapManager.REFRESH_INTERVAL = 1

    -- Minimal magnitude of movement needed to trigger a position update on the map
    MinimapManager.MIN_UPDATE_DISTANCE = 50

    -- Should we use custom hero icons?
    -- These are tracked panoramaside, for faster updates, but less support if hidden
    MinimapManager.USE_CUSTOM_HERO_ICONS = true

end

function MinimapManager:initialize(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    -- List of tracked entities
    self.trackedEntities = {}

    -- Should we stop updating the minimap (e.g. because the game is over)
    self.shouldTerminate = false

    self.visionDummy = CreateUnitByName("dummy_unit", Vector(0,0,0), true, nil, nil, DOTA_TEAM_GOODGUYS)

    self.initialized = true

    -- Defaults, change these if needed
    local bounds = {
        minX = GetWorldMinX(),
        maxX = GetWorldMaxX(),
        minY = GetWorldMinY(),
        maxY = GetWorldMaxY()
    }

    self:setWorldBoundaries(bounds)
    self:setMinimapBoundaries(bounds)

    self:startMinimapUpdates()

    return o
end

function MinimapManager:setMinimapBoundaries(bounds)
    --print("MinimapManager | Setting Minimap Boundaries")
    
    CustomNetTables:SetTableValue("minimap_settings", "minimap_boundaries",
        {       
            min_x = bounds.minX,
            max_x = bounds.maxX,
            min_y = bounds.minY,
            max_y = bounds.maxY
        }
    )

end

function MinimapManager:setWorldBoundaries(bounds)
    --print("MinimapManager | Setting World Boundaries")

    CustomNetTables:SetTableValue("minimap_settings", "minimap_boundaries",
        {       
            min_x = bounds.minX,
            max_x = bounds.maxX,
            min_y = bounds.minY,
            max_y = bounds.maxY
        }
    )

end

function MinimapManager:stopUpdates()
    self.shouldTerminate = true
end

function MinimapManager:startMinimapUpdates()
    -- Tell the panorama-side to hook-in and begin
    --print("MinimapManager | Starting minimap updates")


    -- Every refresh interval...
    Timers:CreateTimer(MinimapManager.REFRESH_INTERVAL, function()

        -- Removes traditional hero minimap icons for our custom ones
        GameRules:SetHeroMinimapIconScale(0)

        --print("MinimapManager | Tick!")

        if self.shouldTerminate then return false end

        -- For all registered units
        for entity,_ in pairs( self.trackedEntities ) do

            --print("MinimapManager | Testing an entity: ".. tostring(entity))

            local oldInformation = CustomNetTables:GetTableValue("minimap_objects", tostring(entity))
            -- Ifs the way my momma tought me how
            if (entity:IsNull() or (not entity:IsAlive()) ) then
                --print("MinimapManager | Registered entity no longer found, deleting" .. tostring(entity))
                self:deregisterEntity(entity)
            else
                local position = entity:GetAbsOrigin()
                if   (
                    (oldInformation) == nil or 
                    (oldInformation.minimapIcon ~= entity.minimapIcon) or
                    (distanceBetweenVectors(position, Vector(oldInformation.xposition, oldInformation.yposition, 0))
                             > MinimapManager.MIN_UPDATE_DISTANCE) or
                    (not entity.minimapAlwaysVisible))
                then
                    -- Something has changed
                    if (entity.minimapAlwaysVisible or self.visionDummy:CanEntityBeSeenByMyTeam(entity)) then
                        -- The unit should be visible

                        --print("MinimapManager | Setting a table")
                        --print("MinimapManager | Locations: "..tostring(position.x)..", "..tostring(position.y))
                        CustomNetTables:SetTableValue("minimap_objects", tostring(entity),
                             {minimapIcon = entity.minimapIcon,
                                 xposition = position.x,
                                 yposition = position.y
                                 })
                    else
                        -- The unit should not be visible, but I'm still interested in it
                        CustomNetTables:SetTableValue("minimap_objects", tostring(entity), {} )
                    end
                end
            end
        end
    
        return MinimapManager.REFRESH_INTERVAL

    end)

end

-- Add a unit to be tracked by the minimapmanager
-- @param entity: handle -- entity to be tracked
-- @param minimapIcon: string -- icon name (set appearance in JS!)
-- @param alwaysVisible: bool -- should the icon be visible even if the entity is hidden?
function MinimapManager:registerEntity(entity, minimapIcon, alwaysVisible)
    --print("MinimapManager | Registering an entity: "..tostring(entity))
    --table.insert(self.trackedEntities, entity)
    if (not self.trackedEntities[entity]) then
        self.trackedEntities[entity] = true
    end

    --print("MinimapManager | All tracked entities: "..tostring(self.trackedEntities))
    entity.minimapIcon = minimapIcon
    entity.minimapAlwaysVisible = alwaysVisible

end

-- Change the icon of a unit that is already being tracked
-- @param entity: handle -- entity to be tracked
-- @param minimapIcon: string -- icon name (set appearance in JS!)
function MinimapManager:updateEntity(entity, minimapIcon)
    entity.minimapIcon = minimapIcon
end

-- Remove a unit from being tracked by the manager
function MinimapManager:deregisterEntity(entity)
    --table.remove(self.trackedEntities, entity)
    self.trackedEntities[entity] = nil
    CustomNetTables:SetTableValue("minimap_objects", tostring(entity), nil )
end

-- Add a region to be tracked by the minimapmanager
-- This function can just be re-called if needed to move the region
-- @param id -- unique identifier (e.g. some hash of the gametime it was created)
-- @param min_x, max_x, min_y, max_y: number -- coordinates of the region
-- @param category: string -- used for purposes of highlighting, tooltips etc.
-- @param style: string -- additional CSS styles to be added
function MinimapManager:registerRegion(id, min_x, max_x, min_y, max_y, category, style)

    CustomNetTables:SetTableValue("minimap_regions", tostring(id), {
        min_x = min_x,
        max_x = max_x,
        min_y = min_y,
        max_y = max_y,
        category = category,
        style = style
    })

end

-- Helper function for circular regions
-- Add a region to be tracked by the minimapmanager
-- This function can just be re-called if needed to move the region
-- @param id -- unique identifier (e.g. some hash of the gametime it was created)
-- @param origin: Vector3d --
-- @param radius: number -- size of the region
-- @param category: string -- used for purposes of highlighting, tooltips etc.
-- @param style: string -- additional CSS styles to be added
function MinimapManager:registerCenteredRegion(id, origin, radius, category, style)
    local x = origin.x
    local y = origin.y

    self:registerRegion(id, 
        x-radius,
        x+radius,
        y-radius,
        y+radius,
        category,
        style )
end

-- Helper function for regions around Hammer regions
-- This function can just be re-called if needed to move the region
-- @param id -- unique identifier (e.g. some hash of the gametime it was created)
-- @param region: handle -- to the hammer region to be highlighted
-- @param category: string -- used for purposes of highlighting, tooltips etc.
-- @param style: string -- additional CSS styles to be added
function MinimapManager:registerHammerRegion(id, region, category, style)
    local regionOrigin = region:GetOrigin()
    local bounds = region:GetBounds()
    --print("MinimapManager registerHammerRegion | bounds: "..
    --            tostring(regionOrigin.x+bounds.Mins.x).." "..
    --            tostring(regionOrigin.x+bounds.Maxs.x).." "..
    --           tostring(regionOrigin.y+bounds.Mins.y).." "..
    --            tostring(regionOrigin.y+bounds.Maxs.y).." "
    --            )

    self:registerRegion(id,
        regionOrigin.x+bounds.Mins.x,
        regionOrigin.x+bounds.Maxs.x,
        regionOrigin.y+bounds.Mins.y,
        regionOrigin.y+bounds.Maxs.y,
        category,
        style
    )

end

-- Add a region to be tracked by the minimapmanager
-- This function can just be re-called if needed to move the region
-- @param id -- unique identifier (e.g. some hash of the gametime it was created)
function MinimapManager:deregisterRegion(id)
    CustomNetTables:SetTableValue("minimap_regions", tostring(id), nil)
end

-- Heroentities are ones we assume that the player can always see, and have a different appearance
-- They will be tracked directly by the panorama, this just updates the nettable
-- @param entity entIndex: Which entity will have the icon follow them?
-- @param minimapIcon String: Which icon should be attached to them (i.e. class icon)
-- @param isPrimary Bool: Is this unit considered a "primary" unit?
-- @param class String: any additional strings to add to this icon's class
function MinimapManager:registerHeroEntity(entity, minimapIcon, isPrimary, class)
    local playerIndex = GetUnitOwnerPlayer(entity).playerInfo.playerIndex

    CustomNetTables:SetTableValue("minimap_heroes", tostring(entity:GetEntityIndex()), {
        minimapIcon = minimapIcon,
        isPrimary = isPrimary,
        playerIndex = playerIndex,
        class = class
    })

end

function MinimapManager:deregisterHeroEntity(entity)
    CustomNetTables:SetTableValue("minimap_heroes", tostring(entity:GetEntityIndex()), nil)
end



if not MinimapManager.initialized then
    MinimapManager:initialize()
end

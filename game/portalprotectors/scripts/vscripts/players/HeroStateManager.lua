-- Main player heroes are passed in and given various helper methods
-- These methods are generally focused on changing the hero's state (such as setting a hero's state to "frozen"
-- while the game mode needs them to be immobile for a bit), or safely
-- interacting with the unit based on their state (ex: ignoring damage while inactive)


if not HeroStateManager then
    HeroStateManager = {}
    HeroStateManager.__index = HeroStateManager
    Log:addLogFunctions(HeroStateManager, "HeroStateManager")
end

-- HeroState is an "enum" representing an overall state the hero is in
HeroStateManager.HeroState = {
    OUT_OF_GAME = 1001, -- Hero is not set up for the game (initial state in game, set to this if removed from game)
    INACTIVE = 1002, -- Hero is not controllable nor has any physics acting on it (this occurs when killed, too)
    FROZEN = 1003, -- Hero is visible and controllable but won't actually move.
    ACTIVE = 1004, -- Hero is active and can be controlled
}

-- Reason codes to use in the dealDamage method to keep track of reasons that damage was dealt
-- (This is important for the HUD as well as logging)
HeroStateManager.DamageReasons = {
    SUICIDED = "suicided",
    DISCONNECTED = "disconnected",
    ENVIRONMENT = "environment",
    CRUSHED = "crushed",
    EXPLODED = "exploded",
    BURNED = "burned",
    UNKNOWN = "unknown"
}

-- Wraps a unit with useful functions
function HeroStateManager:initializeUnit(unit)
    if unit.heroInfo then
        self:loge("Attempt to initializeUnit() on unit (id=" .. tostring(unit.playerInfo.playerId) .. " that already has heroInfo(id=" .. tostring(unit.heroInfo.id))
        return
    end
    if not unit.playerInfo then
        self:loge("Attempt to initializeUnit() on unit (id=" .. tostring(unit.playerInfo.playerId) .. " that does not have a playerInfo yet!")
        return
    end

    self:logv("initializing unit: id=" .. tostring(unit.playerInfo.playerId))

    unit.heroInfo = {
        id = unit.playerInfo.playerId,
        heroState = HeroStateManager.HeroState.OUT_OF_GAME,
        heroStateId = 1, -- Increments every time hero state changed
        cameraLocked = false,
        damageListeners = {} -- Set of functions listening for when the hero takes damage
    }

    Log:addLogFunctions(unit, tostring(unit.playerInfo.playerName) .. "'s Hero")

    -- Returns the current hero state
    function unit:getHeroState()
        return unit.heroInfo.heroState
    end

    -- Updates the hero's state
    function unit:setHeroState(newState)
        local oldState = unit.heroInfo.heroState
        if oldState == newState then
            -- No change in state
            return
        end

        self:logv("State Change | " .. HeroStateManager:HeroStateName(oldState) .. " -> " .. HeroStateManager:HeroStateName(newState))

        -- TODO: Set up various states
        if newState == HeroStateManager.HeroState.OUT_OF_GAME then
            -- Disable physics and wipes info
            ApplyModifierUsingPermanentDummy(unit, "herostate_immobile", "herostate_immobile", 1)
        elseif newState == HeroStateManager.HeroState.INACTIVE then
            -- Disables physics and wipes movement info / velocity+acceleration
            ApplyModifierUsingPermanentDummy(unit, "herostate_immobile", "herostate_immobile", 1)
        elseif newState == HeroStateManager.HeroState.FROZEN then
            -- Just disables physics. They can still change targetPos and maintain velocity once resumed
            ApplyModifierUsingPermanentDummy(unit, "herostate_immobile", "herostate_immobile", 1)
        elseif newState == HeroStateManager.HeroState.ACTIVE then
            -- Let him start moving
            unit:RemoveModifierByName("herostate_immobile")
        else
            self:loge("Attempt to move to invalid state=" .. HeroStateManager:HeroStateName(newState))
            return
        end

        unit.heroInfo.heroState = newState
        unit.heroInfo.heroStateId = unit.heroInfo.heroStateId + 1
    end

    -- Called at the beginning of round after heroes have been placed in their starting spots
    function unit:doPreRoundStart()
        -- Reset any state they may have had from previous rounds
        unit:setHeroState(HeroStateManager.HeroState.INACTIVE)
        RemoveAllItems(unit)
        RemoveAllSkills(unit)

        if not unit:IsAlive() then
            unit:respawn(false, ReviveSystem.REVIVE_INVULNERABILITY)
        end

        -- Move camera onto unit
        unit:focusCameraOnUnit()

        -- Move into the FROZEN state so that they can begin issueing commands but not moving
        unit:setHeroState(HeroStateManager.HeroState.FROZEN)
    end

    -- Called once the round starts and players can now move
    function unit:doRoundStart()
        unit:setHeroState(HeroStateManager.HeroState.ACTIVE)
    end

    -- Called at round end to disable their state
    function unit:doRoundEnd()
        unit:setHeroState(HeroStateManager.HeroState.INACTIVE)
    end

    -- Called when "damage" is dealt to the unit. "Damage" here being how much snow to remove.
    -- If more damage is dealt than the unit has for snow, then the snowball explodes
    -- @param damage | How much snow to remove
    -- @param attacker | Who dealt the damage
    -- @param reason | A HeroStateManager.DamageReasons value
    function unit:dealDamage(damage, attacker, reasonCode)
        if not unit:IsAlive() then
            self:logv("Damaged but unit is already dead! damage=" .. tostring(damage) .. " | attacker=" .. tostring(attackerPlayerId) .. " | " .. HeroDamageReasonName(reasonCode))
            return
        elseif unit:IsInvulnerable() then
            self:logv("Damaged but unit is invulnerable! damage=" .. tostring(damage) .. " | attacker=" .. tostring(attackerPlayerId) .. " | " .. HeroDamageReasonName(reasonCode))
            return
        elseif not reasonCode then
            self:logw("Damaged but reasonCode is null")
            reasonCode = HeroStateManager.DamageReasons.UNKNOWN
        end

        local attackerPlayerId = nil
        if attacker then
            if attacker.playerInfo then
                attackerPlayerId = attacker.playerInfo.playerId
            else
                attackerPlayerId = attacker:GetPlayerOwnerID()
            end
        end
        self:logv("Damaged! damage=" .. tostring(damage) .. " | attacker=" .. tostring(attackerPlayerId) .. " | " .. HeroDamageReasonName(reasonCode))

        local wasKilled = false
        local actualDamageDealt = damage -- Corrects for overkill

        if unit:GetHealth() < damage then
            -- Killed!
            actualDamageDealt = unit:GetHealth() + 1
            wasKilled = true
            self:killHero(reasonCode)
        else
            -- Apply damage so the screen goes red and the damage number appears
            local damage_table = {
                attacker=attacker,
                ability = nil,
                damage_type = DAMAGE_TYPE_PURE,
                damage = damage,
                victim = unit
            }
            ApplyDamage(damage_table)
        end

        if wasKilled and attackerPlayerId == unit.playerInfo.playerId then
            -- If player killed himself, change the reason code to suicide
            reasonCode = HeroStateManager.DamageReasons.SUICIDED
        end

        -- Alert damage listeners
        for listener,_ in pairs(unit.heroInfo.damageListeners) do
            listener(unit, attacker, actualDamageDealt, wasKilled, reasonCode)
        end
    end

    -- Explodes the unit using particles and reseting their snowball size to 0
    function unit:killHero(reasonCode)
        -- Some reasons cause a particle
        if reason == HeroStateManager.DamageReasons.EXPLODED then
            local particle = ParticleManager:CreateParticle("particles/effects/corpse_blood_explosion.vpcf", PATTACH_CUSTOMORIGIN, nil)
            ParticleManager:SetParticleControl(particle, 0, unit:GetAbsOrigin())
        end

        local healthAtDeath = unit:GetHealth()

        unit:setHeroState(HeroStateManager.HeroState.INACTIVE) -- Overrides any other state
        unit:ForceKill(false)

        -- Alert damage listeners
        for listener,_ in pairs(unit.heroInfo.damageListeners) do
            listener(unit, nil, healthAtDeath, true, reasonCode)
        end
    end

    -- Respawns the unit. Note: This will *not* move the unit! Move before calling
    -- @param withParticles | Boolean (default=true) | Displays the revive particles / sound.
    -- @param temporaryInvulAfterRevive | Int (default=0) | Grants invulnerability for the duration
    function unit:respawn(withParticles, temporaryInvulAfterRevive)
        local withParticles = withParticles or true

        if not self:UnitCanRespawn() then
            self:logv("Attempted to revive unit that can't respawn!")
        elseif unit:IsAlive() then
            self:logw("Attempted to revive unit that is already alive!")
        else
            unit:RespawnUnit()
            unit:setHeroState(HeroStateManager.HeroState.ACTIVE) -- Overrides any other state

            -- Emit revive particle
            unit:emitReviveParticle(unit)

            if temporaryInvulAfterRevive and temporaryInvulAfterRevive > 0 then
                -- Grant temporary invulnerability
                ApplyModifierUsingDummyAbility(unit, "common_invulnerable", "common_invulnerable", 1, {duration=temporaryInvulAfterRevive})
            end

            -- I can't believe I have to do this
            -- When the unit is respawned, all of his wearables are attached again on the server, including dupes
            -- So, we have to purge the dupes that were added or the game will start to lag like crazy after enough deaths
            -- We must also continue to remove the particle wearables
            local wearablesAttached = {}
            local children = unit:GetChildren()
            for k,child in pairs(children) do
                if child:GetClassname() == "dota_item_wearable" then
                    local modelName = child:GetModelName()
                    if modelName == "" then
                        -- Remove any particle wearables
                        child:RemoveSelf()
                    elseif wearablesAttached[modelName] then
                        -- Duplicate wearable. Remove it
                        --print("Removing Duplicate Particle: " .. tostring(child:GetModelName()))
                        child:RemoveSelf()
                    else
                        wearablesAttached[modelName] = true
                    end
                end
            end
        end
    end

    -- Adds a function that will be called whenever the unit takes damage / is killed
    -- @listener: function(unit, source, damageDealt, wasKilled, damageReason)
    function unit:addDamageListener(listener)
        self:logv("Attaching damage listener")
        self.heroInfo.damageListeners[listener] = true
    end

    -- Removes a listener attached in addDamageListener. Just pass in the same function sent to that
    function unit:removeDamageListener(listener)
        if not self.heroInfo.damageListeners[listener] then
            self:logw("Attempting to remove a damage listener that doesn't exist")
        else
            self:logv("Removing damage listener")
            self.heroInfo.damageListeners[listener] = nil
        end
    end

    -- MARK: Particles / Emotes on unit

    function unit:emitReviveParticle(unit)
        -- Emit revive particle
        local particleName = "particles/items_fx/aegis_respawn.vpcf"
        ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN_FOLLOW, unit )

        unit:EmitSound("Hero_Omniknight.GuardianAngel.Cast")
    end

    -- Changes whether or not the camera is locked onto the unit.
    -- Reset when round ends.
    function unit:setCameraLocked(isLocked)
        self.heroInfo.cameraLocked = isLocked
        if isLocked then
            PlayerResource:SetCameraTarget(unit:GetPlayerOwnerID(), unit)
        else
            PlayerResource:SetCameraTarget(unit:GetPlayerOwnerID(), nil)
        end
    end

    -- Moves the camera to the unit
    function unit:focusCameraOnUnit()
        if not self.heroInfo.cameraLocked then
            -- Move camera to unit's location but then don't lock it
            -- Feels a bit hacky but it seems to work and doesn't conflict with settings camera lock
            PlayerResource:SetCameraTarget(unit:GetPlayerOwnerID(), unit)
            Timers:CreateTimer(0.25, function()
                PlayerResource:SetCameraTarget(unit:GetPlayerOwnerID(), nil)
            end)
        end
    end

end

function HeroStateManager:HeroStateName(state)
    if state == HeroStateManager.HeroState.OUT_OF_GAME then
        return "OUT_OF_GAME"
    elseif state == HeroStateManager.HeroState.INACTIVE then
        return "INACTIVE"
    elseif state == HeroStateManager.HeroState.FROZEN then
        return "FROZEN"
    elseif state == HeroStateManager.HeroState.ACTIVE then
        return "ACTIVE"
    else
        return "UNKNOWN(" .. tostring(state) .. ")"
    end
end

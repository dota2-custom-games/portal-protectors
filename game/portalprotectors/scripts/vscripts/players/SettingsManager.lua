-- Controls player settings and coordinates with the panorama
--   All settings are stored in net tables

-- TODO: Hook up the panorama side

if not SettingsManager then
    SettingsManager = {}
    SettingsManager.__index = SettingsManager
    Log:addLogFunctions(SettingsManager, "SettingsManager")

    -- map of playerId -> settings table
    SettingsManager.settings = {}

    CustomGameEventManager:RegisterListener("settings_update_event", Dynamic_Wrap(SettingsManager, 'onSettingsEvent'))
    CustomGameEventManager:RegisterListener("settings_sync_event", Dynamic_Wrap(SettingsManager, 'onSettingsSyncEvent'))
    CustomGameEventManager:RegisterListener("settings_request_event", Dynamic_Wrap(SettingsManager, 'onSettingsRequestEvent'))
end

-- Returns true/false whether or not the playerInfo provided has any settings
function SettingsManager:doesPlayerHaveSettings(playerInfo)
    return self.settings[playerInfo.playerId] ~= nil
end

-- MARK: Parnoama event listeners

-- Responds from a listener event when the panorama UI updates
function SettingsManager:onSettingsEvent( keys )
    local playerId = keys.playerId
    local settings = keys.settings

    self:logv("onSettingsEvent | Received data from player: "..tostring(playerId))
    self:logv("                | " .. table.toPrettyString(settings))

    local player = PlayerResource:GetPlayer(playerId)
    SettingsManager.settings[playerId] = settings

        -- Only actually update the settings if the game has started proper
    if GameRules:State_Get() >= DOTA_GAMERULES_STATE_STRATEGY_TIME then
        local playerInfo = PlayerManager:getPlayerInfoForPlayerId(playerId)
        SettingsManager:updateAllSettings(playerInfo)
    end
end

-- Received when a player hits the sync button on the panorama side
function SettingsManager:onSettingsSyncEvent(keys)
    local playerId = keys.playerId
    SettingsManager:sendSettings(playerId)
    --print("WARNING | Settings Sync Event error: No playerInfo for playerId=" .. tostring(playerId))
end

-- Received when panorama loads. If we have settings, broadcast them
function SettingsManager:onSettingsRequestEvent(keys)
    local playerId = keys.playerId
    if SettingsManager.settings[playerId] then
        SettingsManager:sendPanoramaUpdate(playerId)
    else
        self:logw("Settings Request Event error: No settings ready for playerId=" .. tostring(playerId))
    end
end

-- Sends updated options to the Panorama to be displayed properly
function SettingsManager:sendPanoramaUpdate(playerId)
    local settings = self.settings[playerId]
    if settings then
        senddata = {playerId = playerId, settings=settings}
        local player = PlayerResource:GetPlayer(playerId)
        CustomGameEventManager:Send_ServerToPlayer(player, "settings_panorama_event", senddata)
    else
        self:logw("No settings to send for playerId=" .. tostring(playerId))
    end
end

-- Executes func and passes in the settings
function SettingsManager:updateSettings(playerInfo, func)
    local settings = self.settings[playerInfo.playerId]
    if not settings then
        self.settings[playerInfo.playerId] = {}
        settings = self.settings[playerInfo.playerId]
    end
    func(settings)
end

------------------------
-- Settings
-----------------------

-- Generally called when a change occurred on the panorama side or the server downloaded them
-- It is assumed that the self.settings value has been updated, and we need to run the appropiate code
function SettingsManager:updateAllSettings(playerInfo)
    local settings = self.settings[playerInfo.playerId]
    if not settings then
        return
    end

    if SHOW_SETTINGS_LOGS then
        print("SettingsManager | Updating Settings for id=" .. tostring(playerInfo.playerId))
        print("Sharing?: ".. tostring(settings.unitSharing) )
        print("WideMap?: ".. tostring(settings.wideMap) )
        print("View Distance: ".. tostring(settings.view) )
        print("View Angle: ".. tostring(settings.viewAngle) )
        print("Invert Alt: "..tostring(settings.invertAlt))
        print("Hero Icon Size: "..tostring(settings.minimapIconSize))
    end

    -- Unit Sharing, implemented by someone who clearly understands booleans and branching
    if settings.unitSharing == 1 then
        PlayerManager:setPlayerUnitsControllableByAll(playerInfo, true)
    else
        PlayerManager:setPlayerUnitsControllableByAll(playerInfo, false)
    end

    -- GameEventCacheBroadcaster:Send_ServerToPlayer(playerInfo, "stats_wide_mode", {enabled=settings.wideMap})
end

-- Sets unit sharing on/off for the player
function SettingsManager:setUnitSharing(playerInfo, unitSharingOn)
    PlayerManager:setPlayerUnitsControllableByAll(playerInfo, unitSharingOn)

    self:updateSettings(playerInfo, function(settings)
        settings.unitSharing = unitSharingOn

        self:sendPanoramaUpdate(playerId)
    end)
end

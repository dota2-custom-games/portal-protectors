
-- Because Dota 2 has screwed up stats, the mod needs to use an entirely custom system for stats
-- Heroes will store their "stats" in the sdata table and have custom modifiers applied here
--
-- This class can also be used to give non-heroes "stats" just like heroes
-- This is particularly useful for things like armor, which can't be set on non-heroes

if not CustomStats then
    CustomStats = {}
    CustomStats.__index = CustomStats

    -- Stores an item entity to apply the modifiers for each stat
    CustomStats.STAT_ADJUSTMENT_APPLIER = nil
    CustomStats.PRIMARY_STRENGTH = "strength"
    CustomStats.PRIMARY_AGILITY = "agility"
    CustomStats.PRIMARY_INTELLECT = "intellect"
end

-- Sets up the passed in hero for the Custom Stats system
-- The hero will have their actual stats set to 0
function CustomStats:SetUpHeroForCustomStats(hero)
    if STAT_ADJUSTMENT_APPLIER == nil then
        STAT_ADJUSTMENT_APPLIER = CreateItem("item_custom_stats_modifier", nil, nil)
    end

    hero.stats = {
        primary_stat=nil,
        -- "White" Values
        base_strength=0,
        base_agility=0,
        base_intellect=0,
        -- "Green" values
        bonus_strength=0,
        bonus_agility=0,
        bonus_intellect=0,
        -- Amount gained per level:
        level_gain_strength=0,
        level_gain_agility=0,
        level_gain_intellect=0,
        -- Armor:
        base_armor=0
    }

    -- Reapplys all the stats to the hero (useful after a revive)
    function hero:refreshStats()
        print("Stats | Refreshing stats")
        hero:ModifyCustomBaseArmor(0)
        hero:OnCustomStrengthChanged()
        hero:OnCustomAgilityChanged()
        hero:OnCustomIntellectChanged()

        -- Reset any stats just in case the game applied them
        hero:SetBaseStrength(0)
        hero:SetBaseAgility(0)
        hero:SetBaseIntellect(0)
        hero:CalculateStatBonus()
    end

    -- MARK: Leveling up stats

    function hero:SetLevelUpGains(str, agi, int)
        hero.stats.level_gain_strength = str
        hero.stats.level_gain_agility = agi
        hero.stats.level_gain_intellect = int
    end

    function hero:LevelUpStats()
        hero.stats.base_strength = hero.stats.base_strength + hero.stats.level_gain_strength
        hero.stats.base_agility = hero.stats.base_agility + hero.stats.level_gain_agility
        hero.stats.base_intellect = hero.stats.base_intellect + hero.stats.level_gain_intellect
        hero:OnCustomStrengthChanged()
        hero:OnCustomAgilityChanged()
        hero:OnCustomIntellectChanged()

        -- Reset any stats in case the game applied them
        hero:SetBaseStrength(0)
        hero:SetBaseAgility(0)
        hero:SetBaseIntellect(0)
        hero:CalculateStatBonus()
    end

    -- MARK: Strength

    function hero:GetCustomBaseStrength()
        return hero.stats.base_strength
    end

    function hero:GetCustomBonusStrength()
        return hero.stats.bonus_strength
    end

    function hero:GetCustomStrength()
        return hero.stats.base_strength + hero.stats.bonus_strength
    end

    -- Can be negative
    function hero:ModifyCustomBaseStrength(str)
        self:SetCustomBaseStrength(hero.stats.base_strength + str)
    end

    function hero:ModifyCustomBonusStrength(str)
        self:SetCustomBonusStrength(hero.stats.bonus_strength + str)
    end

    function hero:SetCustomBaseStrength(str)
        CustomStats:logHeroStatChange(hero, "Setting Base Strength to: " .. tostring(str))
        hero.stats.base_strength = str

        self:OnCustomStrengthChanged()
    end

    function hero:SetCustomBonusStrength(str)
        CustomStats:logHeroStatChange(hero, "Setting Bonus Strength to: " .. tostring(str))
        hero.stats.bonus_strength = str

        self:OnCustomStrengthChanged()
    end

    function hero:OnCustomStrengthChanged()
        local stacks = hero:GetCustomStrength()
        CustomStats:setBonusModifierStackCount(hero, "modifier_strength", stacks)

        if hero.stats.primaryStat == CustomStats.PRIMARY_STRENGTH then
            hero:OnCustomPrimaryStatChanged(stacks)
        end
    end

    -- MARK: Agility

    function hero:GetCustomBaseAgility()
        return hero.stats.base_agility
    end

    function hero:GetCustomBonusAgility()
        return hero.stats.bonus_agility
    end

    function hero:GetCustomAgility()
        return hero.stats.base_agility + hero.stats.bonus_agility
    end

    function hero:ModifyCustomBaseAgility(agi)
        self:SetCustomBaseAgility(hero.stats.base_agility + agi)
    end

    function hero:ModifyCustomBonusAgility(agi)
        self:SetCustomBonusAgility(hero.stats.bonus_agility + agi)
    end

    function hero:SetCustomBaseAgility(agi)
        CustomStats:logHeroStatChange(hero, "Setting Base Agility to: " .. tostring(agi))
        hero.stats.base_agility = agi

        self:OnCustomAgilityChanged()
    end

    function hero:SetCustomBonusAgility(agi)
        CustomStats:logHeroStatChange(hero, "Setting Bonus Agility to: " .. tostring(agi))
        hero.stats.bonus_agility = agi

        self:OnCustomAgilityChanged()
    end

    function hero:OnCustomAgilityChanged()
        local stacks = hero:GetCustomAgility()
        CustomStats:setBonusModifierStackCount(hero, "modifier_agility", stacks)

        if hero.stats.primaryStat == CustomStats.PRIMARY_AGILITY then
            hero:OnCustomPrimaryStatChanged(stacks)
        end
    end

    -- MARK: Intellect

    function hero:GetCustomBaseIntellect()
        return hero.stats.base_intellect
    end

    function hero:GetCustomBonusIntellect()
        return hero.stats.bonus_intellect
    end

    function hero:GetCustomIntellect()
        return hero.stats.base_intellect + hero.stats.bonus_intellect
    end

    function hero:ModifyCustomBaseIntellect(int)
        self:SetCustomBaseIntellect(hero.stats.base_intellect + int)
    end

    function hero:ModifyCustomBonusIntellect(int)
        self:SetCustomBonusIntellect(hero.stats.bonus_intellect + int)
    end

    function hero:SetCustomBaseIntellect(int)
        CustomStats:logHeroStatChange(hero, "Setting Base Intellect to: " .. tostring(int))
        hero.stats.base_intellect = int

        self:OnCustomIntellectChanged()
    end

    function hero:SetCustomBonusIntellect(int)
        CustomStats:logHeroStatChange(hero, "Setting Bonus Intellect to: " .. tostring(int))
        hero.stats.bonus_intellect = int

        self:OnCustomIntellectChanged()
    end

    function hero:OnCustomIntellectChanged()
        local stacks = hero:GetCustomIntellect()
        CustomStats:setBonusModifierStackCount(hero, "modifier_intelligence", stacks)

        if hero.stats.primaryStat == CustomStats.PRIMARY_INTELLECT then
            hero:OnCustomPrimaryStatChanged(stacks)
        end
    end

    -- MARK: Primary Stat

    function hero:SetPrimaryAttribute(primaryStat)
        if primaryStat ~= nil
            and primaryStat ~= CustomStats.PRIMARY_STRENGTH
            and primaryStat ~= CustomStats.PRIMARY_AGILITY
            and primaryStat ~= CustomStats.PRIMARY_INTELLECT
            then
            Log:e("CustomStats", "Invalid primaryStat being applied to hero: " .. tostring(primaryStat) .. ". Not giving it a primary stat")
            primaryStat = nil
        end

        hero.stats.primary_stat = primaryStat
        if not primaryStat then
            hero:OnCustomPrimaryStatChanged(0)
        elseif primaryStat == CustomStats.PRIMARY_STRENGTH then
            hero:OnCustomPrimaryStatChanged(hero:GetCustomStrength())
        elseif primaryStat == CustomStats.PRIMARY_AGILITY then
            hero:OnCustomPrimaryStatChanged(hero:GetCustomAgility())
        elseif primaryStat == CustomStats.PRIMARY_INTELLECT then
            hero:OnCustomPrimaryStatChanged(hero:GetCustomIntellect())
        end
    end

    function hero:OnCustomPrimaryStatChanged(statValue)
        -- Heroes get 1 damage stack per stat value on their primary stat
        CustomStats:setBonusModifierStackCount(hero, "modifier_damage_constant", statValue)
    end

    -- MARK: Armor

    function hero:ModifyCustomBaseArmor(armor)
        hero:SetCustomBaseArmor(hero.stats.base_armor + armor)
    end

    function hero:SetCustomBaseArmor(armor)
        CustomStats:logHeroStatChange(hero, "Setting Base Armor to: " .. tostring(armor))
        hero.stats.base_armor = armor
        hero:SetPhysicalArmorBaseValue(hero.stats.base_armor)
    end

    -- MARK: Initialization

    -- Transfer their existing stats into the converted system
    hero:SetCustomBaseStrength(hero:GetBaseStrength())
    hero:SetCustomBaseAgility(hero:GetBaseAgility())
    hero:SetCustomBaseIntellect(hero:GetBaseIntellect())
    hero:SetCustomBaseArmor(0)

    hero:SetBaseStrength(0)
    hero:SetBaseAgility(0)
    hero:SetBaseIntellect(0)
    hero:SetBaseHealthRegen(0)
    hero:SetBaseManaRegen(0)
    hero:CalculateStatBonus()

    local strengthPerLevel = hero:GetStrengthGain()
    local agilityPerLevel = hero:GetAgilityGain()
    local intellectPerLevel = hero:GetIntellectGain()
    hero:SetLevelUpGains(strengthPerLevel, agilityPerLevel, intellectPerLevel)
end

-- MARK: Standalone modifiers (useful for non-heroes units)

-- Sets the bonus armor of a unit, even if not a hero. Supports up to 2 decimal places
function CustomStats:setArmorBonus(unit, armor)
    local armorStacks = armor * 100 -- Modifier adds 0.01 armor per stack.
    CustomStats:setBonusModifierStackCount(unit, "modifier_armor_bonus_constant", armorStacks)
end

-- Sets the bonus health of a unit
function CustomStats:setHealthBonus(unit, health)
    CustomStats:setBonusModifierStackCount(unit, "modifier_health_bonus", health)
end

-- Sets the bonus health regen of a unit
function CustomStats:setHealthRegenBonus(unit, healthRegen)
    CustomStats:setBonusModifierStackCount(unit, "modifier_health_regen_constant", healthRegen)
end

-- Sets the bonus damage of a unit
function CustomStats:setBaseAttackBonus(unit, damage)
    CustomStats:setBonusModifierStackCount(unit, "modifier_damage_constant", damage)
end

-- Sets the bonus mana of a unit
function CustomStats:setManaBonus(unit, mana)
    CustomStats:setBonusModifierStackCount(unit, "modifier_mana_bonus", mana)
end

-- Sets the bonus mana regen of a unit
function CustomStats:setManaRegenBonus(unit, manaRegen)
    CustomStats:setBonusModifierStackCount(unit, "modifier_base_mana_regen", manaRegen)
end



-- MARK: Private Util

-- Applies the passed in modifier, if not present, and updates its stack count to match
-- stack counts under 1 just remove it
function CustomStats:setBonusModifierStackCount(unit, modifierName, stackCount)
    if stackCount < 1 then
        unit:RemoveModifierByName(modifierName)
    else
        if CustomStats.STAT_ADJUSTMENT_APPLIER == nil then
            CustomStats.STAT_ADJUSTMENT_APPLIER = CreateItem("item_custom_stats_modifier", nil, nil)
        end
        if not unit:HasModifier(modifierName) then
            CustomStats.STAT_ADJUSTMENT_APPLIER:ApplyDataDrivenModifier(unit, unit, modifierName, {})
        end
        unit:SetModifierStackCount(modifierName, unit, stackCount)
    end
end

function CustomStats:logHeroStatChange(hero, msg)
    Log:v("CustomStats | id=" .. tostring(hero:GetPlayerOwnerID()) .. " | " .. tostring(msg))
end

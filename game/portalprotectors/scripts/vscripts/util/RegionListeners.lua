-- Allows for the registration of region event listeners
-- Map files can define regions that have events when units enter/leave the region.
-- Lua code can register listeners for those regions here, without the map file having to explicitilty know which
-- lua function to call
--
-- Map Files:
--      + Regions should set up events for UnitEntersRegion on DOTA_GOODGUYS to call the lua function "UnitEntersRegion"
--      + Regions should set up events for UnitLeavesRegion on DOTA_GOODGUYS to call the lua function "UnitLeavesRegion"

if not RegionListeners then
    RegionListeners = {}
    RegionListeners.__index = RegionListeners
    Log:addLogFunctions(RegionListeners, "RegionListeners")

    -- All region listeners. Placed here so not reset on script_reload
    RegionListeners.locationsToFunctions = {}
    RegionListeners.regionSetsListeners = {} -- Set of {regionSet=Set, func=listener}
end

-- Registers a function to be called whenever a valid unit enters/leaves a specific region
-- @param region | The region the listener will activate for
-- @param listener | The listener that is called. This will be a function in the form:
--                   function(unit, region, isEntering)
-- @return the listener
function RegionListeners:registerRegionListener(region, listener)
    self:logv("LocationEvents | Registering room " .. tostring(region) .. " to an event")
    if not self.locationsToFunctions[region] then
        self.locationsToFunctions[region] = {}
    end
    table.insert(self.locationsToFunctions[region], listener)
    return listener
end

-- Registers a function to be called whenever a valid unit enters/leaves *any* region in the set
-- @param regionSet | Set of regions the listener will activate for
-- @param listener | The listener that is called. This will be a function in the form:
--                   function(unit, region, isEntering)
-- @return the listener
function RegionListeners:registerRegionSetListener(regionSet, func)
    Check:isTable(regionSet, "region set")
    Check:isFunction(func, "listener")
    if regionSet[1] then
        -- The passed in must be a set (keys are regions) not a list!
        self:loge("RegionSet passed to registerRegionSetListener is probably not a set!!")
        PrintTable(regionSet)
    else
        local regionGroup = {regionSet=regionSet, func=func}
        self.regionSetsListeners[regionGroup] = true
    end
end

-- Removes the listener from being called in the future
-- @param listener | The listener that was register either for a single region or a set
function RegionListeners:removeListener(func)
    Check:isFunction(func, "listener")

    local foundListener = false
    for _,listeners in pairs(self.locationsToFunctions) do
        local index = table.indexOf(listeners, func)
        if index >= 0 then
            table.remove(listeners, index)
            foundListener = true
        end
    end
    for regionGroup,_ in pairs(self.regionSetsListeners) do
        if regionGroup.func == func then
            self.regionSetsListeners[regionGroup] = nil
            foundListener = true
        end
    end

    if not foundListener then
        -- The passed in must be a set (keys are regions) not a list!
        self:loge("removeListener() called but the passed in listener was not registered to any region/regionSet!!")
        PrintTable(regionSet)
    end
end

-- Used to filter out units we don't want these methods interacting with
function RegionListeners:IsValidUnit(unit)
    if not unit then
        return false
    end

    local unitName = unit:GetUnitName()
    local length = string.len(unitName)
    -- Blacklist certain units from proccing UnitEnters events
    -- Note: Some methods don't use this (have more validation)
    if not unitName or length == 0 -- Projectiles have no name
        or (length >= 5 and string.sub(unitName, 1,5) == "dummy")
        or unitName == "npc_dota_thinker"
        then
        return false
    end
    return true
end

-- Checks against all regions registered. If the region is registered,
-- then we'll call its function and pass the unit and the isEntering
function RegionListeners:onUnitRegionEvent(unit, region, isEntering)
    -- RegionEvents from the dota engine work like this:
    -- + If any part of a units bounds intersects with the bounds of a region, it queues up the event
    -- + Moving (with SetAbsOrigin() or just moving really quickly) through a region queues up the event as well
    --
    -- So, this causes some issues with teleporting abilities like Recall. Regions that the unit passes
    -- through during the teleporting will trigger region events in dota.
    --
    -- This confirm method will check to see if the unit is *still in the region* when the event is processed.
    -- If the unit is no longer in it, we'll assume he teleported. Technically, he could have moved quickly through it,
    -- but that's going to be the collateral damage here for now.
    --
    -- Additionally, if the unit is not in the region, we also don't want to proc onLeave events for that region.
    -- So, we'll mark the region on the unit and check against that when he leaves regions and ignore (and remove)
    -- regions we didn't proc a onEnter event for.
    --
    -- tl;dr - Tries to ignore region events triggered by the unit
    --         teleporting through the region with Recall.
    function confirmRegionEvent(unit, region, isEntering)
        if isEntering then
            if UnitBoundsAreInRegion(unit, region) then
                return true
            else
                local regionsUnitDidNotEnter = unit.regionsUnitDidNotEnter or {}
                regionsUnitDidNotEnter[region] = (regionsUnitDidNotEnter[region] or 0) + 1
                unit.regionsUnitDidNotEnter = regionsUnitDidNotEnter
                return false
            end
        else
            local count = unit.regionsUnitDidNotEnter and unit.regionsUnitDidNotEnter[region]
            if count then
                count = count - 1
                if count < 1 then
                    unit.regionsUnitDidNotEnter[region] = nil
                    if next(unit.regionsUnitDidNotEnter) == nil then
                        unit.regionsUnitDidNotEnter = nil
                    end
                else
                    unit.regionsUnitDidNotEnter[region] = count
                end
                return false
            else
                return true
            end
        end
    end

    if not confirmRegionEvent(unit, region, isEntering) then
        if isEntering then
            print("Ignoring region enter event. Unit is no longer in region.")
        else
            print("Ignoring region exit event. Unit enter event was ignored.")
        end
        return
    end

    -- First check if the region is registered to the individual location checks
    local funcs = self.locationsToFunctions[region]
    if funcs then
        for _,func in pairs(funcs) do
            func(unit, region, isEntering)
        end
    end

    -- Now see if it belongs to any region group
    for regionGroup,_ in pairs(self.regionSetsListeners) do
        if regionGroup.regionSet[region] then
            regionGroup.func(unit, region, isEntering)
        end
    end
end

-- Called when a unit enters a generic marked region
-- This function is set and called in the map file itself
-- (which means not all units may trigger it if the map is configured in such a way)
--
-- This function checks to see that the unit is a valid one (basically, not a thinker or dummy unit)
-- and then checks for region listeners on the region and calls those
function UnitEntersRegion(userdata, keys)
    local unit = keys.activator
    if RegionListeners:IsValidUnit(unit) then
        local region = keys.caller
        RegionListeners:onUnitRegionEvent(unit, region, true)
    end
end

-- Called when a unit leaves a generic marked region
-- This function is set and called in the map file itself
-- (which means not all units may trigger it if the map is configured in such a way)
--
-- This function checks to see that the unit is a valid one (basically, not a thinker or dummy unit)
-- and then checks for region listeners on the region and calls those
function UnitLeavesRegion(userdata, keys)
    local unit = keys.activator
    if RegionListeners:IsValidUnit(unit) then
        local region = keys.caller
        RegionListeners:onUnitRegionEvent(unit, region, false)
    end
end

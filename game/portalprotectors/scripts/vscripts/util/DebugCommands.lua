-- Adds debug commands

if not DebugCommands then
    DebugCommands = {}
    DebugCommands.__index = DebugCommands

    -- The command prefix before all debug commands
    DebugCommands.COMMAND_PREFIX = "portal_"
    DebugCommands.REGISTERED_COMMANDS = {}
end

-- MARK: Util Functions

-- Registers a command using the provided command name that will execute the passed in function when called
--[[ Ex:
        DebugCommands:AddDebugCommand(
            "test",
            function(arg)
                print("Test Debug | arg=", arg)
            end
        )
--]]
function DebugCommands:AddDebugCommand(commandName, commandFunction)
    local command = DebugCommands.COMMAND_PREFIX .. commandName

    -- Dota dumps out a log message when overriding registered commands,
    -- which could happen every time a developer calls script_reload.
    -- So, if this command name has already been registered, don't register it with convar
    local alreadyRegisteredConvar = (DebugCommands.REGISTERED_COMMANDS[command] ~= nil)

    DebugCommands.REGISTERED_COMMANDS[command] = commandFunction

    if not alreadyRegisteredConvar then
        Convars:RegisterCommand(
            command,
            DebugCommandExecuted,
            "Debug Command",
            FCVAR_CHEAT
        )
    end
end

-- Free function that is executed any time a command is called from AddDebugCommand
function DebugCommandExecuted(command, ...)
    local commandFunction = DebugCommands.REGISTERED_COMMANDS[command]
    if not commandFunction then
        print("Could not find command function for registered command: '" .. tostring(command) .. "'")
        return
    end

    commandFunction(...)
end

-- Returns the hero of the person activating the command
function DebugCommands:GetPlayerHero()
    local cmdPlayer = Convars:GetCommandClient()
    if cmdPlayer then
        return cmdPlayer:GetAssignedHero()
    end

    print("DEBUG | Could not find assigned hero for debug command issueing player")
    return nil
end

-- MARK: Debug Commands

DebugCommands:AddDebugCommand(
    "time",
    function()
        print("DEBUG | Time")

        --local dotaTime = GameRules:GetDOTATime(false, false)
        print("GameTime: " .. tostring(GameRules:GetGameTime())
              .. "\nGameDuration: " .. tostring(GetGameDuration()) -- This is the one used for most record keeping purposes
              .. "\nDotaTime(f,f): " .. tostring(GameRules:GetDOTATime(false, false))
              .. "\nDotaTime(t,f): " .. tostring(GameRules:GetDOTATime(true, false))
              .. "\nDotaTime(f,t): " .. tostring(GameRules:GetDOTATime(false, true))
              .. "\nDotaTime(t,t): " .. tostring(GameRules:GetDOTATime(true, true)))
    end
)

DebugCommands:AddDebugCommand(
    "fow",
    function()
        mode:SetFogOfWarDisabled(not mode:GetFogOfWarDisabled())
    end
)

DebugCommands:AddDebugCommand(
    "player_info",
    function()
        local playerInfo = DebugCommands:GetPlayerHero().playerInfo
        PrintTable(playerInfo)
    end
)

DebugCommands:AddDebugCommand(
    "scale",
    function(scale)
        local scale = tonumber(scale or 1.7)
        local hero = DebugCommands:GetPlayerHero()
        hero:SetModelScale(scale)
    end
)

DebugCommands:AddDebugCommand(
    "tint",
    function(r, g, b)
        local r = tonumber(r or 255)
        local g = tonumber(g or 255)
        local b = tonumber(b or 255)
        local hero = DebugCommands:GetPlayerHero()
        hero:SetRenderColor(r, g, b)
    end
)

DebugCommands:AddDebugCommand(
    "sound",
    function(num, method)
        local num = num and tonumber(num) or 0
        local method = method and tonumber(method) or 1

        local soundName = nil

        if num == 0 then
            soundName = "SwatReborn.Weapon.AssaultRifle"
        elseif num == 1 then
            EmitGlobalSound("RoshanDT.Death2")
        elseif num == 2 then
            soundName = "RoshanDT.Scream"
        elseif num == 3 then
            soundName = "SwatReborn.Weapon.SniperRifle"
        elseif num == 4 then
            soundName = "SwatReborn.Weapon.RocketLauncher"
        elseif num == 5 then
            soundName = "SwatReborn.Weapon.RocketLauncherImpact"
        elseif num == 6 then
            soundName = "SwatReborn.Weapon.Chaingun"
        elseif num == 7 then
            soundName = "SwatReborn.Radiation.Heartbeat.20"
        elseif num == 8 then
            soundName = "SwatReborn.Radiation.Heartbeat.40"
        elseif num == 9 then
            soundName = "SwatReborn.Radiation.Heartbeat.60"
        elseif num == 10 then
            soundName = "Undying_Zombie.Spawn"
        elseif num == 11 then
            soundName = "SwatReborn.NukeExplosion"
        elseif num == 12 then
            soundName = "Hero_EarthShaker.EchoSlamSmall"
        elseif num == 13 then
            soundName = "Hero_Brewmaster.ThunderClap"
        elseif num == 14 then
            soundName = "Roshan.Attack"
        end

        if soundName then
            if method == 1 then
                local hero = DebugCommands:GetPlayerHero()
                StartSoundEvent(soundName, hero)
            elseif method == 2 then
                local hero = DebugCommands:GetPlayerHero()
                EmitSoundOnLocationForAllies(hero:GetAbsOrigin(), soundName, hero)
            else
                EmitGlobalSound(soundName)
            end
        else
            print("Unknown sound id: " .. tostring(num))
        end
    end
)

DebugCommands:AddDebugCommand(
    "killme",
    function()
        local hero = DebugCommands:GetPlayerHero()
        hero:ForceKill(true)
    end
)

DebugCommands:AddDebugCommand(
    "revive",
    function(withParticles)
        if not withParticles then
            withParticles = true
        else
            withParticles = tonumber(withParticles) > 0
        end

        local hero = DebugCommands:GetPlayerHero()
        hero:respawn(withParticles)
    end
)

-- Sets the log level to the number provided. Lower numbers are more restrictive
-- 1 = ERROR, 2 = WARNING, 3 = INFO, 4 = DEBUG, 5 = VERBOSE
DebugCommands:AddDebugCommand(
    "log_level",
    function(level)
        level = tonumber(level)
        Log:setLogLevel(level)
    end
)

-- Whitelists a tag. Only logs with this tag will display
-- Other tags added with this command will continue to display as well
-- Reset with log_reset
DebugCommands:AddDebugCommand(
    "log_tag",
    function(level)
        Log:whitelistTag(level)
    end
)

-- Whitelists a tag. Logs with this tag will no longer display
-- Other tags added with this command will continue to not display as well
-- Reset with log_reset
DebugCommands:AddDebugCommand(
    "log_ignore",
    function(level)
        Log:blacklistTag(level)
    end
)

-- Clears all whitelisted and blacklisted tags
DebugCommands:AddDebugCommand(
    "log_reset",
    function()
        Log:clearTagFilters()
    end
)

-- DebugCommands:AddDebugCommand(
--     "resend_game_events",
--     function()
--         local playerInfo = DebugGameChatCommand_GetPlayerInfo()
--         GameEventCacheBroadcaster:rebroadcastToPlayerId(playerInfo.playerId)
--     end
-- )

DebugCommands:AddDebugCommand(
    "set_attributes",
    function(str, agi, int, primary)
        local hero = DebugCommands:GetPlayerHero()

        if not hero.stats then
            CustomStats:SetUpHeroForCustomStats(hero)
        end

        hero:SetCustomBaseStrength(tonumber(str))
        hero:SetCustomBaseAgility(tonumber(agi))
        hero:SetCustomBaseIntellect(tonumber(int))

        if primary then
            hero:SetPrimaryAttribute(primary)
        end
    end
)

-- Prints out the hero states of every hero
DebugCommands:AddDebugCommand(
    "hero_states",
    function()
        for _,playerInfo in pairs(PlayerManager.allPlayerInfos) do
            local hero = playerInfo.hero

            if not hero then
                print(tostring(playerInfo.playerId) .. " -> NO HERO")
            elseif not hero.getHeroState then
                print(tostring(playerInfo.playerId) .. " -> NO STATE SET UP?")
            else
                local heroState = HeroStateManager:HeroStateName(hero:getHeroState())
                print(tostring(playerInfo.playerId) .. " -> " .. hero:getHeroState())
            end
        end
    end
)

-- Prints out the hero states of every hero
DebugCommands:AddDebugCommand(
    "set_hero_state",
    function(state)
        if not state then
            print("Need state number (1-4)")
        end

        local hero = DebugCommands:GetPlayerHero()

        local heroState = nil
        if state == "1" or state == "out" then
            heroState = HeroStateManager.HeroState.OUT_OF_GAME
        elseif state == "2" or state == "inactive" then
            heroState = HeroStateManager.HeroState.INACTIVE
        elseif state == "3" or state == "frozen" then
            heroState = HeroStateManager.HeroState.FROZEN
        elseif state == "4" or state == "active" then
            heroState = HeroStateManager.HeroState.ACTIVE
        end

        hero:setHeroState(heroState)
    end
)

-- Prints out the hero states of every hero
DebugCommands:AddDebugCommand(
    "disconnect",
    function()
        print("DEBUG | 'Disconnecting' player")

        local hero = DebugCommands:GetPlayerHero()
        PlayerManager:onPlayerDisconnect(hero.playerInfo)
    end
)

-- Prints out the hero states of every hero
DebugCommands:AddDebugCommand(
    "reconnect",
    function()
        print("DEBUG | 'Reconnecting' player")

        local hero = DebugCommands:GetPlayerHero()
        PlayerManager:onPlayerReconnect(hero.playerInfo)
    end
)

-- Resets the hero states on all heros using HeroStateManager.
-- WARNING: This could break a lot of things if done while a minigame is running
DebugCommands:AddDebugCommand(
    "reset_hero_states",
    function()
        print("DEBUG | Reseting all hero states.")
        for _,hero in pairs(PlayerManager:getPlayerHeroes()) do
            hero.heroInfo = nil
            HeroStateManager:initializeUnit(hero)
        end
    end
)

-- Resets all region listeners
-- WARNING: This could break a lot of things if done while a minigame is running
DebugCommands:AddDebugCommand(
    "reset_region_listeners",
    function()
        print("DEBUG | Reseting all region listener.")
        RegionListeners.locationsToFunctions = {}
        RegionListeners.locationSetsToFunctions = {}
    end
)

-- MARK: MinigameManager Commands

-- Restarts the MinigameManager, effectively restarting the entire game.
DebugCommands:AddDebugCommand(
    "restart_game",
    function(isDebug)
        local mapName = GetMapName()
        if string.find(mapName, "playground") then
            print("WARNING: The playground map does not support this command.")

        elseif string.find(mapName, "portalprotectors") then
            if getmetatable(GameManager.minigameManager) == TestMinigameManager then
                print("ERROR: Game is in Debug mode. Disable debug mode to restart.")
            else
                print("== Restarting Game ==")
                local minigames = PortalProtectorsMap:loadMinigames()
                GameManager.minigameManager:onStop()
                GameManager.minigameManager:onStart()
            end

        else
            error("ERROR | Could not find a map loader for map " .. tostring(mapName))
        end
    end
)

DebugCommands:AddDebugCommand(
    "list_actions",
    function(isDebug)
        print("== MingameManager Actions ==")
        local currentAction = GameManager.minigameManager.currentAction
        if not currentAction then
            print ">> No current action <<"
        else
            print("Current Action: " .. currentAction:toString())
        end

        local actions = GameManager.minigameManager.state.actions
        if not actions then
            print ">> No actions in queue <<"
        else
            for _,action in pairs(actions) do
                print(action:toString())
            end
        end
    end
)

DebugCommands:AddDebugCommand(
    "next_action",
    function(isDebug)
        print("== DEBUG COMMAND: MingameManager Moving to next action ==")
        local currentAction = GameManager.minigameManager:startNextAction()
    end
)

-- MARK: Minigame debug mode commands
--       (Flips into a mode where minigames can be loaded individually and the game won't keep track of anything)

-- Moves map into debug mode
DebugCommands:AddDebugCommand(
    "debug_mode",
    function(isDebug)
        isDebug = (isDebug == "1" or isDebug == "true")

        local mapName = GetMapName()
        if isDebug then
            local mapManager = nil
            if string.find(mapName, "playground") then
                print("This map is already in debug mode")

            elseif string.find(mapName, "portalprotectors") then
                if getmetatable(GameManager.minigameManager) == TestMinigameManager then
                    print("Already in Debug mode")
                else
                    print("== DEBUG MODE ENABLED ==")
                    local minigames = PortalProtectorsMap:loadMinigames()
                    GameManager.minigameManager = TestMinigameManager:new({
                        minigames=minigames
                    })
                end

            else
                error("ERROR | Could not find a map loader for map " .. tostring(mapName))
            end
        else
            if string.find(mapName, "playground") then
                print("The playground map only support debug mode")

            elseif string.find(mapName, "portalprotectors") then
                if getmetatable(GameManager.minigameManager) == MinigameManager then
                    print("Already in regular mode")
                else
                    print("== DEBUG MODE DISABLED ==")
                    local minigames = PortalProtectorsMap:loadMinigames()
                    GameManager.minigameManager = MinigameManager:new({
                        map = PortalProtectorsMap
                    })
                end

            else
                error("ERROR | Could not find a map loader for map " .. tostring(mapName))
            end
        end
    end
)

-- Lists all minigames
DebugCommands:AddDebugCommand(
    "minigames",
    function()
        if getmetatable(GameManager.minigameManager) == TestMinigameManager then
            local minigames = GameManager.minigameManager.minigames
            print("DEBUG | Listing all minigames:")
            for index,minigame in pairs(minigames) do
                print("   [ " .. tostring(index) .. " ] " .. tostring(minigame.tag))
            end
        else
            print(GetMapName() .. " map must be in debug_mode to use this command")
        end
    end
)

-- Disposes of any active minigame and starts a new one
DebugCommands:AddDebugCommand(
    "launch_minigame",
    function(arg)
        if getmetatable(GameManager.minigameManager) == TestMinigameManager then
            local minigames = GameManager.minigameManager.minigames
            local minigameClass = minigames[tonumber(arg)]
            if not minigameClass then
                print("DEBUG | Error. No minigameClass found with index: " .. tostring(arg) .. " | See: minigames command for valid indices")
                return
            end

            local minigameManager = GameManager.minigameManager
            if minigameManager.activeRound then
                minigameManager.activeRound:dispose()
            end

            minigameManager.activeRound = minigameManager:createRound(minigameClass)
            minigameManager:startActiveRound()
        else
            print(GetMapName() .. " map must be in debug_mode to use this command")
        end
    end
)

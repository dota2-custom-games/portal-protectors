-- Util for display popup text like damage numbers

if not Popups then
    Popups = {}
    Popups.__index = Popups

    Popups.PreSymbols = {
        PLUS = 0,
        MINUS = 1,
        SADFACE = 2,
        BROKENARROW = 3,
        SHADES = 4,
        MISS = 5,
        EVADE = 6,
        DENY = 7,
        ARROW = 8
    }
    Popups.PostSymbols = {
        EXCLAMATION = 0,
        POINTZERO = 1,
        MEDAL = 2,
        DROP = 3,
        LIGHTNING = 4,
        SKULL = 5,
        EYE = 6,
        SHIELD = 7,
        POINTFIVE = 8
    }

end

function Popups:Numbers(target, pfx, color, lifetime, number, presymbol, postsymbol)
    print("==>",target,pfx,color,lifetime,number)
    local pfxPath = string.format("particles/msg_fx/msg_%s.vpcf", pfx)
    local pidx = ParticleManager:CreateParticle(pfxPath, PATTACH_ABSORIGIN_FOLLOW, target) -- target:GetOwner()

    local digits = 0
    if number ~= nil then
        number = math.floor(number)
        digits = #tostring(number)
    end
    if presymbol ~= nil then
        digits = digits + 1
    end
    if postsymbol ~= nil then
        digits = digits + 1
    end

    ParticleManager:SetParticleControl(pidx, 1, Vector(tonumber(presymbol), tonumber(number), tonumber(postsymbol)))
    ParticleManager:SetParticleControl(pidx, 2, Vector(lifetime, digits, 0))
    ParticleManager:SetParticleControl(pidx, 3, color)
end

-- e.g. when healed by an ability
function Popups:Healing(target, amount)
    Popups:Numbers(target, "heal", Vector(0, 255, 0), 1.0, amount, Popups.PreSymbols.PLUS, nil)
end

---- e.g. the popup you get when you suddenly take a large portion of your health pool in damage at once
function Popups:Damage(target, amount)
    Popups:Numbers(target, "damage", Vector(255, 0, 0), 1.0, amount, nil, Popups.PostSymbols.DROP)
end

function Popups:DamageSmall(target, amount)
    Popups:Numbers(target, "damage_small", Vector(255, 0, 0), 1.0, amount, nil, Popups.PostSymbols.DROP)
end

---- e.g. when dealing critical damage
function Popups:CriticalDamage(target, amount)
    Popups:Numbers(target, "crit", Vector(255, 0, 0), 1.0, amount, nil, Popups.PostSymbols.LIGHTNING)
end

---- e.g. when taking damage over time from a poison type spell
function Popups:DamageOverTime(target, amount)
    Popups:Numbers(target, "poison", Vector(215, 50, 248), 1.0, amount, nil, Popups.PostSymbols.EYE)
end

---- e.g. when blocking damage with a stout shield
function Popups:DamageBlock(target, amount)
    Popups:Numbers(target, "block", Vector(255, 255, 255), 1.0, amount, Popups.PreSymbols.MINUS, nil)
end

---- e.g. when last-hitting a creep
function Popups:GoldGain(target, amount)
    Popups:Numbers(target, "gold", Vector(255, 200, 33), 1.0, amount, Popups.PreSymbols.PLUS, nil)
end

---- e.g. when missing uphill
function Popups:Miss(target)
    Popups:Numbers(target, "miss", Vector(255, 0, 0), 1.0, nil, Popups.PreSymbols.MISS, nil)
end

function Popups:Experience(target, amount)
    Popups:Numbers(target, "miss", Vector(154, 46, 254), 1.0, amount, Popups.PreSymbols.PLUS, nil)
end

function Popups:Mana(target, amount)
    Popups:Numbers(target, "heal", Vector(0, 176, 246), 1.0, amount, Popups.PreSymbols.PLUS, nil)
end

function Popups:HPRemovalDamage(target, amount)
    Popups:Numbers(target, "crit", Vector(154, 46, 254), 3.0, amount, nil, Popups.PostSymbols.LIGHTNING)
end

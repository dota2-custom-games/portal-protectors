-- useful functions for printing things

function PrintTable(t, indent, done)
  --print ( string.format ('PrintTable type %s', type(keys)) )
  if type(t) ~= "table" then return end

  done = done or {}
  done[t] = true
  indent = indent or 0

  local l = {}
  for k, v in pairs(t) do
    table.insert(l, k)
  end

  table.sort(l)
  for k, v in ipairs(l) do
    -- Ignore FDesc
    if v ~= 'FDesc' then
      local value = t[v]

      if type(value) == "table" and not done[value] then
        done [value] = true
        print(string.rep ("\t", indent)..tostring(v)..":")
        PrintTable (value, indent + 2, done)
      elseif type(value) == "userdata" and not done[value] then
        done [value] = true
        print(string.rep ("\t", indent)..tostring(v)..": "..tostring(value))
        PrintTable ((getmetatable(value) and getmetatable(value).__index) or getmetatable(value), indent + 2, done)
      else
        if t.FDesc and t.FDesc[v] then
          print(string.rep ("\t", indent)..tostring(t.FDesc[v]))
        else
          print(string.rep ("\t", indent)..tostring(v)..": "..tostring(value))
        end
      end
    end
  end
end

function ShallowPrintTable(t)
    print("ShallowPrintTable -------------------------Start-------------------------")
    if t and type(t) == "table" then
        for k,v in pairs(t) do
            print(k,v)
        end
    end
    print("ShallowPrintTable --------------------------End--------------------------")
end

function PrintEntityFunctions(t, all)

    local mt = getmetatable(t)
    if not mt then
        print("Object has no metatable.")
        return
    end

    mt = mt.__index
    if mt and mt ~= t then
        ShallowPrintTable(mt)
        if all then
            PrintEntityFunctions(mt)
        end
    end
end

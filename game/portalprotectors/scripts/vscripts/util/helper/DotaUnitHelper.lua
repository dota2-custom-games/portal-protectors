-- Util functions involving Dota "Units" (Creatures/Heroes)

-- Returns if the passed in unit type has a flag set to 1
-- in its unit type KV file
-- Ex:
-- some_unit.txt:
--      "ConsideredHero" "1"
-- HasUnitTypeFlag(unit, "ConsideredHero") == 1
--
-- Determines this by checking the K-V pairs for a unit's type for a value of 1.
-- @param unit Unit to check
-- @param flag_name The name of the key to check
function HasUnitTypeFlag(unit, flag_name)
    local unitInfo = GameMode.unit_infos[unit:GetUnitName()]

    -- If it is defined, and the flag_name value is 1 then return true
    -- otherwise, the default is false
    return unitInfo ~= nil and unitInfo[flag_name] == 1
end

--- Change the level of an ability for a unit.
-- @param unit | The unit who owns the ability
-- @param abilityName | The name of the ability to change
-- @param level | The level to set the ability to
function SetUnitAbilityLevel(unit, abilityName, level)
    local ability = unit:FindAbilityByName(abilityName)
    if ability then
        ability:SetLevel(new_level)
    else
        Log:w(
            "DotaUnitHelper",
            "Could not set unit's " .. tostring(abilityName)
            .. " ability to level " .. tostring(level)
            .. " because the unit does not have that ability"
        )
    end
end

-- Levels up the passed unit to the provided level
-- but does so in a way as to NOT affect life, mana, or ability levels
-- which is what would innately happen
function SetCreatureLevel(unit, level)
    -- We have to prevent DOTA from fucking upgrading all of the abilities and setting max mana and heals, and setting max health/mana
    if not unit:IsNull() and unit:IsAlive() then
        local mobHealth = unit:GetHealth()
        local mobMana = unit:GetMana()
        local abilities = {}
        for i=0,16 do
            local ability = unit:GetAbilityByIndex(i)
            i = i + 1
            if ability then
                abilities[ability:GetName()] = ability:GetLevel()
            end
        end
        local currentLevel = unit:GetLevel() or 1
        unit:CreatureLevelUp(level - currentLevel)
        unit:SetHealth(math.max(1,mobHealth))
        unit:SetMana(mobMana)
        for i=0,16 do
            local ability = unit:GetAbilityByIndex(i)
            i = i + 1
            if ability then
                ability:SetLevel(abilities[ability:GetName()])
            end
        end
    end
end

--- Returns true if the unit is not at max life
function IsHurt(unit)
    return unit:GetHealthDeficit() > 0
end

-- Returns the distance between two units
function DistanceBetweenUnits(src, dest)
    return (src:GetAbsOrigin() - dest:GetAbsOrigin()):Length()
end

-- Dota's FindAllInSphere is *broken* and FindAllByName is useless
-- ** FindAllInSphere will use the unit's scale when determining if in the radius.
-- This will search through all creatures and return those matching the passed in name
function FindAllCreaturesByName(unitName)
    local retval = {}
    --local allUnits = Entities:FindAllInSphere(caster:GetAbsOrigin(), radius)
    local allUnits = Entities:FindAllByClassname("npc_dota_creature")
    for _,unit in pairs(allUnits) do
        if unit.GetUnitName then
            if unit:GetUnitName() == unitName then
                table.insert(retval, unit)
            end
        end
    end
    return retval
end

-- Dota's FindAllInSphere is *broken* and FindAllByName is useless
-- ** FindAllInSphere will use the unit's scale when determining if in the radius.
-- This will search through all creatures and return those matching the passed in name
-- that are within radius of the passed in vector
function FindAllCreaturesByNameWithin(unitName, position, radius)
    local retval = {}
    --local allUnits = Entities:FindAllInSphere(caster:GetAbsOrigin(), radius)
    local allUnits = Entities:FindAllByClassname("npc_dota_creature")
    for _,unit in pairs(allUnits) do
        if unit.GetUnitName then
            if unit:GetUnitName() == unitName then
                if distanceBetweenVectors(position, unit:GetAbsOrigin()) <= radius then
                    table.insert(retval, unit)
                end
            end
        end
    end
    return retval
end

-- Returns the name of the player controlling this unit (else returns the unit)
function GetUnitOwnerName(unit)
    local name = nil
    local playerId = unit:GetPlayerOwnerID()
    if playerId then
        name = PlayerResource:GetPlayerName(playerId)
    end
    if (not name) or (string.len(name) < 1) then
        local player = PlayerResource:GetPlayer(playerId)
        if player and player.playerInfo then
            name = "Player " .. tostring(player.playerInfo.playerIndex)
        else
            name = unit:GetUnitName()
        end
    end
    return name
end

function GetUnitOwnerPlayer(unit)
    local playerId = unit:GetPlayerOwnerID()
    local player = PlayerResource:GetPlayer(playerId)

    return player
end

-- Because the default CreateUnit fails all the time at finding clear space, this method will create the unit and add a timer
-- to find clear space for the unit
-- @param unitName [String] | The unit's name to spawn
-- @param position [Vector] | The location to spawn
-- @param teamNumber [Int]  | The team number to spawn for
-- @return unit created
function CreateUnitAndFindClearSpace(unitName, position, teamNumber)
    if not unitName or not position or not teamNumber then
        print("Warning | Could not create and find space for unit: unitName=" .. tostring(unitName) .. " | position=" .. tostring(position) .. " | teamNumber=" .. tostring(teamNumber))
    end
    local unit = CreateUnitByName( unitName, position, true, nil, nil, teamNumber )
    Timers:CreateTimer(function()
        FindClearSpaceForUnit(unit, position, true)
    end)
    return unit
end

-- Emits a sound at the provided position using a dummy unit
function EmitSoundWithDummy(position, sound)
    local dummy = CreateTemporaryDummyUnit(position, 1, DOTA_TEAM_GOODGUYS)
    dummy:EmitSound(sound)
end

function CreateTemporaryDummyUnit(position, duration, team)
    local dummy = CreateUnitByName("dummy_unit", position, true, nil, nil, team)
    Timers:CreateTimer(duration, function()
        if not dummy:IsNull() then
            dummy:RemoveSelf()
        end
    end)
    return dummy
end

-- Given a position (Vector) and a list of entities,
-- @return the closest entity to the position
function GetClosestEntity(position, entities)
    local closestEnt = nil
    local closestDistance = 0
    for _,ent in pairs(entities) do
        local distance = distanceBetweenVectors(position, ent:GetAbsOrigin())
        if (not closestEnt) or distance < closestDistance then
            closestDistance = distance
            closestEnt = ent
        end
    end

    return closestEnt
end

-- Adds a function that will be executed when the unit dies
-- All functions added are executed in the order they are added
-- @param onDeathFunction(killedUnit, killerEntity?, killerAbility?)
function AddOnDeathFunction(unit, onDeathFunction)
    if not unit then
        print("EnemySpawner | WARNING | Must supply a unit to addOnDeathFunction!")
    elseif not onDeathFunction or type(onDeathFunction) ~= "function" then
        print("EnemySpawner | WARNING | Must supply a function to addOnDeathFunction!")
    end

    if not unit.onDeath then
        unit.onDeath = onDeathFunction
    elseif type(unit.onDeath) == "function" then
        -- Convert to a list of functions to run
        local onDeathFunctionList = { unit.onDeath, onDeathFunction }
        unit.onDeath = onDeathFunctionList
    else
        table.insert(unit.onDeath, onDeathFunction)
    end
end

-- Removes all functions that would have been run when the unit died
function RemoveAllOnDeathFunctions(unit)
    unit.onDeath = nil
end

-- Applies (or sets) a movespeed modifier to a unit
-- We can only set units below 100 movespeed using an "absolute" modifier,
-- and a unit can have only one of these. So we have to apply them all from the same place
function ApplyMultiplicativeMoveSpeedModifier(unit, amount)
    if not unit.moveSpeedMultiplier then
        --print("Adding new ms modifier. Amount: "..tostring(amount))
        unit.moveSpeedMultiplier = amount
        local oldms = unit.postScalingMoveSpeedMultiplier
        unit.postScalingMoveSpeedMultiplier = ScaleMoveSpeedMultiplier(unit)
        local modifier = unit:FindModifierByName("modifier_common_movespeed")
        if not modifier then
            unit:AddNewModifier(unit, nil, "modifier_common_movespeed", {})
        end
    else

        --print("Updating move speed modifier. Old: "..tostring(unit.moveSpeedMultiplier)..", new: "..tostring(unit.moveSpeedMultiplier + amount))
        unit.moveSpeedMultiplier = unit.moveSpeedMultiplier + amount -- The actual slow will be handled by the modifier
        unit.postScalingMoveSpeedMultiplier = ScaleMoveSpeedMultiplier(unit)
        --print("After scaling value: "..tostring(unit.postScalingMoveSpeedMultiplier))
        if unit.moveSpeedMultiplier == 0 and not unit.moveSpeedStatic then
            unit:RemoveModifierByName("modifier_common_movespeed")
            unit.moveSpeedMultiplier = nil
            unit.postScalingMoveSpeedMultiplier = nil
        end
    end
end

function ApplyAdditiveMoveSpeedModifier(unit, amount)
    local modifier = unit:FindModifierByName("modifier_common_movespeed")
    if not unit.moveSpeedStatic then
        unit.moveSpeedStatic = amount
        unit:AddNewModifier(unit, nil, "modifier_common_movespeed", {})
    else
        unit.moveSpeedStatic = unit.moveSpeedStatic + amount -- The actual slow will be handled by the modifier
        if unit.moveSpeedStatic == 0 and not unit.moveSpeedMultiplier then
            unit:RemoveModifierByName("modifier_common_movespeed")
            unit.moveSpeedStatic = nil
        end
    end
end

-- Scales a movespeed penalty for a unit
-- Movespeed penalties "taper" towards -1,
-- so that a -100% slow doesn't immobilise completely
-- Math note: we convert a negative slow to a value above 1
-- Then we square root it (bringing it closer to 1), then revert it back below 0
-- This means that larger slows are affected more, making it very hard to achieve 100%
function ScaleMoveSpeedMultiplier(unit)

    if unit:isHero() then
        if unit.moveSpeedMultiplier > -0.5 then
            return unit.moveSpeedMultiplier
        end

        return -((0.5 - unit.moveSpeedMultiplier)^(0.5)-0.5)
    end

    -- Implicit else
    if unit.moveSpeedMultiplier > -0.7 then
        return unit.moveSpeedMultiplier
    end

    return -((0.3-unit.moveSpeedMultiplier)^(0.5)-0.3)

end

-- Removes all of the skills and modifiers on the passed in hero
function RemoveAllSkills(hero)
    -- loop through hero's skills, fetch them, and remove them subtract one to 0 index
    for index = 0, hero:GetAbilityCount()-1 do
        -- Check if we get an ability, because GetAbilityCount likes to
        -- return 16 (max abilities a hero can have?) regardless
        if hero:GetAbilityByIndex(index) then
            hero:RemoveAbility(hero:GetAbilityByIndex(index):GetAbilityName())
        end
    end

    -- Remove all modifiers
    local modifiers = hero:FindAllModifiers()
    for _,modifierName in pairs(modifiers) do
        hero:RemoveModifierByName(modifierName:GetName())
    end
end

-- This function hides all dota item cosmetics (hats/wearables) from the hero/unit and store them into a handle variable
function HideWearables(unit)
    unit.hiddenWearables = {} -- Keep every wearable handle in a table to show them later
    local model = unit:FirstMoveChild()
    while model ~= nil do
        if model:GetClassname() == "dota_item_wearable" then
            model:AddEffects(EF_NODRAW) -- Set model hidden
            table.insert(unit.hiddenWearables, model)
        end
        model = model:NextMovePeer()
    end
end

-- This function un-hides (shows) wearables that were hidden with HideWearables() function.
function ShowWearables(unit)
    for i,v in pairs(unit.hiddenWearables) do
        v:RemoveEffects(EF_NODRAW)
    end
end

-- Removes all wearable items on the passed in hero
function RemoveWearables(hero)
    local children = hero:GetChildren()
    for k,child in pairs(children) do
        if child:GetClassname() == "dota_item_wearable" then
            child:RemoveSelf()
        end
    end
end

-- This function changes (swaps) dota item cosmetic models (hats/wearables)
function SwapWearable(unit, target_model, new_model)
    local wearable = unit:FirstMoveChild()
    while wearable ~= nil do
        if wearable:GetClassname() == "dota_item_wearable" then
            if wearable:GetModelName() == target_model then
                wearable:SetModel(new_model)
                return
            end
        end
        wearable = wearable:NextMovePeer()
    end
end

-- Creates a temporary dummy unit that is given the passed ability and then it applies the modifier
-- to the unit
-- This is particularly handy when dealing with Officers or other units that may have 16 abilities
-- Level will default to 1
MODIFIERS_BEING_APPLIED = {}
function ApplyModifierUsingDummyAbility(unit, abilityName, modifierName, level, args)
    local level = level or 1
    local args = args or {}

    if unit and not unit:IsNull() then
        local dummy = CreateUnitByName("dummy_unit", unit:GetAbsOrigin(), true, nil, nil, DOTA_TEAM_GOODGUYS)
        local ability = dummy:AddAbility(abilityName)
        ability:SetLevel(level)
        if level > 1 then
            -- Not sure why, but if the level is greater than 1, we need to wait
            -- before applying the modifier, otherwise it applies as if the ability level was 1
            -- Additionally, because timers are not necessarily executed in order, we must keep track
            -- of the most recent modifier level applied to a unit so that we don't overwrite it with a different one

            local combinedName = abilityName .. ":" .. modifierName
            MODIFIERS_BEING_APPLIED[unit] = MODIFIERS_BEING_APPLIED[unit] or {}
            MODIFIERS_BEING_APPLIED[unit][combinedName] = dummy
            Timers:CreateTimer(0.33, function()
                if MODIFIERS_BEING_APPLIED[unit] and MODIFIERS_BEING_APPLIED[unit][combinedName] == dummy then
                    -- This is still the most recently applied level for the modifier in question, so apply it
                    ability:ApplyDataDrivenModifier(unit, unit, modifierName, args)
                    MODIFIERS_BEING_APPLIED[unit][combinedName] = nil
                    if TableIsEmpty(MODIFIERS_BEING_APPLIED[unit]) then
                        MODIFIERS_BEING_APPLIED[unit] = nil
                    end
                else
                    -- A different level has been applied to the unit since the application of this modifier. So, don't do anything
                    print("Ignoring application of '" .. tostring(combinedName) .. "' [" .. tostring(level) .. "] because it's being applied by something else")
                end
                dummy:RemoveSelf()
            end)
        else
            ability:ApplyDataDrivenModifier(unit, unit, modifierName, args)
            dummy:RemoveSelf()
        end
    else
        print("ERROR: no unit or null unit passed to ApplyModifierUsingDummyAbility: " .. tostring(unit))
    end
end

-- Creates a permanent dummy unit that is given the passed ability and then it applies the modifier
-- to the unit. There is only one permanent dummy unit per ability
-- This is particularly handy when dealing with Officers or other units that may have 16 abilities
-- The benefit of this is that any displayed modifier will have the correct icon instead of an empty dota one
-- Level will default to 1
dummy_modifier_units = {}
function ApplyModifierUsingPermanentDummy(unit, abilityName, modifierName, level, args)
    level = level or 1
    local args = args or {}

    local dummyUnit = dummy_modifier_units[abilityName]
    local ability = nil
    if not dummyUnit then
        print("Making permanent dummy unit for ability: " .. tostring(abilityName))
        dummyUnit = CreateUnitByName("dummy_unit", Vector(0,0,0), true, nil, nil, DOTA_TEAM_GOODGUYS)
        ability = dummyUnit:AddAbility(abilityName)
        ability:SetLevel(level)
        dummy_modifier_units[abilityName] = dummyUnit
    else
        ability = dummyUnit:FindAbilityByName(abilityName)
    end

    ability:SetLevel(level)
    ability:ApplyDataDrivenModifier(unit, unit, modifierName, args)
end

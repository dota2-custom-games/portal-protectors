-- useful functions on Sets
-- A Set is defined here as a table where only the keys matter

function Set( my_table )
    local set = {}
    for _, element in pairs( my_table ) do
        set[element] = true
    end
    return set
end

function SetToList(set)
    local retval = {}
    local index = 1
    for value,_ in pairs(set) do
        retval[index] = value
        index = index + 1
    end
    return retval
end

function SetToString( set )
    local retval = "{"
    local addingCommas = false
    for element,_ in pairs(set) do
        if addingCommas then
            retval = retval .. ", "
        else
            addingCommas = true
        end
        retval = retval .. tostring(element)
    end
    retval = retval .. "}"
    return retval
end

-- Util functions dealing with DOTA Items

-- Creates and launches an item at a specific position
-- returns the item created
function CreateAndLaunchItem(position, itemName)
    local item = CreateItem(itemName, nil, nil)
    if not item then
        print("ItemSpawningManager | Can't create item named: " .. tostring(itemName))
    end
    CreateItemOnPositionSync(position, item)

    item:LaunchLoot(false, 200, 0.75, RandomTraversablePosition(position, 75, 100))
    return item
end

-- Decreases the charge of an item or removes the item if it would be empty
-- If the item is permanent, the charges can be set to 0
function DecreaseItemCount(caster, item)
    local current_charges = item:GetCurrentCharges()
    if item:IsPermanent() or current_charges > 1 then
        item:SetCurrentCharges( current_charges - 1 )
    else
        caster:RemoveItem(item)
    end
end

-- Removes all items the unit has. Not drop. Remove
function RemoveAllItems(unit)
    if SHOW_ITEM_DEBUG_LOGS then
        print( 'ItemFunctions | RemoveAllItems unit=' .. tostring(unit) )
    end
    if unit:HasInventory() then
        local unitOrigin = unit:GetAbsOrigin()
        for itemSlot = 0, 5, 1 do
            local item = unit:GetItemInSlot( itemSlot )
            if item ~= nil then
                unit:RemoveItem(item)
            end
        end
    end
end

DOTA_ITEMS_TO_REMOVE = {
    item_tpscroll=true
}
-- Removes all items the unit has that are invalid (like item_tpscroll)
function RemoveAllInvalidItems(unit)
    if unit:HasInventory() then
        if SHOW_ITEM_DEBUG_LOGS then
            print( 'ItemFunctions | RemoveAllInvalidItems unit=' .. tostring(unit) )
        end
        -- Remove any items dota adds on respawn
        local unitOrigin = unit:GetAbsOrigin()
        for itemSlot = 0, 5, 1 do
            local item = unit:GetItemInSlot( itemSlot )
            if item ~= nil then
                local itemName = item:GetAbilityName()
                if DOTA_ITEMS_TO_REMOVE[itemName] then
                    unit:RemoveItem(item)
                end
            end
        end
    end
end

-- Drops all items that are not marked as stay on death
function DropAllItems(unit)
    if SHOW_ITEM_DEBUG_LOGS then
        print( 'ItemFunctions | DropAllItems unit=' .. tostring(unit) )
    end
    if unit:HasInventory() then
        local unitOrigin = unit:GetAbsOrigin()
        for itemSlot = 0, 5, 1 do
            local item = unit:GetItemInSlot( itemSlot )
            if item ~= nil then
                -- Check to make sure we can drop this item on death
                local itemName = item:GetAbilityName()
                local itemAttributes = GameMode.ItemInfoKV[itemName]
                if (not itemAttributes) or (not itemAttributes.noDropOnDeath) then
                    local itemPosition = unitOrigin
                    if itemSlot < 3 then
                        itemPosition = itemPosition + Vector(-50 + (50 * itemSlot), 30,  0)
                    else
                        itemPosition = itemPosition + Vector(-50 + (50 * (itemSlot - 3)), -30, 0)
                    end
                    unit:DropItemAtPositionImmediate(item, unitOrigin)
                    item:LaunchLoot(false, RandomInt(100,200), RandomFloat(0.5,0.9), itemPosition)
                end
            end
        end
    end
end

-- Sets up a unit to have inventory
-- @Param itemsDisabled | (Default false) If true, the unit can pick up items but can not use them (like a robodog)
-- @param numberOfSlots | (Default 6) If less than 6, limits the number of slots
function InitializeInventory(unit, itemsDisabled, numberOfSlots)
    local itemsDisabled = itemsDisabled or false
    local numberOfSlots = math.min(6, (numberOfSlots or 6))

    if itemsDisabled then
        unit.itemsDisabled = itemsDisabled
    end
    unit.itemSlotsEnabled = numberOfSlots

    -- Add dummy items to their backpack
    for itemSlot = DOTA_ITEM_SLOT_7, DOTA_ITEM_SLOT_9 do
        local item = CreateItem("item_disable", nil, nil)
        unit:AddItem(item)
        unit:SwapItems(DOTA_ITEM_SLOT_1, itemSlot)
    end

    -- Add dummy items to fill empty slots
    if numberOfSlots < 6 then
        for itemSlot = (DOTA_ITEM_SLOT_1 + numberOfSlots),DOTA_ITEM_SLOT_6 do
            --print("Disabled Initial Slot: " .. tostring(itemSlot))
            local item = CreateItem("item_disable", nil, nil)
            unit:AddItem(item)
            unit:SwapItems(DOTA_ITEM_SLOT_1, itemSlot)
        end
    end

end

-- Sets the number of item slots a unit has
function SetItemSlots(unit, numberOfSlots)
    if not unit.itemSlotsEnabled then
        unit.itemSlotsEnabled = 6
    end

    if unit.itemSlotsEnabled < numberOfSlots then
        -- Unit gained slots - remove dummy items
        for i = DOTA_ITEM_SLOT_1 + unit.itemSlotsEnabled, DOTA_ITEM_SLOT_1 + numberOfSlots - 1 do
            --print("Enabled Slot: " .. tostring(i))
            local item = unit:GetItemInSlot(i)
            if item then
                unit:RemoveItem(item)
            end
        end
    elseif unit.itemSlotsEnabled > numberOfSlots then
        -- Unit lost slots - add dummy items
        for itemSlot = DOTA_ITEM_SLOT_1 + numberOfSlots, (DOTA_ITEM_SLOT_1 + unit.itemSlotsEnabled - 1) do
            --print("Disabled Slot: " .. tostring(i))
            local item = unit:GetItemInSlot(itemSlot)
            if item then
                -- Remove any existing item
                local position = unit:GetAbsOrigin()
                unit:DropItemAtPositionImmediate(item, position)
                item:LaunchLoot(false, 150, 0.33, position)
            end

            -- Find the first empty slot, add item and swap it to the spot we need
            for slot = DOTA_ITEM_SLOT_1, DOTA_ITEM_SLOT_6 do
                if not unit:GetItemInSlot(slotCheck) then
                    -- Add dummy item
                    local item = CreateItem("item_disable", nil, nil)
                    unit:AddItem(item)
                    unit:SwapItems(slot, itemSlot)
                end
            end
        end
    end

    unit.itemSlotsEnabled = numberOfSlots
    UpdateItemUsabilityFor(unit)
end

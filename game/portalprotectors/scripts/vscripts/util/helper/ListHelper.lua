-- useful functions on "lists"

-- Inserts all elements in otherlist into the list
function table.insertAll(list, otherList)
    local listSize = #list
    for index=(listSize+1),(listSize+#otherList) do
        list[index] = otherList[index - listSize]
    end
end

-- Returns the index position of the value in the list
-- If the value is not in the list, returns -1
function table.indexOf(list, value)
    for index,v in pairs(list) do
        if v == value then
            return index
        end
    end
    return -1
end

-- Returns a string describing the list contents
function ListString(list)
    local retval = "["
    local addingCommas = false
    for i=1,#list do
        if addingCommas then
            retval = retval .. ", "
        else
            addingCommas = true
        end
        retval = retval .. tostring(list[i])
    end
    retval = retval .. "]"
    return retval
end

-- Randomizes the elements within the list
function RandomizeList(list)
    for i = 1,#list do
        local randNum = RandomInt(1,#list)
        local temp = list[randNum]
        list[randNum] = list[i]
        list[i] = temp
    end
end

-- Given a list (table in the form {1=A, 2=B...}), returns a random value
function RandomValueInList(list)
    return list[RandomInt(1, #list)]
end

-- Converts a passed list to a 0 index one
-- (Useful for sending to javascript, which is 0 indexed)
-- @return the 0 indexed list
function ConvertListToZeroIndex(list)
    local retval = {}
    for k,v in pairs(list) do
        retval[k-1] = v
    end
    return retval
end

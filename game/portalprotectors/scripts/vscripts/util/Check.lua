-- Provides parameter checking methods for better type safety
--
-- Most checks involve passing in an arg followed by a "name" string
-- The name string is used for logging purposes if the check fails

if not Check then
    Check = {}
    Check.__index = Check
end

function Check:notNil(arg, name)
    if not arg then
        Check:throwError("notNil", "argument was nil", arg, name)
    end
end

-- MARK: Number Checks

function Check:isNumber(arg, name)
    if not arg then
        Check:throwError("isNumber", "argument was nil", arg, name)
    end
    if type(arg) ~= "number" then
        Check:throwError("isNumber", "argument was not a number. Type was " .. type(arg), arg, name)
    end
end

function Check:isPositive(arg, name)
    if not arg then
        Check:throwError("isPositive", "argument was nil", arg, name)
    end
    if type(arg) ~= "number" then
        Check:throwError("isPositive", "argument was not a number. Type was " .. type(arg), arg, name)
    end
    if arg <= 0 then
        Check:throwError("isPositive", "argument was not positive.", arg, name)
    end
end

function Check:isNegative(arg, name)
    if not arg then
        Check:throwError("isNegative", "argument was nil", arg, name)
    end
    if type(arg) ~= "number" then
        Check:throwError("isNegative", "argument was not a number. Type was " .. type(arg), arg, name)
    end
    if arg >= 0 then
        Check:throwError("isNegative", "argument was not positive.", arg, name)
    end
end

function Check:notPositive(arg, name)
    if not arg then
        Check:throwError("notPositive", "argument was nil", arg, name)
    end
    if type(arg) ~= "number" then
        Check:throwError("notPositive", "argument was not a number. Type was " .. type(arg), arg, name)
    end
    if arg > 0 then
        Check:throwError("notPositive", "argument was not positive.", arg, name)
    end
end

function Check:notNegative(arg, name)
    if not arg then
        Check:throwError("notNegative", "argument was nil", arg, name)
    end
    if type(arg) ~= "number" then
        Check:throwError("notNegative", "argument was not a number. Type was " .. type(arg), arg, name)
    end
    if arg < 0 then
        Check:throwError("notNegative", "argument was not positive.", arg, name)
    end
end

-- MARK: Function Checks

function Check:isFunction(arg, name)
    if not arg then
        Check:throwError("isFunction", "argument was nil", arg, name)
    end
    if type(arg) ~= "function" then
        Check:throwError("isFunction", "argument was not a table. Type was " .. type(arg), arg, name)
    end
end

-- MARK: Table Checks

function Check:isTable(arg, name)
    if not arg then
        Check:throwError("isTable", "argument was nil", arg, name)
    end
    if type(arg) ~= "table" then
        Check:throwError("isTable", "argument was not a table. Type was " .. type(arg), arg, name)
    end
end

-- Utility method to check if a table has the passed in keys. Mostly used to check that
-- a passed in class has the required methods/parameters needed.
-- Ex:
--      Check:tableHasKeys(someTable, "Person Table", {"name", "age"})
--      checks that the variable someTable is not nil and at least has the keys someTable["name"] and someTable["age"]
-- @param arg | The table to check for
-- @param name | An optional string name for this check for the logs if it fails
-- @param parameters | list of elements that table must have as keys in order to pass this check
function Check:tableHasKeys(arg, name, parameters)
    Check:isTable(arg, name)
    if parameters == nil or #parameters == 0 then
        -- Shouldn't happen
        PrintTable(table)
        Check:throwError("tableHasKeys", "parameters to check for are nil?", arg, name)
    end

    local isValid = true
    for _,parameter in pairs(parameters) do
        if parameter == nil then
            -- Shouldn't happen
            PrintTable(table)
            Check:throwError("tableHasKeys", "a parameter to check for was nil?", arg, name)
        end
        if arg[parameter] == nil then
            isValid = false
        end
    end

    if not isValid then
        print("\n!!!!!!!!!!!!!!!!!!!")
        print("!! " .. tostring(logName or "[Some table]") .. " is missing a required parameter !!\n")
        for _,parameter in pairs(parameters) do
            local isPresent = "good"
            if not arg[parameter] then
                isPresent = "MISSING!"
            end
            print("--'" .. tostring(parameter) .. "' : " .. tostring(isPresent))
        end
        print("\nObj:\n")
        PrintTable(table)
        print("\n!!!!!!!!!!!!!!!!!!!")

        Check:throwError("tableHasKeys", "table was missing a parameter", arg, name)
    end
end

-- MARK: List Checks

function Check:isList(arg, name)
    if not arg then
        Check:throwError("isList", "argument was nil", arg, name)
    end
    if type(arg) ~= "table" then
        Check:throwError("isList", "argument was not a table. Type was " .. type(arg), arg, name)
    end
    -- Not a guarentee this is a true list, but it's not performant for this check
    -- to exhaustively search the keys so it does do things:
    --      + Check if there an element at the first slot
    --      + If not, then check if there is at least one key, if so its an invalid list
    if not arg[1] then
    end
    for k,_ in pairs(arg) do
        if type(k) ~= "number" then
        end
    end

end

-- MARK: Private Helpers

function Check:throwError(
    checkName,
    reason,
    arg,
    name
)
    local message = "Check Failed: " .. tostring(checkName)
    message = message .. "\nArgument: " .. tostring(arg)
    if name then
        message = message .. "\nName: " .. tostring(name)
    end
    message = message .. "\nReason: " .. tostring(reason)
    error(message)
end

-- MARK: Tests

-- Check:notNil(nil, "varName")
-- Check:isNumber("hello", "varName")
-- Check:isPositive(-3, "varName")
-- Check:isNegative(-3, "varName")
-- Check:notPositive(0, "varName")
-- Check:notNegative(0, "varName")
-- Check:isTable({}, "varName")
-- Check:isTable("hello", "varName")

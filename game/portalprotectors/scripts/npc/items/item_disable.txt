"DOTAAbilities"
{
// Place into slots to disable them.
"item_disable"
{
    // General
    //-------------------------------------------------------------------------------------------------------------
    "BaseClass"						"item_datadriven"
    "ID"							"10410"
    "AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"

    "AbilityTextureName"			"custom/backpack_disable"

    // Item Info
    //-------------------------------------------------------------------------------------------------------------
    "ItemKillable"					"0"
    "ItemStackable"					"0"
    "ItemDroppable"                 "1"
    "ItemPermanent"					"1"
    "ItemInitialCharges"			"0"
    "ItemShareability"              "ITEM_NOT_SHAREABLE"

    "ItemPurchasable"               "0"
    "ItemCost"						"0"
    "ItemShopTags"					"consumable"
    "ItemQuality"					"consumable"
    "ItemAliases"					"disable"
    "ItemSellable"                  "0"

    "ItemStockMax"      "0"
    "ItemStockTime"     "0"
    "ItemStockInitial"  "0"

    "SideShop" "0"
    "SecretShop" "0"
}
}

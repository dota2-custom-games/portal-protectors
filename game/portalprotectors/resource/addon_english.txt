"lang"
{
	"Language"		"English"
	"Tokens"
	{		
		"addon_game_name"														"Portal Protectors"
		

		// ------------------------------- Playground Units and Abilities ----------------------- //
		"test_builder"															"Test Builder"


        // ------------------------ General Purpose UI ----------------------- //

        "PP_Countdown_StartsIn"                         "Starts In..."
        "PP_Make_A_Choice"                              "Choose:"
        "PP_Up_Next"                                    "Up Next..."
        "PP_Minigame_Success_true"                      "Success!"
        "PP_Minigame_Success_false"                     "Failure!"

        "PP_Score_Metric_time_seconds"                  "Time (Seconds)"

        "PP_Average_Rank"                               "Average Rank: "

        "PP_Zone_Complete"                              "Zone Complete!"
        "PP_Zone_Failed"                                "Zone Failed"
        "PP_Zone_Failed_Flavour"                        "You did not win enough games to proceed to the next zone. "
        "PP_Zone_Complete_Flavour"                      "Proceeding to the next zone! "

        "PP_Entering_Zone"                              "Now Entering..."
        "PP_Pass_N_Games"                               "Win %s games to proceed!"

        "PP_Zone_Name_Arcade"                           "Arcade Zone"
        "PP_Zone_Name_Scifi"                            "Scifi Zone"
        "PP_Zone_Name_Fantasy"                          "Fantasy Zone"
        "PP_Zone_Name_Dota"                             "Dota Zone"

        // These are used by the "up next" screens between games
        "PP_Minigame_Title_Arcade"                           "Arcade Zone"
        "PP_Minigame_Title_Scifi"                            "Scifi Zone"
        "PP_Minigame_Title_Fantasy"                          "Fantasy Zone"
        "PP_Minigame_Title_Dota"                             "Dota Zone"
        "PP_Minigame_Title_undefined"                        ""

        // ------------------- Test Data ------------------- //


        "PP_Minigame_Title_testgame1"                           "Test Game 1"
        "PP_Minigame_Title_Image_Available_testgame1_english"   "TRUE" // Set to true if we have a fancy title image
        "PP_Minigame_Description_testgame1"                     "A thrilling game description"
        "PP_Minigame_Description_Image_testgame1_0"             "Step 1: Steal Underpants"
        "PP_Minigame_Description_Image_testgame1_1"             "Step 2: ???"
        "PP_Minigame_Description_Image_testgame1_2"             "Step 3: Profit!"


        "PP_Minigame_Title_testgame2"                           "Test Game 2"
        "PP_Minigame_Title_Image_Available_testgame2_english"   "FALSE" // Set to true if we have a fancy title image
        "PP_Minigame_Description_testgame2"                     "Make spela"
        "PP_Minigame_Description_Image_testgame2_0"             "Watch out for ghosts!"
        "PP_Minigame_Description_Image_testgame2_1"             "Don't get eaten by a grue"
        "PP_Minigame_Description_Image_testgame2_2"             "Press Z or R twice"
        "PP_Minigame_Choice_Text_testgame2_0"                   "Scary Dragon"
        "PP_Minigame_Choice_Text_testgame2_1"                   "Fort Thing"
        "PP_Minigame_Choice_Text_testgame2_2"                   "Triangle King"
        "PP_Minigame_Choice_Text_Title_testgame2_0"                   "Magic Butterfly"
        "PP_Minigame_Choice_Text_Title_testgame2_1"                   "Rainbow Ferry"
        "PP_Minigame_Choice_Text_Title_testgame2_2"                   "Scarlet Cruise"
        "PP_Minigame_Choice_Text_Description_testgame2_0"             "Play as a magic butterfly with flappy wings"
        "PP_Minigame_Choice_Text_Description_testgame2_1"             "Roll on the rainbow ferry, igniting the air with the explosive heat of a continuous fusion reaction"
        "PP_Minigame_Choice_Text_Description_testgame2_2"             "Dress in red for this provocative soiree"


        // ********************************************* //
        // ******************* Games ******************* //
        // ********************************************* //



        // ------------- Game - Prologue --------------- //

        "PP_Minigame_Title_Prologue"                             "The Portal Protectors"
        "PP_Minigame_Title_Image_Available_Prologue_english"     "FALSE"
        "PP_Minigame_Description_Prologue"                       "Activate the portals!"

        // ------------- Game - Epilogue --------------- //

        "PP_Minigame_Title_Epilogue"                             "The End?"
        "PP_Minigame_Title_Image_Available_Epilogue_english"     "FALSE"
        "PP_Minigame_Description_Epilogue"                       "Open the portals to release their energy"
        "PP_Minigame_Description_Image_Tarpits_0"               "Portals must recharge after being opened too long"
        "PP_Minigame_Description_Image_Tarpits_1"               "Shut down any harmful portals"

        // ------------- Game - Tanks --------------- //

        "PP_Minigame_Title_Tanks"                             "Tank Tango"
        "PP_Minigame_Title_Image_Available_Tanks_english"     "FALSE"
        "PP_Minigame_Description_Tanks"                       "Use your tanks to protect the HQ"
        "PP_Minigame_Description_Image_Tanks_0"               "Your turret is controlled separately to the rest of the vehicle"
        "PP_Minigame_Description_Image_Tanks_1"               "Watch out for artillery strikes"

        // ------------- Game - ScifiTD --------------- //

        "PP_Minigame_Title_ScifiTD"                             "Undead Assault"
        "PP_Minigame_Title_Image_Available_ScifiTD_english"     "FALSE"
        "PP_Minigame_Description_ScifiTD"                       "Build towers to protect from the undead horde"
        "PP_Minigame_Description_Image_ScifiTD_0"               "Enemies will attack from all sides"
        "PP_Minigame_Description_Image_ScifiTD_1"               "Use walls to delay the assault"

        // ------------- Game - AxeFarm --------------- //

        "PP_Minigame_Title_AxeFarm"                             "Lumberjacked"
        "PP_Minigame_Title_Image_Available_AxeFarm_english"     "FALSE"
        "PP_Minigame_Description_AxeFarm"                       "Accelerate the deforestation process"
        "PP_Minigame_Description_Image_AxeFarm_0"               "Be careful - some trees are deadly"

        // ------------- Game - DotaDodging --------------- //

        "PP_Minigame_Title_DotaDodging"                      "Plunderer's Pitfall"
        "PP_Minigame_Title_Image_Available_DotaDodging_english"     "FALSE"
        "PP_Minigame_Description_DotaDodging"                       "Dodge the traps as they appear"
        "PP_Minigame_Description_Image_DotaDodging_0"               "Activate shrines to gain movement abilities"

        // ------------- Game - PixelMaze --------------- //

        "PP_Minigame_Title_PixelMaze"                             "Danger Road"
        "PP_Minigame_Title_Image_Available_PixelMaze_english"     "FALSE"
        "PP_Minigame_Description_PixelMaze"                       "Get to the end of the road!"
        "PP_Minigame_Description_Image_PixelMaze_0"               "Blast or dodge any obstacles"

        // ------------- Game - SpiderHunt --------------- //

        "PP_Minigame_Title_SpiderHunt"                             "Spider Hunt"
        "PP_Minigame_Title_Image_Available_SpiderHunt_english"     "FALSE"
        "PP_Minigame_Description_SpiderHunt"                       "Slay the Spider Queen"
        "PP_Minigame_Description_Image_SpiderHunt_0"               "Gather ability crystals to gain in power before she spawns"
        "PP_Minigame_Description_Image_SpiderHunt_1"               "Don't get swarmed by spiderlings!"

        // ------------- Game - SpaceShips --------------- //

        "PP_Minigame_Title_SpaceShips"                             "Space Battles"
        "PP_Minigame_Title_Image_Available_SpaceShips_english"     "FALSE"
        "PP_Minigame_Description_SpaceShips"                       "Collect points by blasting enemies"
        "PP_Minigame_Description_Image_SpaceShips_0"               "You lose all powerups when you die"

        // ------------- Game - TarPits --------------- //

        "PP_Minigame_Title_TarPits"                             "The Tar Pits"
        "PP_Minigame_Title_Image_Available_TarPits_english"     "FALSE"
        "PP_Minigame_Description_TarPits"                       "Navigate the maze without getting too sticky!"
        "PP_Minigame_Description_Image_Tarpits_0"               "Tar will slow you down"
        "PP_Minigame_Description_Image_Tarpits_1"               "Wash off at the waterfalls! "

	}
}

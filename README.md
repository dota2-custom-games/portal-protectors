# Portal Protectors

This is the public repository for work on the "Portal Protectors" arcade
mod for the DotA 2 Arcade.

## Cloning Instructions

DotA2 doesn't behave very well with simlinks. To set up the repository with the DotA workshop:

1. Clone this repository

Download Link Shell Extension if you don't have it (easy symbolic links)

2. In windows explorer, in your addon directory (C:\Dota_Addons\portalprotectors in example):
a. Navigate to ...\SteamApps\common\dota 2 beta\content\dota_addons (probably in your program_files or (x86) directory)
b. Move the portalprotectors\content folder into this folder
c. Right click on the newly moved content folder, and click "Pick Link Source"
d. Return to the git folder ...\portalprotectors\content
e. Right click and select drop as > Junction

3. Repeat this process for the /game/portalprotectors folder into ...\SteamApps\common\dota 2 beta\game\dota_addons